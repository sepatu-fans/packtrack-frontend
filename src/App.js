import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Home from './contents/Home'
import Items from './contents/Items'
import ItemsAdd from './contents/ItemsAdd'
import GradesA from './contents/GradesA'
import GradesOther from './contents/GradesOther'
import GradesADetail from './contents/GradesADetail'
import GradesOtherDetail from './contents/GradesOtherDetail'
import NotFound from './contents/NotFound'
import ItemsImport from './contents/ItemsImport';
import ManajementLabels from './contents/ManajementLabels';
import ManajementLabelsAdd from './contents/ManajementLabelsAdd';
import LabelPreview from './contents/LabelPreview';
import LabelOuterPreview from './contents/LabelOuterPreview';
import ProfilKarton from './contents/ProfilKarton'
import ProfilKartonAdd from './contents/ProfilKartonAdd'
import Shipping from './contents/Shipping';
import ShippingDetail from './contents/ShippingDetail';
import KonfigurasiSistem from './contents/KonfigurasiSistem';
import User from './contents/User';
import Login from './contents/Login';
import OpenCloseBox from './contents/OpenCloseBox';
import ShippingImport from './contents/ShippingImport';
import Laporan from './contents/Laporan';
import NoBox from './contents/NoBox';
import NoBoxDetail from './contents/NoBoxDetail';
import Receiving from './contents/Receiving';
import ReceivingDetail from './contents/ReceivingDetail';
import ReceivingImport from './contents/ReceivingImport';
import Transfer from './contents/Transfer';
import TransferDetail from './contents/TransferDetail';
import TransferImport from './contents/TransferImport';

function App() {
  const NoMatch = ({ location }) => (
    <NotFound />
  )
  
  return (
    <div id="wrapper">
      <Router>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/items">
            <Items />
          </Route>
          <Route exact path="/items-add">
            <ItemsAdd />
          </Route>
          <Route exact path="/items-import">
            <ItemsImport />
          </Route>
          <Route exact path="/grades-a">
            <GradesA />
          </Route>
          <Route exact path="/grades-a-detail">
            <GradesADetail />
          </Route>
          <Route exact path="/grades-other">
            <GradesOther />
          </Route>
          <Route exact path="/grades-other-detail">
            <GradesOtherDetail />
          </Route>
          <Route exact path="/nobox">
            <NoBox />
          </Route>
          <Route exact path="/nobox-detail">
            <NoBoxDetail />
          </Route>
          <Route exact path="/manajement-labels">
            <ManajementLabels />
          </Route>
          <Route exact path="/manajement-labels-add">
            <ManajementLabelsAdd />
          </Route>
          <Route exact path="/label-preview">
            <LabelPreview />
          </Route>
          <Route exact path="/label-outer-preview">
            <LabelOuterPreview />
          </Route>
          <Route exact path="/open-close-box">
            <OpenCloseBox />
          </Route>
          <Route exact path="/profil-karton">
            <ProfilKarton />
          </Route>
          <Route exact path="/profil-karton-add">
            <ProfilKartonAdd />
          </Route>
          <Route exact path="/pengiriman">
            <Shipping />
          </Route>
          <Route exact path="/pengiriman-detail">
            <ShippingDetail />
          </Route>
          <Route exact path="/pengiriman-import">
            <ShippingImport />
          </Route>
          <Route exact path="/penerimaan">
            <Receiving />
          </Route>
          <Route exact path="/penerimaan-detail">
            <ReceivingDetail />
          </Route>
          <Route exact path="/penerimaan-import">
            <ReceivingImport />
          </Route>
          <Route exact path="/konfigurasi-sistem">
            <KonfigurasiSistem />
          </Route>
          <Route exact path="/laporan">
            <Laporan />
          </Route>
          <Route exact path="/user">
            <User />
          </Route>
          <Route exact path="/login">
            <Login />
          </Route>
          <Route exact path="/pemindahan">
            <Transfer />
          </Route>
          <Route exact path="/pemindahan-detail">
            <TransferDetail />
          </Route>
          <Route exact path="/pemindahan-import">
            <TransferImport />
          </Route>
          <Route component={NoMatch} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
