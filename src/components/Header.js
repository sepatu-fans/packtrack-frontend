// Sidebar.js
import React, {Component} from 'react';
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';
import md5 from 'md5'
import DropdownList from 'react-widgets/lib/DropdownList'

export default class Sidebar extends Component {
    state = {
        show : false,
        show1 : false,
        locations : [],
        location : false
    }

    constructor(props) {
        super(props);
        if(localStorage.getItem("IS-LOGIN") !== "1") {
            window.location.href = "/login";
        }
    }

    handleClose = () => this.setState({show:false});
    handleShow = () => {
        this.setState({show:true});
    };
    handleClose1 = () => this.setState({show1:false});
    handleShow1 = () => {
        var locations = JSON.parse(localStorage.getItem("LOCATIONS"));
        var location = false;
        locations.forEach((e) => {
            if(e.id.toString() === localStorage.getItem("LOCATION_ID")) {
                location = e;
            }
        })
        this.setState({show1:true, locations: locations, location: location});
    };

    logOut() {
        localStorage.setItem("IS-LOGIN", "0");
        window.location.href = "/login";
    }

    handleSave1() {
        if(!this.state.location) {
            alert("Lokasi harus dipilih");
            return;
        }

        var data = {
            id:localStorage.getItem("USERID"),
            lokasi:this.state.location.id
        }
        axios.post(process.env.REACT_APP_SERVER_URL+'/set-user-location', data)
            .then(response => response.data)
            .then((res) => {
                if(res.status) {
                    localStorage.setItem("LOCATION_NAME",this.state.location.nama_site + " - " + this.state.location.nama);
                    localStorage.setItem("LOCATION_ID",this.state.location.id);
                    window.location.reload();
                }else {
                    alert(res.error);
                }
            }
        );
    }

    handleSave() {
        if(!this.state.password) {
            alert('Password harus diisi');
            return;
        }
        if(this.state.password !== this.state.repassword) {
            alert('Password dan Ulangi Password tidak sama');
            return;
        }

        var data = {
            username:localStorage.getItem("USERNAME"),
            password:md5(this.state.password)
        }
        axios.post(process.env.REACT_APP_SERVER_URL+'/user-change-password', data)
            .then(response => response.data)
            .then((res) => {
                    // console.log(res);
                if(res.status) {
                    alert("Password berhasil diubah");    
                    this.setState({
                        password : "",
                        repassword : "",
                        show : this.state.id === 0
                    })
                }else {
                    alert(res.error);
                }
            }
        );
    }
    
    render(){
        return (
            <nav className="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                <div className="container-fluid"><button className="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop" type="button"><i className="fas fa-bars"></i></button>
                    <ul className="nav navbar-nav flex-nowrap ml-auto">
                        <li className="nav-item dropdown d-sm-none no-arrow"><a href="/" className="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false"><i className="fas fa-search"></i></a>
                            <div className="dropdown-menu dropdown-menu-right p-3 animated--grow-in" role="menu" aria-labelledby="searchDropdown">
                                <form className="form-inline mr-auto navbar-search w-100">
                                    <div className="input-group">
                                        <input className="bg-light form-control border-0 small" type="text" placeholder="Search for ..."/>
                                        <div className="input-group-append"><button className="btn btn-primary py-0" type="button"><i className="fas fa-search"></i></button></div>
                                    </div>
                                </form>
                            </div>
                        </li>
                        {/* <li className="nav-item dropdown no-arrow mx-1" role="presentation">
                            <div className="nav-item dropdown no-arrow"><a href="/" className="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false"><span className="badge badge-danger badge-counter">3+</span><i className="fas fa-bell fa-fw"></i></a>
                                <div className="dropdown-menu dropdown-menu-right dropdown-list dropdown-menu-right animated--grow-in"
                                    role="menu">
                                    <h6 className="dropdown-header">alerts center</h6>
                                    <a href="/" className="d-flex align-items-center dropdown-item">
                                        <div className="mr-3">
                                            <div className="bg-primary icon-circle"><i className="fas fa-file-alt text-white"></i></div>
                                        </div>
                                        <div><span className="small text-gray-500">December 12, 2019</span>
                                            <p>A new monthly report is ready to download!</p>
                                        </div>
                                    </a>
                                    <a href="/" className="d-flex align-items-center dropdown-item">
                                        <div className="mr-3">
                                            <div className="bg-success icon-circle"><i className="fas fa-donate text-white"></i></div>
                                        </div>
                                        <div><span className="small text-gray-500">December 7, 2019</span>
                                            <p>$290.29 has been deposited into your account!</p>
                                        </div>
                                    </a>
                                    <a href="/" className="d-flex align-items-center dropdown-item">
                                        <div className="mr-3">
                                            <div className="bg-warning icon-circle"><i className="fas fa-exclamation-triangle text-white"></i></div>
                                        </div>
                                        <div><span className="small text-gray-500">December 2, 2019</span>
                                            <p>Spending Alert: We've noticed unusually high spending for your account.</p>
                                        </div>
                                    </a>
                                    <a href="/" className="text-center dropdown-item small text-gray-500">Show All Alerts</a></div>
                            </div>
                        </li> */}
                        {/* <li className="nav-item dropdown no-arrow mx-1" role="presentation">
                            <div className="nav-item dropdown no-arrow">
                                <a href="/" className="dropdown-toggle nav-link d-none" data-toggle="dropdown" aria-expanded="false"><i className="fas fa-envelope fa-fw"></i><span className="badge badge-danger badge-counter">7</span></a>
                                <div className="dropdown-menu dropdown-menu-right dropdown-list dropdown-menu-right animated--grow-in"
                                    role="menu">
                                    <h6 className="dropdown-header">alerts center</h6>
                                    <a href="/" className="d-flex align-items-center dropdown-item">
                                        <div className="dropdown-list-image mr-3">
                                            <img alt="" className="rounded-circle" src="img/avatars/avatar4.jpeg"/>
                                            <div className="bg-success status-indicator"></div>
                                        </div>
                                        <div className="font-weight-bold">
                                            <div className="text-truncate"><span>Hi there! I am wondering if you can help me with a problem I've been having.</span></div>
                                            <p className="small text-gray-500 mb-0">Emily Fowler - 58m</p>
                                        </div>
                                    </a>
                                    <a href="/" className="d-flex align-items-center dropdown-item">
                                        <div className="dropdown-list-image mr-3">
                                            <img alt="" className="rounded-circle" src="img/avatars/avatar2.jpeg"/>
                                            <div className="status-indicator"></div>
                                        </div>
                                        <div className="font-weight-bold">
                                            <div className="text-truncate"><span>I have the photos that you ordered last month!</span></div>
                                            <p className="small text-gray-500 mb-0">Jae Chun - 1d</p>
                                        </div>
                                    </a>
                                    <a href="/" className="d-flex align-items-center dropdown-item">
                                        <div className="dropdown-list-image mr-3">
                                            <img alt="" className="rounded-circle" src="img/avatars/avatar3.jpeg"/>
                                            <div className="bg-warning status-indicator"></div>
                                        </div>
                                        <div className="font-weight-bold">
                                            <div className="text-truncate"><span>Last month's report looks great, I am very happy with the progress so far, keep up the good work!</span></div>
                                            <p className="small text-gray-500 mb-0">Morgan Alvarez - 2d</p>
                                        </div>
                                    </a>
                                    <a href="/" className="d-flex align-items-center dropdown-item">
                                        <div className="dropdown-list-image mr-3">
                                            <img alt="" className="rounded-circle" src="img/avatars/avatar5.jpeg"/>
                                            <div className="bg-success status-indicator"></div>
                                        </div>
                                        <div className="font-weight-bold">
                                            <div className="text-truncate"><span>Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</span></div>
                                            <p className="small text-gray-500 mb-0">Chicken the Dog · 2w</p>
                                        </div>
                                    </a>
                                    <a href="/" className="text-center dropdown-item small text-gray-500">Show All Alerts</a></div>
                            </div>
                            <div className="shadow dropdown-list dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown"></div>
                        </li> */}
                        {/* <div className="d-none d-sm-block topbar-divider"></div> */}
                        <li className="nav-item dropdown no-arrow" role="presentation">
                            <div className="nav-item dropdown no-arrow">
                                <a href="/" className="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false">
                                    <span className="d-none d-lg-inline mr-2 text-gray-600 small">{localStorage.getItem("USERNAME")}</span>
                                    <span className="d-none d-lg-inline mr-2 text-gray-600 small">| <b>{localStorage.getItem("LOCATION_NAME")}</b></span>
                                    <img alt="" className="border rounded-circle img-profile" src="img/login.png"/>
                                </a>
                                <div className="dropdown-menu shadow dropdown-menu-right animated--grow-in" role="menu">
                                    <a href="##" style={{cursor:'pointer'}} onClick={()=>this.handleShow()} className="dropdown-item" role="presentation"><i className="fas fa-key fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Ubah Password</a>
                                    <a href="##" style={{cursor:'pointer'}} onClick={()=>this.handleShow1()} className="dropdown-item" role="button"><i className="fas fa-map-marker fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Ubah Lokasi</a>
                                    {/* <a href="/" className="dropdown-item" role="presentation"><i className="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Activity log</a> */}
                                    <div className="dropdown-divider"></div>
                                    <a href="##" style={{cursor:'pointer'}} onClick={()=>this.logOut()} className="dropdown-item" role="presentation"><i className="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Logout</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>

                <Modal show={this.state.show} onHide={this.handleClose} animation={true} className="">
                    <Modal.Header closeButton>
                    <Modal.Title>Ubah Password {this.state.isLoading1 ? ' (Loading ...)' : ''}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="form-group">
                            <label htmlFor="kode-karton">Password Baru</label>
                            <input type="password" className="form-control" name="qty-label" placeholder="Password Baru" value={this.state.password} onChange={(e)=> this.setState({password:e.target.value})}/>
                            
                            <label htmlFor="kode-karton">Ulangi Password Baru</label>
                            <input type="password" className="form-control" name="qty-label" placeholder="Ulangi Password Baru" value={this.state.repassword} onChange={(e)=> this.setState({repassword:e.target.value})}/>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.handleClose()} className="btn btn-warning">Batal</button>
                        <button onClick={()=> this.handleSave()} className="btn btn-primary btn-utama">Simpan</button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.show1} onHide={this.handleClose1} animation={true} className="">
                    <Modal.Header closeButton>
                    <Modal.Title>Ubah Lokasi{this.state.isLoading1 ? ' (Loading ...)' : ''}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="form-group">
                            <DropdownList
                                data={this.state.locations}
                                textField={item => item === undefined ? "Pilih Site" : item.nama_site + " - " + item.nama}
                                filter
                                busy={this.state.isLoading2}
                                onChange={value => this.setState({location:value})}
                                value={this.state.location}
                            />
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.handleClose1()} className="btn btn-warning">Batal</button>
                        <button onClick={()=> this.handleSave1()} className="btn btn-primary btn-utama">Simpan</button>
                    </Modal.Footer>
                </Modal>
            </nav>
        )
    }
}
