// Sidebar.js
import React, {Component} from 'react';
import Helper from '../Helper'
const $ = window.$;

export default class Sidebar extends Component {

    state = {
        access : []
    }

    componentDidMount() {
        $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
            $("body").toggleClass("sidebar-toggled");
            $(".sidebar").toggleClass("toggled");
            if ($(".sidebar").hasClass("toggled")) {
                $('.sidebar .collapse').collapse('hide');
            };
        });
        // Smooth scrolling using jQuery easing
        $(document).on('click', 'a.scroll-to-top', function(e) {
            var $anchor = $(this);
            $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top)
            }, 1000, 'easeInOutExpo');
            e.preventDefault();
        });
    }

    render(){
        return (
        <nav className="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0" style={{backgroundColor: '#179bde',backgroundImage: 'url("#179bde")'}}>
            <div className="container-fluid d-flex flex-column p-0">
                <a href="/" className="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0">
                    <div className="sidebar-brand-text mx-3"><img alt="" className="side-logo w-100 pl-0" src="img/white-logo.svg"/></div>
                </a>
                <hr className="sidebar-divider my-0"/>
                <ul className="nav navbar-nav text-light" id="accordionSidebar">
                    <li style={{display:Helper.getAccessStatus('Beranda', 'view') ? 'inline' : 'none'}} className="nav-item" role="presentation"><a className={"nav-link " + (this.props.active === 'home' ? 'active' : '')} href="/"><i className="fas fa-tachometer-alt"></i><span>Beranda</span></a></li>
                    <li style={{display:Helper.getAccessStatus('Pengiriman Produk', 'view') ? 'inline' : 'none'}} className="nav-item" role="presentation"><a className={"nav-link " + (this.props.active === 'shipping' ? 'active' : '')} href="/pengiriman"><i className="fas fa-truck-moving"></i><span>Pengiriman Produk</span></a></li>
                    <li style={{display:Helper.getAccessStatus('Penerimaan Produk', 'view') ? 'inline' : 'none'}} className="nav-item" role="presentation"><a className={"nav-link " + (this.props.active === 'receiving' ? 'active' : '')} href="/penerimaan"><i className="fas fa-backward"></i><span>Penerimaan Produk</span></a></li>
                    <li style={{display:Helper.getAccessStatus('Pemindahan Langsung', 'view') ? 'inline' : 'none'}} className="nav-item" role="presentation"><a className={"nav-link " + (this.props.active === 'transfer' ? 'active' : '')} href="/pemindahan"><i className="fas fa-luggage-cart"></i><span>Pemindahan Langsung</span></a></li>
                    <li>
                        <hr style={{color: "white",backgroundColor: "#17a7f1",height: "5px"}}/>
                    </li>
                    <li style={{display:Helper.getAccessStatus(['Pengisian Produk - Grade A','Pengisian Produk - Grade Other','Pengisian Produk - No Box'], 'view') ? 'block' : 'none'}} className="nav-item" role="presentation">
                        <div><a className={"btn btn-link nav-link " + (this.props.active === 'grades-a' || this.props.active === 'grades-other' || this.props.active === 'nobox' ? 'active' : '')} data-toggle="collapse" aria-expanded="true" aria-controls="collapse-1" href="#collapse-1" role="button"><i className="far fa-list-alt"></i>&nbsp;<span>Pengisian Produk</span></a>
                            <div className={"collapse " + (this.props.active === 'grades-a' || this.props.active === 'grades-other' || this.props.active === 'nobox' ? 'show' : '')} id="collapse-1">
                                <div className="bg-white border rounded py-2 collapse-inner">
                                    <h6 className="collapse-header">Pilih Grade</h6>
                                    <a style={{display:Helper.getAccessStatus('Pengisian Produk - Grade A', 'view') ? 'block' : 'none'}} className={"collapse-item " + (this.props.active === 'grades-a' ? 'active' : '')} href="/grades-a">Grade A</a>
                                    <a style={{display:Helper.getAccessStatus('Pengisian Produk - Grade Other', 'view') ? 'block' : 'none'}} className={"collapse-item " + (this.props.active === 'grades-other' ? 'active' : '')} href="/grades-other">Non-Grade A</a>
                                    <a style={{display:Helper.getAccessStatus('Pengisian Produk - No Box', 'view') ? 'block' : 'none'}} className={"collapse-item " + (this.props.active === 'nobox' ? 'active' : '')} href="/nobox">No Box</a></div>
                            </div>
                        </div>
                    </li>
                    <li style={{display:Helper.getAccessStatus('Laporan', 'view') ? 'inline' : 'none'}} className="nav-item" role="presentation"><a className={"nav-link " + (this.props.active === 'laporan' ? 'active' : '')} href="/laporan"><i className="fas fa-file-excel"></i><span>Laporan</span></a></li>
                    <li>
                        <hr style={{color: "white",backgroundColor: "#17a7f1",height: "5px"}}/>
                    </li>
                    <li style={{display:Helper.getAccessStatus('Data Produk', 'view') ? 'inline' : 'none'}} className="nav-item" role="presentation"><a className={"nav-link " + (this.props.active === 'items' ? 'active' : '')} href="/items">
                        <i className="fas fa-list-alt"></i><span>Data Produk</span></a>
                    </li>
                    {/* <li style={{display:Helper.getAccessStatus('Kelompok Produk', 'view') ? 'inline' : 'none'}} className="nav-item" role="presentation"><a className="nav-link" href="manajemen-produk.html"><i className="fas fa-boxes"></i><span>Kelompok Produk</span></a></li> */}
                    <li style={{display:Helper.getAccessStatus('Profil Karton', 'view') ? 'inline' : 'none'}} className="nav-item" role="presentation"><a className={"nav-link " + (this.props.active === 'profil-karton' ? 'active' : '')} href="/profil-karton"><i className="fas fa-inbox"></i><span>Profil Karton</span></a></li>
                    <li style={{display:Helper.getAccessStatus(["Manajemen Label Inner","Manajemen Label Outer","Open Close Box"], 'view') ? 'block' : 'none'}} className="nav-item" role="presentation">
                        <div><a className={"btn btn-link nav-link " + (this.props.active === 'manajement-labels' || this.props.active === 'open-close-box' ? 'active' : '')} data-toggle="collapse" aria-expanded="true" aria-controls="collapse-2" href="#collapse-2" role="button"><i className="far fa-list-alt"></i>&nbsp;<span>Pengaturan Label</span></a>
                            <div className={"collapse " + (this.props.active === 'manajement-labels' || this.props.active === 'open-close-box' ? 'show' : '')} id="collapse-2">
                                <div className="bg-white border rounded py-2 collapse-inner">
                                    <h6 className="collapse-header">Pilih Menu</h6>
                                    <a style={{display:Helper.getAccessStatus(["Manajemen Label Inner","Manajemen Label Outer"], 'view') ? 'block' : 'none'}} className={"collapse-item " + (this.props.active === 'manajement-labels' ? 'active' : '')} href="/manajement-labels">Manajemen Label</a>
                                    <a style={{display:Helper.getAccessStatus('Open Close Box', 'view') ? 'block' : 'none'}} className={"collapse-item " + (this.props.active === 'open-close-box' ? 'active' : '')} href="/open-close-box">Open Close Box</a></div>
                            </div>
                        </div>
                    </li>
                    {/* <li style={{display:Helper.getAccessStatus(["Manajemen Label Inner","Manajemen Label Outer"], 'view') ? 'inline' : 'none'}} className="nav-item" role="presentation"><a className={"nav-link " + (this.props.active === 'manajement-labels' ? 'active' : '')} href="manajement-labels"><i className="fas fa-qrcode"></i><span>Manajemen Label</span></a></li> */}
                    <li style={{display:Helper.getAccessStatus('Manajemen User', 'view') ? 'inline' : 'none'}} className="nav-item" role="presentation"><a className={"nav-link " + (this.props.active === 'user' ? 'active' : '')} href="user"><i className="fas fa-users"></i><span>Manajemen User</span></a></li>
                    <li style={{display:Helper.getAccessStatus(["Konfigurasi Sistem - Manufaktur","Konfigurasi Sistem - Ukuran","Konfigurasi Sistem - Kategori","Konfigurasi Sistem - Warna","Konfigurasi Sistem - Role Pengguna","Konfigurasi Sistem - Prefix Label","Konfigurasi Sistem - Site","Konfigurasi Sistem - Lokasi"], 'view') ? 'inline' : 'none'}} className="nav-item" role="presentation"><a className={"nav-link " + (this.props.active === 'konfigurasi-sistem' ? 'active' : '')} href="konfigurasi-sistem"><i className="fas fa-cog"></i><span>Konfigurasi Sistem</span></a></li>
                </ul>
                <div className="text-center d-none d-md-inline"><button className="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
        )
    }
}
