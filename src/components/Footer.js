// Header.js
import React, {Component} from 'react';

export default class Header extends Component {

    componentDidMount() {
    }

    render(){
        return (
        <footer className="bg-white sticky-footer">
            <div className="container my-auto">
                <div className="text-center my-auto copyright"><span>Copyright © ​​PackTrack Fans {new Date().toISOString().substr(0,4)} Build {process.env.REACT_APP_LAST_COMMIT_ID} {process.env.REACT_APP_LAST_COMMIT_DATE}</span></div>
            </div>
        </footer>
        )
    }
}
