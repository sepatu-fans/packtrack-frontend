import { savePDF } from '@progress/kendo-react-pdf';

class DocService {
  createPdf = (html, title) => {
    console.log(html)
    this.setState({isLoading:true})
    savePDF(html, { 
      paperSize: 'A8',
      fileName: title + '.pdf',
      margin: 0,
      landscape: true
    }, () => {
      alert("Done")
    })
  }
}

const Doc = new DocService();
export default Doc;