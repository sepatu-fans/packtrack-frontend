import React from 'react';

export default (props) => {
  const bodyRef = React.createRef();
  const createPdf = () => props.createPdf(bodyRef.current);
  const kembali = () => props.kembali(bodyRef.current);
  
  return (
    <section className="pdf-container">
      <section className="pdf-toolbar">
        <button className="btn btn-warning" onClick={kembali}><i className="fa fa-long-arrow-left"></i>&nbsp;Kembali</button>&nbsp;
        {this.isLoading ? "Loading ..." : <button id="btnCetak" className="btn btn-danger" onClick={createPdf}><i className="fa fa-print"></i>&nbsp;Cetak</button>}
        <hr/>
      </section>
      <section className="pdf-body" ref={bodyRef} >
        {props.children}
      </section>
    </section>
  )
}