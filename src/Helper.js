import Moment from 'moment';

function getAccessStatus(e,f) {
    var result = ""
    if(typeof e === 'string') {
        result = localStorage.getItem(e + "-" + f);
        return result === "1";
    }else {
        result = "0";
        e.forEach(element => {
            var access = localStorage.getItem(element + "-" + f);
            if(result === "0") {
                result = access;
            }
        });

        return result === "1";
    }
}

function getCurrentTime(date, offset = 0) {

    if(date === undefined || date === null) {
        date = new Date().toISOString()
    }
    return Moment(date).utcOffset (offset).format("YYYY-MM-DD HH:mm:ss")
}

function getCurrentDate(date, offset = 0) {

    if(date === undefined || date === null) {
        date = new Date().toISOString()
    }
    return Moment(date).utcOffset (offset).format("YYYY-MM-DD")
}

export default {
    getAccessStatus,
    getCurrentTime,
    getCurrentDate
};