// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';
import Helper from '../Helper'
import BaseComponent from '../components/BaseComponent'

export default class ProfilKarton extends Component {

    render(){
        return (
        <Fragment>
            <Sidebar active="profil-karton"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <ProfilKartonSub />
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class ProfilKartonSub extends BaseComponent {
    
    pageTitle = "Profil Karton"

    state = {
        items: [],
        size : 10,
        page : 1,
        pages : [1],
        isLoading : false,
        search : '',
        show : false,
    }

    role_name = "Profil Karton"

    handleClose = () => this.setState({show:false});
    handleShow = () => this.setState({show:true});

    sizes = [
        10,20,50,100
    ]

    componentDidMount() {
        super.componentDidMount()
        if(!Helper.getAccessStatus(this.role_name, "view")) {
            window.location.href = "/notfound";
        }
        this.loadData();
    }

    loadData() {
        this.setState({isLoading:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/profil-karton?page=' + this.state.page + '&size=' + this.state.size + '&search=' + this.state.search)
            .then(res => res.json())
            .then((data) => {
            this.setState({ items: data.data, pages: data.pages })
            this.setState({isLoading:false})
        })
        .catch(console.log)
    }

    handleChange = (event) => {
        console.log(event.target.value);
        this.setState({ size: event.target.value }, function() {
            this.loadData();
        });
    };

    searchChange = (event) => {
        console.log(event.target.value);
        this.setState({search:event.target.value,page:1}, function() {
            this.loadData();
        });
    }

    pageChange = (event) => {
        console.log(event.target.getAttribute('data'));
        this.setState({ page: event.target.getAttribute('data') }, function() {
            this.loadData();
        });
    };

    delete = (id) => {
        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            var data = {
                id:id
            }
            axios.post(process.env.REACT_APP_SERVER_URL+'/profil-karton-delete', data)
                .then(response => response.data)
                .then((res) => {
                        // console.log(res);
                        if(res.status) {
                            // alert("Data berhasil dihapus");  
                            this.loadData();
                        }else {
                            alert(res.error);
                        }
                    }
                );
        }
    }

    getJumlahByPLU(plu) {
        if(plu === '' || plu === null) {
            return 0;
        }

        var plus = JSON.parse(plu);
        var total = 0;
        plus.forEach(e => {
            total += e[1];
        })

        return total;
    }

    getJumlahBySize(size) {
        if(size === '' || size === null) {
            return 0;
        }

        var sizes = JSON.parse(size);
        var total = 0;
        sizes.forEach(e => {
            total += e[1];
        })

        return total;
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/profil-karton"><span className="bc-link">Profil Karton</span></a></li>
                        </ol>
                    </div>
                    <div className="col text-right">
                        {
                        !Helper.getAccessStatus(this.role_name, "add") ? null : 
                        <button onClick={()=>{this.handleShow()}} className="btn btn-primary btn-utama" id="btn-grade-a-requirement">Tambah</button>
                        }
                    </div>
                </div>
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <h6 className="text-primary m-0 font-weight-bold">Daftar Profil Karton</h6>
                        </div>
                        <div className="card-body">
                            <div className="row my-3">
                                <div className="col">
                                    <select className="custom-select max-view mb-3" defaultValue={this.state.size} onChange={this.handleChange} >
                                        {this.sizes.map((item) => (
                                            <option key={item} value={item}>{item} baris</option>
                                        ))}
                                    </select>
                                </div>
                                <div className="col">
                                    <div className="input-group">
                                        <input onChange={this.searchChange} className="form-control" type="text" placeholder="Cari Produk" name="cari-produk"/>
                                        <div className="input-group-append"><button onClick={this.handleSearch} className="btn btn-info" type="button"><i className="fa fa-search"></i></button></div>
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                <table className="table dataTable my-0" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Nama Profil</th>
                                            <th>Tipe</th>
                                            <th>Jumlah</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.isLoading ? (
                                            <tr>
                                                <td colSpan="8">
                                                    Loading
                                                </td>
                                            </tr>
                                        ) : 
                                        this.state.items.map((item) => (
                                            <tr key={item.id}>
                                                <td>{item.nama}</td>
                                                <td>
                                                    {item.tipe === 1 ? 'Solid' : null}
                                                    {item.tipe === 2 ? 'Assort' : null}
                                                    {item.tipe === 3 ? 'Spesific' : null}
                                                    {item.tipe === 4 ? 'Mix' : null}
                                                </td>
                                                <td>
                                                    {item.tipe === 3 ? this.getJumlahBySize(item.plu) : null}
                                                    {item.tipe === 2 ? this.getJumlahByPLU(item.size) : null}
                                                    {item.tipe === 1 || item.tipe === 4 ? item.jumlah : null}
                                                </td>
                                                <td>
                                                    {Helper.getAccessStatus(this.role_name, "edit") ? <a href={"/profil-karton-add?id=" + item.id} className="m-2 link-green">Edit</a> : null}
                                                    {Helper.getAccessStatus(this.role_name, "delete") ? <a onClick={()=>this.delete(item.id)} href="##" className="m-2 link-red">Hapus</a> : null}
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                            <div className="row">
                                <div className="col-md-6 align-self-center">
                                    <div></div>
                                    <div className="mb-4">
                                        {/* <a href="/profil-karton-import" className="btn btn-success mr-2" role="button">Impor Data</a> */}
                                        {/* <a href="/profil-karton" className="btn btn-danger mr-2" role="button">Ekspor Data</a> */}
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <nav className="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers" style={{'float':'right'}}>
                                        <ul className="pagination">
                                            <li className={"page-item " + (this.state.pages[0] === 1 ? "disabled" : "")}><button data={this.state.pages[0]-1} className="page-link" aria-label="Previous" onClick={this.pageChange}>«</button></li>
                                            {this.state.pages.map((page) => (
                                                <li key={page} className={"page-item " + ((parseInt(this.state.page) === page ? "active" : ""))}><button data={page} className="page-link" onClick={this.pageChange}>{page}</button></li>
                                            ))}
                                            <li className="page-item"><button data={this.state.pages[this.state.pages.length-1]+1} className="page-link" aria-label="Next" onClick={this.pageChange}>»</button></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
                <Modal show={this.state.show} onHide={this.handleClose} animation={true} className="">
                    <Modal.Header closeButton>
                    <Modal.Title>Pilih Tipe Karton</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div class="d-flex" style={{height: "85px"}}>
                            <a class="btn btn-light border rounded d-flex d-xl-flex justify-content-center align-items-center justify-content-xl-center align-items-xl-center mx-2 w-100" role="button" href="/profil-karton-add?type=1">Solid</a>
                            <a class="btn btn-light border rounded d-flex d-xl-flex justify-content-center align-items-center justify-content-xl-center align-items-xl-center mx-2 w-100" role="button" href="/profil-karton-add?type=2">Assort</a>
                        </div>
                        <br/>
                        <div class="d-flex" style={{height: "85px"}}>
                            <a class="btn btn-light border rounded d-flex d-xl-flex justify-content-center align-items-center justify-content-xl-center align-items-xl-center mx-2 w-100" role="button" href="/profil-karton-add?type=3">Spesific</a>
                            <a class="btn btn-light border rounded d-flex d-xl-flex justify-content-center align-items-center justify-content-xl-center align-items-xl-center mx-2 w-100" role="button" href="/profil-karton-add?type=4">Mix</a>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.handleClose()} className="btn btn-warning">Batal</button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}