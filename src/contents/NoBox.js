// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BaseComponent from '../components/BaseComponent'
import axios from 'axios';
import Helper from '../Helper'

export default class NoBox extends Component {

    render(){
        return (
        <Fragment>
            <Sidebar active="nobox"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <NoBoxSub />
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class NoBoxSub extends BaseComponent {

    pageTitle = 'Daftar Produk No Box'
    
    state = {
        profil_kartons: [],
        items: [],
        size : 10,
        page : 1,
        pages : [1],
        isLoading : false,
        isLoading1 : false,
        search : '',
        show : false,
        innerbox: ''
    }

    role_name = "Pengisian Produk - No Box"

    handleClose = () => this.setState({show:false});
    handleShow = () => {
        this.setState({show:true});
    };

    sizes = [
        10,20,50,100
    ]

    componentDidMount() {
        super.componentDidMount()
        if(!Helper.getAccessStatus(this.role_name, "view")) {
            window.location.href = "/notfound";
        }
        this.loadData();
    }

    loadData() {
        this.setState({isLoading:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/nobox?page=' + this.state.page + '&size=' + this.state.size + '&search=' + this.state.search + '&lokasi=' + localStorage.getItem("LOCATION_ID"))
            .then(res => res.json())
            .then((data) => {
            this.setState({ items: data.data, pages: data.pages })
            this.setState({isLoading:false})
        })
        .catch(console.log)
    }

    handleChange = (event) => {
        console.log(event.target.value);
        this.setState({ size: event.target.value }, function() {
            this.loadData();
        });
    };

    searchChange = (event) => {
        console.log(event.target.value);
        this.setState({search:event.target.value,page:1}, function() {
            this.loadData();
        });
    }

    pageChange = (event) => {
        console.log(event.target.getAttribute('data'));
        this.setState({ page: event.target.getAttribute('data') }, function() {
            this.loadData();
        });
    };

    delete = (id) => {
        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            var data = {
                id:id
            }
            axios.post(process.env.REACT_APP_SERVER_URL+'/nobox-delete', data)
                .then(response => response.data)
                .then((res) => {
                        // console.log(res);
                        if(res.status) {
                            // alert("Data berhasil dihapus");  
                            this.loadData();
                        }else {
                            alert(res.error);
                        }
                    }
                );
        }
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/nobox"><span className="bc-link">Daftar No Box</span></a></li>
                        </ol>
                    </div>
                    <div className="col text-right">
                        {Helper.getAccessStatus(this.role_name, "add") ? <a role="button" href="/nobox-detail" className="btn btn-primary btn-utama" id="btn-grade-a-requirement">Tambah</a> : null}
                    </div>
                </div>
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <h6 className="text-primary m-0 font-weight-bold">Daftar Produk No Box</h6>
                        </div>
                        <div className="card-body">
                            <div className="row my-3">
                                <div className="col">
                                    <select className="custom-select max-view mb-3" defaultValue={this.state.size} onChange={this.handleChange} >
                                        {this.sizes.map((item) => (
                                            <option key={item} value={item}>{item} baris</option>
                                        ))}
                                    </select>&nbsp;
                                </div>
                                <div className="col">
                                    <div className="input-group">
                                        <input onChange={this.searchChange} className="form-control" type="text" placeholder="Cari Produk" name="cari-produk"/>
                                        <div className="input-group-append"><button onClick={this.handleSearch} className="btn btn-info" type="button"><i className="fa fa-search"></i></button></div>
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                <table className="table dataTable my-0" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Tanggal</th>
                                            <th>Grade</th>
                                            <th>Nama Produk</th>
                                            <th>Barcode</th>
                                            <th>Warna</th>
                                            <th>Ukuran</th>
                                            <th>Innberbox</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.isLoading ? (
                                            <tr>
                                                <td colSpan="9">
                                                    Loading
                                                </td>
                                            </tr>
                                        ) : 
                                        this.state.items.map((item) => (
                                            <tr key={item.id}>
                                                <td>{Helper.getCurrentTime(item.last_trans)}</td>
                                                <td>{item.grade_name}</td>
                                                <td>{item.name}</td>
                                                <td>{item.barcode}</td>
                                                <td>{item.color}</td>
                                                <td>{item.size}</td>
                                                <td>{item.serial_no}</td>
                                                <td>
                                                    {!item.is_latest ? null : 
                                                        <a onClick={()=>this.delete(item.id)} href="##" className="m-2 link-red">Hapus</a>
                                                    }
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                            <div className="row">
                                <div className="col-md-6 align-self-center">
                                    <div></div>
                                    <div className="mb-4">
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <nav className="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers" style={{'float':'right'}}>
                                        <ul className="pagination">
                                            <li className={"page-item " + (this.state.pages[0] === 1 ? "disabled" : "")}><button data={this.state.pages[0]-1} className="page-link" aria-label="Previous" onClick={this.pageChange}>«</button></li>
                                            {this.state.pages.map((page) => (
                                                <li key={page} className={"page-item " + ((parseInt(this.state.page) === page ? "active" : ""))}><button data={page} className="page-link" onClick={this.pageChange}>{page}</button></li>
                                            ))}
                                            <li className="page-item"><button data={this.state.pages[this.state.pages.length-1]+1} className="page-link" aria-label="Next" onClick={this.pageChange}>»</button></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}