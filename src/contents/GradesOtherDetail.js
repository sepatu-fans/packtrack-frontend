// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BaseComponent from '../components/BaseComponent'
import axios from 'axios';
import queryString from 'query-string';
import Helper from '../Helper'

export default class GradesOtherDetail extends Component {
    constructor(props) {
      super(props);
      
      this.params = queryString.parse(window.location.search)
    }

    render(){
        return (
        <Fragment>
            <Sidebar active="grades-other"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <GradesOtherDetailSub params={this.params}/>
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class GradesOtherDetailSub extends BaseComponent {
    pageTitle = 'Input Produk Non-Grade A'
    state = {
        id : 0,
        gradedata: {
            grade_name: '',
            serial_no: ''
        },
        details: [],
        deletes : [], 
        isLoading1 : false,
        isLoading3 : false,
        innerbox : '',
        show : false,
    }

    isDisable = false;

    handleClose = () => this.setState({show:false});
    handleShow = () => {
        this.setState({show:true});
    };

    constructor(props) {
        super(props);
        this.params = props.params;
        console.log(this.params);

        if(this.params.id !== undefined && this.params.id !== '0') {
            if(!Helper.getAccessStatus(['Pengisian Produk - Grade Other'], "edit")) {
                window.location.href = "/notfound";
            }

            this.state.isEdit = true;
            this.state.id = this.params.id;

            fetch(process.env.REACT_APP_SERVER_URL + '/grades-other-by-id?id=' + this.params.id)
                .then(res => res.json())
                .then((res) => {
                    if(res.status) {
                        var details = []
                        res.data.details.forEach((e) => {
                            if(e.status === 1) {
                                details.push(e);
                            }
                        })
                        this.setState({
                            'gradedata':res.data.grade,
                            'details' : details,
                        });
                    }else {
                        alert(res.error);
                        window.history.back();  
                    }
                })
            .catch(console.log)
            
        }else {
            if(!Helper.getAccessStatus(['Pengisian Produk - Grade Other'], "add")) {
                window.location.href = "/notfound";
            }
            fetch(process.env.REACT_APP_SERVER_URL + '/grades-by-id?id=' + this.params.grade)
                .then(res => res.json())
                .then((res) => {
                    if(res.status) {
                        var grade = {
                            id : this.params.id,
                            outerbox : this.params.outerbox,
                            serial_no : this.params.serial_no,
                            grade : this.params.grade,
                            grade_name : res.data.nama,
                            details:[]
                        }
                        console.log(grade);
                        this.setState({'gradedata':grade,'details' : []})
                    }else {
                        alert(res.error);
                        window.history.back();  
                    }
                })
            .catch(console.log)
        }
    }

    componentDidMount() {
        super.componentDidMount()
    }
    

    delete = (serial) => {

        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            var newdetails = [];
            var deletes = this.state.deletes;
            this.state.details.forEach(elem => {
                if(elem.innerbox === serial) {
                    deletes.push(elem.innerbox);
                }else {
                    newdetails.push(elem);
                }
            });
            console.log(deletes);
            console.log(newdetails);
            this.setState({'details':newdetails,'deletes':deletes});
        }
    }
    
    loadInnerLabel() {
        if(this.isDisable) {
            return
        }
        this.isDisable = true;

        if(this.state.innerbox === '') {
            alert('Serial innerbox harus diisi');
            this.isDisable = false;
            return;
        }
        var isSkip = false;
        this.state.details.forEach(elem => {
            if(elem.serial_no === this.state.innerbox) {
                alert("Data sudah pernah dimasukkan");
                this.setState({'innerbox':''});
                isSkip = true;
                return;
            }
        });
        if(isSkip) {
            this.isDisable = false;
            return;
        }
        this.setState({isLoading3:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/labels-inner-by-code?code=' + this.state.innerbox)
            .then(res => res.json())
            .then((res) => {
                if(res.status) {
                    var innerboxid = res.data.innerbox;
                    var data = res.data;

                    fetch(process.env.REACT_APP_SERVER_URL + '/grades-innerbox-check?innerbox=' + innerboxid + '&gradea=OTHER-' + this.state.id)
                        .then(res => res.json())
                        .then((res) => {
                            this.setState({isLoading3:false})
                            if(res.status) {
                                var details = []
                                var deletes = []
                                if(this.state.gradedata.grade !== undefined) {
                                    details = this.state.details;
                                    details.push(data);
                                    deletes = this.state.deletes;
                                    if(deletes.indexOf(innerboxid) >= 0) {
                                        deletes.splice(deletes.indexOf(innerboxid), 1);
                                    }
                                    this.setState({'details':details,'deletes':deletes,'innerbox':''});
                                }else {
                                    details = this.state.details;
                                    details.push(data);
                                    deletes = this.state.deletes;
                                    if(deletes.indexOf(innerboxid) >= 0) {
                                        deletes.splice(deletes.indexOf(innerboxid), 1);
                                    }
                                    this.setState({'details':details,'deletes':deletes,'innerbox':''});
                                }
                                this.isDisable = false;
                            }else {
                                alert(res.error);
                                this.setState({'innerbox':''});
                                this.isDisable = false;
                            }
                        })
                    .catch(console.log)
                }else {
                    alert(res.error);
                    this.setState({'innerbox':''});
                    this.setState({isLoading3:false})
                    this.isDisable = false;
                }
            })
        .catch(console.log)
    }

    loadOuterLabel() {
        this.setState({isLoading1:true})
        if(this.state.profil_karton === '') {
            alert('Profil Karton harus diisi');
            return;
        }
        fetch(process.env.REACT_APP_SERVER_URL + '/labels-outer-by-code?code=' + this.state.outerbox)
            .then(res => res.json())
            .then((res) => {
                this.setState({isLoading1:false})
                if(res.status) {
                    var outerboxid = res.data.id;
                    fetch(process.env.REACT_APP_SERVER_URL + '/grades-outerbox-check?outerbox=' + outerboxid)
                        .then(res => res.json())
                        .then((res) => {
                            this.setState({isLoading1:false})
                            if(res.status) {
                                this.setState({
                                    outerbox_id:outerboxid
                                })
                            }else {
                                alert(res.error);
                            }
                        })
                    .catch(console.log)
                }else {
                    alert(res.error);
                }
            })
        .catch(console.log)
    }

    save = (is_close) => {
        if(this.state.details.length === 0) {
            alert("Belum ada innerbox");
            return;
        }
        // if(this.state.outerbox_id === 0) {
        //     alert("Outerbox belum diisi");
        //     return;
        // }
        if(true) {
            var details = [];
            this.state.details.forEach((e) => {
                if(this.state.id === '0') {
                    details.push(e.innerbox);
                } else if(e.shipping_status === undefined) {
                    details.push(e.innerbox);
                }
            });
            var data = {
                grade_other:this.state.id,
                outerbox:this.state.gradedata.outerbox,
                grade:this.state.gradedata.grade,
                details:details,
                deletes:this.state.deletes,
                created_at:Helper.getCurrentTime(null, 7),
                created_by:localStorage.getItem("USERID"),
                lokasi:localStorage.getItem("LOCATION_ID"),
                is_close:is_close
            }
            axios.post(process.env.REACT_APP_SERVER_URL+'/grades-other-detail-save', data)
                .then(response => response.data)
                .then((res) => {
                        // console.log(res);
                        if(res.status) {
                            // alert("Data berhasil disimpan");
                            if(this.state.isEdit) {
                                window.location.href='/grades-other';
                            }else {
                                window.location.href='/grades-other?isadd=1';
                            }
                        }else {
                            alert(res.error);
                        }
                    }
                );
        }
    }

    onEnterPress = (f) => {
        if(f.keyCode === 13 && f.shiftKey === false) {
            f.preventDefault();
            this.loadOuterLabel()
        }
    }

    onEnterPress1 = (f) => {
        if(f.keyCode === 13 && f.shiftKey === false) {
            f.preventDefault();
            this.loadInnerLabel()
        }
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/grades-other"><span className="bc-link">Produk Non-Grade A</span></a></li>
                            <li className="breadcrumb-item"><a href="/grades-other"><span className="bc-link">Input Produk Non-Grade A</span></a></li>
                        </ol>
                    </div>
                </div>
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <h6 className="text-primary m-0 font-weight-bold">Produk</h6>
                        </div>
                        <div className="card-body">
                            <div className="row my-3">
                                <div className="col-xl-4">
                                    <h4 className="mb-4"><strong>Profil Packing</strong><br/></h4>
                                    <div className="form-group">
                                        <div className="mb-3"><label htmlFor="profil-sepatu">Grade</label><input type="text" className="form-control" readOnly value={this.state.gradedata.grade_name} name="profil-sepatu"/></div>
                                        <div className="mb-3"><label htmlFor="kode-karton">Serial Outerbox</label><input type="text" className="form-control" name="kode-karton" value={this.state.gradedata.serial_no} readOnly disabled=""/></div>
                                        <div className="mb-3">
                                            <label htmlFor="kode-produk">Serial Innerbox</label>
                                            <input autoFocus={true} onKeyDown={(f)=>this.onEnterPress1(f)} type="text" className="form-control" name="kode-produk" placeholder="Masukkan Serial Innerbox" value={this.state.innerbox} onChange={(e)=>this.setState({'innerbox':e.target.value})}/>
                                        </div>
                                        <div className="text-right mt-2">
                                            <button onClick={()=>this.loadInnerLabel()} className="btn btn-primary btn-utama" type="button">Tambah</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="col">
                                    <h4 className="mb-2"><strong>Data Inner Box</strong></h4>
                                    <p>{this.state.details.length} Innerbox dimasukkan</p>
                                    <div className="table-responsive">
                                        <table className="table">
                                            <thead>
                                                <tr style={{backgroundColor:"green"}}>
                                                    <th>Nama Produk</th>
                                                    <th>Warna</th>
                                                    <th>Ukuran</th>
                                                    <th>Innerbox</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.details.length === 0 ? (
                                                    <tr>
                                                        <td colSpan="5">Belum ada data</td>
                                                    </tr>
                                                ) : this.state.details.map((item) => (
                                                    <tr key={item.innerbox}>
                                                        <td>{item.name}</td>
                                                        <td>{item.color}</td>
                                                        <td>{item.size}</td>
                                                        <td>
                                                            {item.serial_no}
                                                        </td>
                                                        <td><a onClick={()=>this.delete(item.innerbox)} className="shadow-none text-danger" href="##"><i className="fa fa-trash-o"></i>&nbsp;Hapus</a></td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>  
                            <div className="row my-4">
                                <div className="col text-right d-flex justify-content-between">
                                    <a className="btn btn-warning" role="button" href="/grades-other">Batal</a>
                                    <div>
                                        <button className="btn btn-primary btn-utama" onClick={()=>this.save(1)}>Simpan</button>&nbsp;&nbsp;
                                        {/* <button className="btn btn-primary btn-utama" onClick={()=>this.save(1)}>Simpan dan Tutup</button> */}
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            
            </div>
        )
    }
}