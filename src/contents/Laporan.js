// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BaseComponent from '../components/BaseComponent'
import Helper from '../Helper'
import IBXTrack from './Laporan/IBXTrack'

export default class Laporan extends Component {

    render(){
        return (
        <Fragment>
            <Sidebar active="laporan"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <LaporanSub />
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class LaporanSub extends BaseComponent {
    
    pageTitle = "Laporan"

    state = {
        'ibxtrack_aktif':"0",
    }

    componentDidMount() {
        super.componentDidMount()
        if(!Helper.getAccessStatus(['Laporan - IBXTrack'], "view")) {
            // window.location.href = "/notfound";
        }
    }

    constructor(props) {
        super(props);

        if(Helper.getAccessStatus('Laporan - IBXTrack', 'view')) {
            this.state.ibxtrack_aktif= "1"
        }
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/laporan"><span className="bc-link">Laporan</span></a></li>
                        </ol>
                    </div>
                </div>
                <div>
                    <ul className="nav nav-tabs">
                        <li style={{display:Helper.getAccessStatus('Laporan - IBXTrack', 'view') ? 'inline' : 'none'}} className="nav-item"><a className={"nav-link " + (this.state.ibxtrack_aktif === "1" ? "active" : "")} role="tab" data-toggle="tab" href="#tab-1">IBXTrack</a></li>
                    </ul>
                    <div className="tab-content">
                        <IBXTrack id="tab-1" active={this.state.ibxtrack_aktif === "1" ? "active" : ""}/>
                    </div>
                </div>

            </div>
        )
    }
}