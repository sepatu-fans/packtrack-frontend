import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BaseComponent from '../components/BaseComponent'
import DropdownList from 'react-widgets/lib/DropdownList'
import axios from 'axios';
import Helper from '../Helper'
import queryString from 'query-string';

export default class ItemsAdd extends Component {
    constructor(props) {
        super(props);
        
        this.params = queryString.parse(window.location.search)
    }

    render(){
        return (
        <Fragment>
            <Sidebar active="items"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <ItemsAddSub params={this.params}/>
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class ItemsAddSub extends BaseComponent {
    state = {
        id : 0,
        colors : [],
        color : false,
        categories : [],
        category : false,
        manufaktures : [],
        manufaktur : false,
        sizes : [],
        size : false,
        name : '',
        plu : '',
        sku : '',
        data : false,
        status : 0,
        discontinue : 0
    }

    pageTitle = "Tambah Item - Data Produk"

    constructor(props) {
        super(props);
        this.params = props.params;

        if(this.params.id !== undefined) {
            this.pageTitle = "Ubah Item - Data Produk"
            if(!Helper.getAccessStatus("Data Produk", "edit")) {
                window.location.href = "/notfound";
            }
            this.state.isEdit = true;
            this.state.id = this.params.id;

            fetch(process.env.REACT_APP_SERVER_URL + '/item-by-id?id=' + this.params.id)
                .then(res => res.json())
                .then((res) => {
                    if(res.status) {
                        this.setState({
                            name:res.data.name,
                            code:res.data.code,
                            plu:res.data.barcode,
                            sku:res.data.sku,
                            sku_name:res.data.sku_name,
                            description:res.data.description,
                            data:res.data,
                            status:res.data.status,
                            discontinue:res.data.discontinue
                        })
                        this.loadColor();
                        this.loadManufaktur();
                        this.loadCategory();
                    }else {
                        alert(res.error);
                        window.history.back();  
                    }
                })
            .catch(console.log)
        }else {
            if(!Helper.getAccessStatus("Data Produk", "add")) {
                window.location.href = "/notfound";
            }
            this.loadColor();
            this.loadManufaktur();
            this.loadCategory();
        }
    }
    
    componentDidMount() {
        super.componentDidMount()
        // this.loadData();
    }

    loadColor() {
        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/color-list')
            .then(res => res.json())
            .then((res) => {
                this.setState({colors: res.data })
                if(this.state.data) {
                    res.data.forEach((e) => {
                        if(e.id === this.state.data.color) {
                            this.setState({color:e});
                            return;
                        }
                    })
                }
            })
        .catch(console.log)
    }

    loadManufaktur() {
        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/manufaktur-list')
            .then(res => res.json())
            .then((res) => {
                this.setState({manufaktures: res.data })
                if(this.state.data) {
                    res.data.forEach((e) => {
                        if(e.id === this.state.data.manufacturer) {
                            this.setState({manufaktur:e});
                            this.loadUkuran(e)
                            return;
                        }
                    })
                }
            })
        .catch(console.log)
    }

    loadCategory() {
        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/category-list')
            .then(res => res.json())
            .then((res) => {
                this.setState({categories: res.data })
                if(this.state.data) {
                    res.data.forEach((e) => {
                        if(e.id === this.state.data.category_code) {
                            this.setState({category:e});
                            return;
                        }
                    })
                }
            })
        .catch(console.log)
    }

    loadUkuran(value) {
        this.setState({manufaktur:value});
        fetch(process.env.REACT_APP_SERVER_URL + '/size-list?manufaktur=' + value.id)
            .then(res => res.json())
            .then((res) => {
                this.setState({sizes: res.data })
                if(this.state.data) {
                    res.data.forEach((e) => {
                        if(e.size === this.state.data.size) {
                            this.setState({size:e});
                            return;
                        }
                    })
                }
            })
        .catch(console.log)
    }

    getCatoryName(categories) {
        var result = "";
        if(categories === "" || categories === null) {
            return "";
        }
        categories = categories.split(",");
        this.state.categories.forEach((e) => {
            if(categories.indexOf(e.id.toString()) >= 0) {
                if(result === "") {
                    result = e.kode;
                }else {
                    result += "," + e.kode;
                }
            }
        })
        if(result !== "") {
            result = result + ",";
        }

        return result;
    }

    save() {
        if(!this.state.code) {
            alert('Item Kode harus diisi');
            return;
        }
        if(!this.state.name) {
            alert('Nama Sepatu harus diisi');
            return;
        }
        if(!this.state.sku) {
            alert('SKU harus diisi');
            return;
        }
        if(!this.state.sku_name) {
            alert('Nama SKU harus diisi');
            return;
        }
        if(!this.state.plu) {
            alert('Barcode harus diisi');
            return;
        }
        if(!this.state.color) {
            alert('Warna harus diisi');
            return;
        }
        if(!this.state.category) {
            alert('Kategori harus diisi');
            return;
        }
        if(!this.state.manufaktures) {
            alert('Manufaktur harus diisi');
            return;
        }
        if(!this.state.size) {
            alert('Ukuran harus diisi');
            return;
        }

        var data = {
            code:this.state.code,
            name:this.state.name,
            sku:this.state.sku,
            sku_name:this.state.sku_name,
            description:this.state.description,
            plu:this.state.plu,
            color:this.state.color.id,
            category_code:this.state.category.id,
            manufakturer:this.state.manufaktur.id,
            size:this.state.size.size,
            status:this.state.status,
            discontinue:this.state.discontinue,
            created_at:Helper.getCurrentTime(null, 7),
            created_by:localStorage.getItem("USERID"),
        }
        if(this.state.id !== 0) {
            data.id = this.state.id;
        }
        axios.post(process.env.REACT_APP_SERVER_URL+'/item-add', data)
            .then(response => response.data)
            .then((res) => {
                    // console.log(res);
                if(res.status) {
                    // alert("Data berhasil disimpan");  
                    window.location.href = '/items'
                }else {
                    alert(res.error);
                }
            }
        );
    }

    getSizeName(size) {
        var result = "";
        result = size.size + " (UK : " + size.uk + ",US : " + size.us + ",CM : " + size.cm + ",MM(P) : " + size.mm_p + ",MM(L) : " + size.mm_l + ",MM(G) : " + size.mm_g + ")";
        return result;
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/items"><span className="bc-link">Daftar Data Item</span></a></li>
                            <li className="breadcrumb-item"><a href="/items-add"><span className="bc-link">Tambah Item</span></a></li>
                        </ol>
                    </div>
                    <div className="col text-right"></div>
                </div>
                <div>
                    <div className="row">
                        <div className="col-lg-12 col-xl-12">
                            <div className="card shadow mb-4">
                                <div className="card-header py-3">
                                    <h6 className="text-primary m-0 font-weight-bold">Data Produk</h6>
                                </div>
                                <div className="card-body">
                                    <h4 className="mb-4"><strong>Info Produk Baru</strong></h4>
                                    <div className="form-group">
                                        <div className="mb-3">
                                            <div className="row">
                                                <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label htmlFor="nama-sepatu">Item Kode</label>
                                                    <input type="text" value={this.state.code} onChange={(e)=>this.setState({code:e.target.value})} className="form-control" name="nama-sepatu" placeholder="Mis: Black RX Jr."/>
                                                </div>
                                                <div className="col">
                                                    <label htmlFor="kode-plu">Barcode</label>
                                                    <input value={this.state.plu} onChange={(e)=>this.setState({plu:e.target.value})} type="text" className="form-control" placeholder="Masukkan Barcode Produk" name="kode-plu"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="mb-3">
                                            <div className="row">
                                                <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label htmlFor="nama-sepatu">Nama Sepatu</label>
                                                    <input type="text" value={this.state.name} onChange={(e)=>this.setState({name:e.target.value})} className="form-control" name="nama-sepatu" placeholder="Mis: Black RX Jr."/>
                                                </div>
                                                <div className="col">
                                                    <label htmlFor="kode-plu">Deskripsi</label>
                                                    <input value={this.state.description} onChange={(e)=>this.setState({description:e.target.value})} type="text" className="form-control" placeholder="Masukkan Deskripsi Produk" name="kode-plu"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="mb-3">
                                            <div className="row">
                                                <div className="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label htmlFor="sku-produk">SKU Produk</label>
                                                    <input value={this.state.sku} onChange={(e)=>this.setState({sku:e.target.value})} type="text" className="form-control" name="sku-produk" placeholder="Masukkan SKU Produk"/>
                                                </div>
                                                <div className="col">
                                                    <label htmlFor="kode-plu">Nama SKU</label>
                                                    <input value={this.state.sku_name} onChange={(e)=>this.setState({sku_name:e.target.value})} type="text" className="form-control" placeholder="Masukkan Nama SKU" name="kode-plu"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="mb-3">
                                            <div className="row">
                                                <div className="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label htmlFor="pilih-grade">Kategori Produk</label>
                                                    <DropdownList
                                                        data={this.state.categories}
                                                        textField={item => item === false ? "Pilih Kategori" : item.full_code + " " + item.nama}
                                                        filter
                                                        onChange={value => this.setState({category:value})}
                                                        value={this.state.category}
                                                    />
                                                </div>
                                                <div className="col">
                                                    <label htmlFor="pilih-grade">Manufaktur</label>
                                                    <DropdownList
                                                        data={this.state.manufaktures}
                                                        textField={item => item === false ? "Pilih Manufaktur" : item.nama}
                                                        filter
                                                        onChange={value => this.loadUkuran(value)}
                                                        value={this.state.manufaktur}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="mb-3">
                                            <div className="row">
                                                <div className="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label htmlFor="pilih-grade">Ukuran</label>
                                                    <DropdownList
                                                        data={this.state.sizes}
                                                        textField={item => item === false ? "Pilih Ukuran" : this.getSizeName(item)}
                                                        filter
                                                        onChange={value => this.setState({size:value})}
                                                        value={this.state.size}
                                                    />
                                                </div>
                                                <div className="col">
                                                    <label htmlFor="pilih-grade">Warna Sepatu</label>
                                                    <DropdownList
                                                        data={this.state.colors}
                                                        textField={item => item === false ? "Pilih Warna" : item.nama}
                                                        filter
                                                        onChange={value => this.setState({color:value})}
                                                        value={this.state.color}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="mb-3">
                                            <div className="row">
                                                <div className="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label htmlFor="pilih-grade">Status</label>
                                                    <DropdownList
                                                        data={[0,1]}
                                                        textField={item => item === false || item === 0 ? "Tidak Aktif" : "Aktif"}
                                                        filter
                                                        onChange={value => this.setState({status:value})}
                                                        value={this.state.status}
                                                    />
                                                </div>
                                                <div className="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label htmlFor="pilih-grade">Discontinue</label>
                                                    <DropdownList
                                                        data={[0,1]}
                                                        textField={item => item === false || item === 0 ? "Tidak" : "Iya"}
                                                        filter
                                                        onChange={value => this.setState({discontinue:value})}
                                                        value={this.state.discontinue}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    <div className="mb-3"></div>
                                </div>
                            <div className="row my-4">
                                <div className="col text-right">
                                    <button onClick={()=>this.save()} className="btn btn-primary btn-utama">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                </div>
            </div>
        )
    }
}