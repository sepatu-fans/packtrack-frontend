// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BaseComponent from '../components/BaseComponent'
import Manufaktur from './KonfigurasiSistem/Manufaktur'
import Ukuran from './KonfigurasiSistem/Ukuran';
import Warna from './KonfigurasiSistem/Warna';
import Kategori from './KonfigurasiSistem/Kategori';
import Role from './KonfigurasiSistem/Role';
import Helper from '../Helper'
import Prefix from './KonfigurasiSistem/Prefix';
import Site from './KonfigurasiSistem/Site';
import Lokasi from './KonfigurasiSistem/Lokasi';
import Grade from './KonfigurasiSistem/Grade';
import ShippingType from './KonfigurasiSistem/ShippingType';
import ReceivingType from './KonfigurasiSistem/ReceivingType';

export default class KonfigurasiSistem extends Component {

    render(){
        return (
        <Fragment>
            <Sidebar active="konfigurasi-sistem"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <KonfigurasiSistemSub />
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class KonfigurasiSistemSub extends BaseComponent {
    
    pageTitle = "Konfigurasi Sistem"

    state = {
        'manufaktur_aktif':"0",
        'ukuran_aktif':"0",
        'kategori_aktif':"0",
        'warna_aktif':"0",
        'role_aktif':"0",
        'prefix_aktif':"0",
        'site_aktif':"0",
        'grade_aktif':"0",
        'tipepengiriman_aktif':"0",
        'tipepenerimaan_aktif':"0",
    }

    componentDidMount() {
        super.componentDidMount()
        if(!Helper.getAccessStatus(['Konfigurasi Sistem - Manufaktur','Konfigurasi Sistem - Ukuran','Konfigurasi Sistem - Kategori','Konfigurasi Sistem - Warna','Konfigurasi Sistem - Role Pengguna','Konfigurasi Sistem - Prefix Label','Konfigurasi Sistem - Site','Konfigurasi Sistem - Lokasi'], "view")) {
            // window.location.href = "/notfound";
        }
    }

    constructor(props) {
        super(props);

        if(Helper.getAccessStatus('Konfigurasi Sistem - Manufaktur', 'view')) {
            this.state.manufaktur_aktif= "1"
        }else if(Helper.getAccessStatus('Konfigurasi Sistem - Ukuran', 'view')) {
            this.state.ukuran_aktif= "1"
        }else if(Helper.getAccessStatus('Konfigurasi Sistem - Kategori', 'view')) {
            this.state.kategori_aktif= "1"
        }else if(Helper.getAccessStatus('Konfigurasi Sistem - Warna', 'view')) {
            this.state.warna_aktif= "1"
        }else if(Helper.getAccessStatus('Konfigurasi Sistem - Role Pengguna', 'view')) {
            this.state.role_aktif= "1"
        }else if(Helper.getAccessStatus('Konfigurasi Sistem - Prefix Label', 'view')) {
            this.state.prefix_aktif= "1"
        }else if(Helper.getAccessStatus('Konfigurasi Sistem - Site', 'view')) {
            this.state.site_aktif= "1"
        }else if(Helper.getAccessStatus('Konfigurasi Sistem - Lokasi', 'view')) {
            this.state.lokasi_aktif= "1"
        }else if(Helper.getAccessStatus('Konfigurasi Sistem - Grade', 'view')) {
            this.state.grade_aktif= "1"
        }else if(Helper.getAccessStatus('Konfigurasi Sistem - Tipe Pengiriman', 'view')) {
            this.state.tipepengiriman_aktif= "1"
        }else if(Helper.getAccessStatus('Konfigurasi Sistem - Tipe Penerimaan', 'view')) {
            this.state.tipepenerimaan_aktif= "1"
        }
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/manajement-labels"><span className="bc-link">Konfigurasi Sistem</span></a></li>
                        </ol>
                    </div>
                </div>
                <div>
                    <ul className="nav nav-tabs">
                        <li style={{display:Helper.getAccessStatus('Konfigurasi Sistem - Manufaktur', 'view') ? 'inline' : 'none'}} className="nav-item"><a className={"nav-link " + (this.state.manufaktur_aktif === "1" ? "active" : "")} role="tab" data-toggle="tab" href="#tab-1">Manufaktur</a></li>
                        <li style={{display:Helper.getAccessStatus('Konfigurasi Sistem - Ukuran', 'view') ? 'inline' : 'none'}} className="nav-item"><a className={"nav-link " + (this.state.ukuran_aktif === "1" ? "active" : "")} role="tab" data-toggle="tab" href="#tab-2">Ukuran</a></li>
                        <li style={{display:Helper.getAccessStatus('Konfigurasi Sistem - Kategori', 'view') ? 'inline' : 'none'}} className="nav-item"><a className={"nav-link " + (this.state.kategori_aktif === "1" ? "active" : "")} role="tab" data-toggle="tab" href="#tab-3">Kategori</a></li>
                        <li style={{display:Helper.getAccessStatus('Konfigurasi Sistem - Warna', 'view') ? 'inline' : 'none'}} className="nav-item"><a className={"nav-link " + (this.state.warna_aktif === "1" ? "active" : "")} role="tab" data-toggle="tab" href="#tab-4">Warna</a></li>
                        <li style={{display:Helper.getAccessStatus('Konfigurasi Sistem - Role Pengguna', 'view') ? 'inline' : 'none'}} className="nav-item"><a className={"nav-link " + (this.state.role_aktif === "1" ? "active" : "")} role="tab" data-toggle="tab" href="#tab-5">Role Pengguna</a></li>
                        <li style={{display:Helper.getAccessStatus('Konfigurasi Sistem - Prefix Label', 'view') ? 'inline' : 'none'}} className="nav-item"><a className={"nav-link " + (this.state.prefix_aktif === "1" ? "active" : "")} role="tab" data-toggle="tab" href="#tab-6">Prefix Label</a></li>
                        <li style={{display:Helper.getAccessStatus('Konfigurasi Sistem - Site', 'view') ? 'inline' : 'none'}} className="nav-item"><a className={"nav-link " + (this.state.site_aktif === "1" ? "active" : "")} role="tab" data-toggle="tab" href="#tab-7">Site</a></li>
                        <li style={{display:Helper.getAccessStatus('Konfigurasi Sistem - Lokasi', 'view') ? 'inline' : 'none'}} className="nav-item"><a className={"nav-link " + (this.state.lokasi_aktif === "1" ? "active" : "")} role="tab" data-toggle="tab" href="#tab-8">Lokasi</a></li>
                        <li style={{display:Helper.getAccessStatus('Konfigurasi Sistem - Grade', 'view') ? 'inline' : 'none'}} className="nav-item"><a className={"nav-link " + (this.state.grade_aktif === "1" ? "active" : "")} role="tab" data-toggle="tab" href="#tab-9">Grade</a></li>
                        <li style={{display:Helper.getAccessStatus('Konfigurasi Sistem - Tipe Pengiriman', 'view') ? 'inline' : 'none'}} className="nav-item"><a className={"nav-link " + (this.state.tipepengiriman_aktif === "1" ? "active" : "")} role="tab" data-toggle="tab" href="#tab-10">Tipe Pengiriman</a></li>
                        <li style={{display:Helper.getAccessStatus('Konfigurasi Sistem - Tipe Penerimaan', 'view') ? 'inline' : 'none'}} className="nav-item"><a className={"nav-link " + (this.state.tipepenerimaan_aktif === "1" ? "active" : "")} role="tab" data-toggle="tab" href="#tab-11">Tipe Penerimaan</a></li>
                    </ul>
                    <div className="tab-content">
                        <Manufaktur id="tab-1" active={this.state.manufaktur_aktif === "1" ? "active" : ""}/>
                        <Ukuran id="tab-2" active={this.state.ukuran_aktif  === "1"? "active" : ""}/>
                        <Kategori id="tab-3" active={this.state.kategori_aktif === "1" ? "active" : ""}/>
                        <Warna id="tab-4" active={this.state.warna_aktif === "1" ? "active" : ""}/>
                        <Role id="tab-5" active={this.state.role_aktif === "1" ? "active" : ""}/>
                        <Prefix id="tab-6" active={this.state.prefix_aktif === "1" ? "active" : ""}/>
                        <Site id="tab-7" active={this.state.site_aktif === "1" ? "active" : ""}/>
                        <Lokasi id="tab-8" active={this.state.lokasi_aktif === "1" ? "active" : ""}/>
                        <Grade id="tab-9" active={this.state.grade_aktif === "1" ? "active" : ""}/>
                        <ShippingType id="tab-10" active={this.state.tipepengiriman_aktif === "1" ? "active" : ""}/>
                        <ReceivingType id="tab-11" active={this.state.tipepenerimaan_aktif === "1" ? "active" : ""}/>
                    </div>
                </div>

            </div>
        )
    }
}