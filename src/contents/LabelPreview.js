import React,{ Component,Fragment } from 'react';
import queryString from 'query-string';
import htmlToImage from 'html-to-image';
import '../LabelPreview.css';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BaseComponent from '../components/BaseComponent'
import Helper from '../Helper'
import { savePDF } from '@progress/kendo-react-pdf';

const bodyRef = React.createRef();

export default class LabelPreview extends Component {
  constructor(props) {
    super(props);
    
    this.params = queryString.parse(window.location.search)
  }

  render(){
      return (
      <Fragment>
          <Sidebar active="manajement-labels"/>
          <div className="d-flex flex-column" id="content-wrapper">
              <div id="content">
                  <Header/>
                  <LabelPreviewSub params={this.params} />
              </div>
              <Footer/>
          </div>
      </Fragment>
      )
  }
}

class LabelPreviewSub extends BaseComponent {

  pageTitle = "Cetak Inner Label"
    
  state = {
    ids : [],
    refids : [],
    title : ''
  }
  constructor(props) {
    super(props);

    this.params = props.params;
    console.log(this.params);

    var param = "";
    var title = "";
    var url = '';
    if(this.params.special === '1') {
      url = '/labels-inner-by-id-min-max?idmin=' + this.params.idmin + '&idmax=' + this.params.idmax;
    }else {
      if(Array.isArray(this.params.id)) {
        this.params.id.forEach(elem => {
          param += "id=" + elem + "&";
        });
      }else {
        param = "id=" + this.params.id;
      }
      url = '/labels-inner-by-ids?' + param;
    }
    fetch(process.env.REACT_APP_SERVER_URL + url)
            .then(res => res.json())
            .then((res) => {
                if(res.status) {
                    var ids = [];
                    var refids = [];
                    res.data.forEach(elem => {
                      elem.refid = this.makeid(4);
                      refids.push(elem.refid);
                      ids.push(elem)
                      // refids.push(elem.refid);
                      // ids.push(elem)
                      // if(title === '') {
                      //   title = elem.serial_no;
                      // }else {
                      //   title = title + "-" + elem.serial_no;
                      // }
                    })
                    // if(this.params.special === '1') {
                      title = res.data[0].serial_no + "-" + res.data[res.data.length-1].serial_no;
                      if(res.data.length === 1) {
                        title = res.data[0].serial_no;
                      }
                    // }
                    this.setState({ids:ids,refids:refids,title:title});
                }else {
                  alert(res.error);
                  window.history.back();
                }
            })
        .catch(console.log)
  }

  componentDidMount() {
      super.componentDidMount()
      if(!Helper.getAccessStatus(['Manajemen Label Inner'], "print")) {
          window.location.href = "/notfound";
      }
      document.getElementById("btnCetak").style.display = "none"
      window.addEventListener('load', this.handleLoad)
  }
  handleLoad() {
    document.getElementById("btnCetak").style.display = ""
  }

  makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  downloads() {
    this.ids.map((id, i) => {     
      return this.download(id);
    })
  }

  download(id) {
    htmlToImage.toPng(document.getElementById("div_" + id), { quality: 1 })
              .then(function (dataUrl) {
                // var link = document.createElement('a');
                // link.download = 'label.png';
                // link.href = dataUrl;

                var canvas  = document.createElement("canvas");
                var img = new Image();
                img.src = dataUrl;
                img.onload = () => {
                  canvas.height = img.width;
                  canvas.width = img.height;
                  var context = canvas.getContext("2d");
                  context.rotate(-90 * Math.PI / 180);
                  context.translate(-canvas.height, 0);
                  context.drawImage(img, 0, 0);
                  var rotatedImageSrc =  canvas.toDataURL("image/jpeg", 100);

                  // document.getElementById(idPrint).style.visibility = "visible";
                  document.getElementById("div_" + id).style.width = "50mm";
                  document.getElementById("div_" + id).style.height = "70mm";
                  document.getElementById("div_" + id).innerHTML = "<img style='width:150px;height:210px;' src='" + rotatedImageSrc + "'/>";
                };
              });
  }

  createPdf = () => {
    var param = "";
    var url = '';
    if(this.params.special === '1') {
      url = '/print-labels-inner-by-id-min-max?idmin=' + this.params.idmin + '&idmax=' + this.params.idmax;
    }else {
      this.state.ids.forEach(elem => {
        param += "id=" + elem.id + "&";
      });
      url = '/print-labels-inner-by-ids?' + param;
    }
    this.setState({isLoading:true})
    fetch(process.env.REACT_APP_SERVER_URL + url)
          .then(res => res.json())
          .then((data) => {
              if(data.status) {
                this.setState({isLoading:true})
                savePDF(bodyRef.current, { 
                  paperSize: ["50mm","74mm"],
                  fileName: this.state.title + '.pdf',
                  margin: 0,
                  landscape: true
                }, () => {
                  this.setState({isLoading:false})
                })
              }
          })
        .catch(() => {
          this.setState({isLoading:false})
        })
    return;
  };

  kembali = () => {
    window.location.href = "/manajement-labels"
  }

  render() {
    return(
      
      <div className="container-fluid">
        <div className="row py-2 my-3">
            <div className="col-6 col-sm-8 col-md-8">
                <ol className="breadcrumb h6">
                    <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                    <li className="breadcrumb-item"><a href="/manajement-labels"><span className="bc-link">Manajemen Label</span></a></li>
                    <li className="breadcrumb-item"><a href="/"><span className="bc-link">Cetak Inner Label</span></a></li>
                </ol>
            </div>
            <div className="col text-right"></div>
        </div>
        <div>
            <div className="row">
                <div className="col-lg-12 col-xl-12">
                <div className="card shadow mb-4">
                  <div className="card-header py-3">
                      <h6 className="text-primary m-0 font-weight-bold">Cetak Label</h6>
                  </div>
                  <div className="card-body">
                      <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                      <div style={{width:"100%"}}>
                          <section className="header-bar">
                            {/* <h6>Progress : <span id="progress">0%</span></h6> */}
                            <input type="hidden" id="progressmax" value={this.state.ids.length}/>
                            {/* <button onClick={(e) => this.downloads()} style={{margin:"0px auto"}}>
                              Export Single Label
                            </button> */}
                          </section>
                          <section className="pdf-container">
                            <section className="pdf-toolbar">
                              <button className="btn btn-warning" onClick={this.kembali}><i className="fa fa-long-arrow-left"></i>&nbsp;Kembali</button>&nbsp;
                              {this.state.isLoading ? "Loading ..." : <button id="btnCetak" className="btn btn-danger" onClick={this.createPdf}><i className="fa fa-print"></i>&nbsp;Cetak</button>}
                              <hr/>
                            </section>
                            <section className="pdf-body" ref={bodyRef} style={{maxHeight:"350px",overflowY:"scroll"}}>
                              {
                                this.state.ids.map((dt, i) => {    
                                  return (<BarcodeCard key={dt.refid} id={dt.refid} cetak_ke={dt.cetak_ke} barcode={dt.barcode} serial_no={dt.serial_no} name={dt.sku_name === null || dt.sku_name === '' ? dt.name : dt.sku_name} color={dt.color} size={dt.size} size_cm={dt.cm} size_us={dt.us} size_uk={dt.uk} jumlah={dt.jumlah} />) 
                                })
                              }
                            </section>
                          </section>
                        </div>
                      </div>
                      <div className="row my-4">
                          {/* <div className="col text-left"><a className="btn btn-warning text-white" role="button" href="manajemen-label.html"><i className="fa fa-long-arrow-left"></i>&nbsp;Kembali</a></div> */}
                          {/* <div className="col text-right"><a className="btn btn-danger text-white" role="button" rel="noopener noreferrer" target="_blank" href="about:blank"><i className="fa fa-print"></i>&nbsp;Cetak</a></div> */}
                      </div>
                  </div>
              </div>
                </div>
            </div>
        </div>
    </div>

      
    )
  }
}

class BarcodeCard extends Component {

  constructor(props) {
    super(props);
    // Don't call this.setState() here!
    this.d = props.id;
    this.id =  "div_" + props.id;
    this.idPrint =  "btnprint_" + props.id;
  }

  download(id, jumlah) {
    setTimeout(function() {
      htmlToImage.toPng(document.getElementById("div_" + id), { quality: 1 })
              .then(function (dataUrl) {

                var canvas  = document.createElement("canvas");
                var img = new Image();
                img.src = dataUrl;
                img.onload = () => {
                  console.log(dataUrl);

                  canvas.height = img.width;
                  canvas.width = img.height;
                  var context = canvas.getContext("2d");
                  context.rotate(-90 * Math.PI / 180);
                  context.translate(-canvas.height, 0);
                  context.drawImage(img, 0, 0);
                  var rotatedImageSrc =  canvas.toDataURL("image/jpeg", 100);

                  // document.getElementById(idPrint).style.visibility = "visible";
                  document.getElementById("div_" + id).style.width = "50mm";
                  document.getElementById("div_" + id).style.height = "70mm";
                  document.getElementById("div_" + id).className = "imageDone";
                  document.getElementById("div_" + id).innerHTML = "<img style='width:150px;height:210px;' src='" + rotatedImageSrc + "'/>";

                  var progress = parseInt(document.getElementsByClassName("imageDone").length);
                  var progressMax = parseInt(document.getElementById("progressmax").value);
                  var percent = parseInt(progress*100/progressMax);
                  document.getElementById("progress").innerHTML = percent + "%";

                  for(var i = 0; i < jumlah - 1; i++) {
                    document.getElementById("div_" + id).parentElement.insertBefore(document.getElementById("div_" + id).cloneNode(true),document.getElementById("div_" + id))
                  }
                };
              });
    }, 50);
  }


  getBarcodeSize(barcode) {
    if(barcode === null) {
      return "15px"
    }
    if(barcode.length < 15) {
      return "25px"
    }else if(barcode.length < 20) {
      return "20px"
    }
    return "15px"
  }

  render() {
    return (
      <div id={this.id} style={{background: "white"}}>
        <div style={{paddingLeft:"15px"}}>
          <table style={{}}>
            <tbody>
            <tr>
              <td colSpan="2" style={{borderTop:"0px",padding:"0rem"}}>
                <div style={{color:"black",fontSize:this.getBarcodeSize(this.props.name), fontWeight:"bold"}}>{this.props.name}</div>
                <div style={{color:"black",fontSize:"12px", fontWeight:"medium", marginTop:"-7px"}}>{this.props.color}</div>
              </td>
            </tr>
            <tr>
              <td style={{borderTop:"0px",padding:"0rem"}}>
                <div style={{marginLeft:"",marginTop:"5px",textAlign:"center"}}>
                  <img alt="" style={{width:"100px"}} src={process.env.REACT_APP_SERVER_URL+"/barcode?height=22&code=" + this.props.barcode}/>
                </div>
                <div style={{marginLeft:"",marginTop:"5px",textAlign:"center"}}>
                  <img alt="" style={{width:"75px"}} src={process.env.REACT_APP_SERVER_URL+"/barcode?height=15&code=" + this.props.serial_no}/>
                </div>
              </td>
              <td style={{borderTop:"0px",padding:"0rem"}}>
                <table style={{marginTop:"0px", marginLeft:"5px"}}>
                  <tbody>
                  <tr>
                    <td style={{borderTop:"0px",padding:"1.5px",color:"black",fontSize:"10px", fontWeight:"bold", textAlign:"center"}}>CM<br/>{this.props.size_cm}</td>
                    <td style={{borderTop:"0px",padding:"1.5px",color:"black",fontSize:"10px", fontWeight:"bold", textAlign:"center"}}>UK<br/>{this.props.size_uk}</td>
                    <td style={{borderTop:"0px",padding:"1.5px",color:"black",fontSize:"10px", fontWeight:"bold", textAlign:"center"}}>USA<br/>{this.props.size_us}</td>
                  </tr>
                  <tr>
                    <td colSpan="3" style={{borderTop:"0px",padding:"0rem"}}>
                      <div style={{color:"black",fontSize:"45px", fontWeight:"bold", marginTop:"-12.5px",textAlign:"center"}}>
                        {this.props.size}
                      </div>
                        <div style={{fontSize:"10px",float:"right",marginTop:"-20px"}}>{this.props.cetak_ke}</div>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            </tbody>
          </table>
          
        </div>
        <div className="page-break"></div>
        {/* {this.download(this.d, this.props.jumlah)} */}
      </div>
    );
  }
}