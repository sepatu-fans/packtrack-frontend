import React, {Component, Fragment} from 'react';
import axios from 'axios';
import Modal from 'react-bootstrap/Modal';
import DropdownList from 'react-widgets/lib/DropdownList'
import Helper from '../../Helper'

export default class Ukuran extends Component {
    state = {
        items : [],
        size : 10,
        pages : [1],
        page : 1,
        isLoading : false,
        isLoading1 : false,
        search : '',
        selecteds : [],
        show : false,
        nama : "",
        id : 0,
        manufaktures : [],
        manufaktur : false,
        manufakturid : '',
        showImport : false,
    }

    role_name = "Konfigurasi Sistem - Ukuran"

    constructor(props) {
        super(props);
        this.id = props.id;
        this.active = props.active;
    }

    handleClose = () => this.setState({
        manufaktur_id : false,
        manufaktur : false,
        ukuran:"",
        cm:"",
        uk:"",
        us:"",
        mm_p:"",
        mm_l:"",
        mm_g:"",
        show:false
    });
    handleShow = () => this.setState({id:0,show:true});

    handleCloseImport = () => this.setState({showImport:false});
    handleShowImport = () => this.setState({id:0,showImport:true});
    
    sizes = [
        10,20,50,100
    ]

    componentDidMount() {
        // if(!Helper.getAccessStatus(this.role_name, "view")) {
        //     window.location.href = "/notfound";
        // }
        this.loadData();
        this.loadManufaktur();
    }

    loadManufaktur() {
        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/manufaktur-list')
            .then(res => res.json())
            .then((res) => {
                if(res.status) {
                    this.setState({manufaktures: res.data })
                }
                this.setState({isLoading1:false})
            })
        .catch(console.log)
    }

    loadData() {
        this.setState({isLoading:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/size?page=' + this.state.page + '&size=' + this.state.size + '&search=' + this.state.search + '&manufaktur=' + this.state.manufakturid)
            .then(res => res.json())
            .then((data) => {
            this.setState({ items: data.data, pages: data.pages })
            this.setState({isLoading:false})
        })
        .catch(console.log)
    }

    handleChange = (event) => {
        console.log(event.target.value);
        this.setState({ size: event.target.value }, function() {
            this.loadData();
        });
    };

    handleChangeManufaktur = (event) => {
        console.log(event.target.value);
        this.setState({ manufakturid: event.target.value }, function() {
            this.loadData();
        });
    };

    searchChange = (event) => {
        console.log(event.target.value);
        this.setState({search:event.target.value,page:1}, function() {
            this.loadData();
        });
    }

    pageChange = (event) => {
        console.log(event.target.getAttribute('data'));
        this.setState({ page: event.target.getAttribute('data') }, function() {
            this.loadData();
        });
    };

    edit(id) {
        fetch(process.env.REACT_APP_SERVER_URL + '/size-by-id?id=' + id)
            .then(res => res.json())
            .then((data) => {
                if(data.status) {
                    var manufaktur = false;
                    this.state.manufaktures.forEach((e) => {
                        if(data.data.manufaktur_id === e.id) {
                            manufaktur = e;
                        }
                    })
                    this.setState({
                        id:id,
                        ukuran:data.data.size,
                        uk:data.data.uk,
                        us:data.data.us,
                        cm:data.data.cm,
                        mm_p:data.data.mm_p,
                        mm_l:data.data.mm_l,
                        mm_g:data.data.mm_g,
                        manufaktur:manufaktur,
                        show:true
                    })
                }else {
                    alert(data.error);
                }
            })
            .catch(console.log)
    }

    save() {
        if(!this.state.manufaktur) {
            alert('Manufaktur harus diisi');
            return;
        }
        if(!this.state.ukuran) {
            alert('Size harus diisi');
            return;
        }
        if(!this.state.cm) {
            alert('CM harus diisi');
            return;
        }
        if(!this.state.uk) {
            alert('UK harus diisi');
            return;
        }
        if(!this.state.us) {
            alert('US harus diisi');
            return;
        }
        if(!this.state.mm_p) {
            alert('MM(P) harus diisi');
            return;
        }
        if(!this.state.mm_l) {
            alert('MM(L) harus diisi');
            return;
        }
        if(!this.state.mm_g) {
            alert('MM(G) harus diisi');
            return;
        }

        var data = {
            manufaktur_id:this.state.manufaktur.id,
            size:this.state.ukuran,
            cm:this.state.cm,
            uk:this.state.uk,
            us:this.state.us,
            mm_p:this.state.mm_p,
            mm_l:this.state.mm_l,
            mm_g:this.state.mm_g,
            created_at:Helper.getCurrentTime(null, 7),
            created_by:localStorage.getItem("USERID"),
        }
        if(this.state.id !== 0) {
            data.id = this.state.id;
        }
        axios.post(process.env.REACT_APP_SERVER_URL+'/size-add', data)
            .then(response => response.data)
            .then((res) => {
                    // console.log(res);
                if(res.status) {
                    alert("Data berhasil disimpan");  
                    this.loadData();
                    this.setState({
                        manufaktur_id : false,
                        ukuran:"",
                        cm:"",
                        uk:"",
                        us:"",
                        mm_p:"",
                        mm_l:"",
                        mm_g:"",
                        show : this.state.id === 0
                    })
                }else {
                    alert(res.error);
                }
            }
        );
    }

    delete = (id) => {
        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            var data = {
                id:id
            }
            axios.post(process.env.REACT_APP_SERVER_URL+'/size-delete', data)
                .then(response => response.data)
                .then((res) => {
                        // console.log(res);
                        if(res.status) {
                            // alert("Data berhasil dihapus");  
                            this.loadData();
                        }else {
                            alert(res.error);
                        }
                    }
                );
        }
    }
    
    uploadCsv(e) {
        let imageFromObj = new FormData();
        imageFromObj.append("csv", e.target.files[0]);
        this.setState({isLoading:true});
        axios.post(process.env.REACT_APP_SERVER_URL+'/size-uploadcsv', imageFromObj)
            .then(response => response.data)
            .then((res) => {
                if(res.status) {
                    alert("Upload Success");
                    this.setState({csv:res.data, isLoading:false});
                }else {
                    alert(res.error);
                    this.setState({isLoading:false});
                }
            }
        );
    }

    exportData() {
        window.open(process.env.REACT_APP_SERVER_URL + '/size-download')
    }
  
    handleSave = () => {
      console.log('this is:', this.state);
      if(this.state.csv === '') {
        alert("CSV File is required!");
        return;
      }
      this.putDataToDB();
    }
  
    putDataToDB = () => {
      var data = {
        csv:this.state.csv,
        created_by:localStorage.getItem("USERID")
      }
      console.log(data);
      this.setState({isLoading:true});
      axios.post(process.env.REACT_APP_SERVER_URL+'/size-upload', data)
          .then(response => response.data)
          .then((res) => {
            this.setState({isLoading:false});
            if(res.status) {
              alert("Import Success. " + res.data + " Imported");
              this.handleCloseImport();
              this.loadData();
            }else {
              alert(res.error);
            }
          }
        );
    };

    downlaodTemplate() {
        window.location.href = '/templates/template-size.csv';
    }

    render(){
        return (
        <Fragment>
            <div className={"tab-pane " + this.active} role="tabpanel" id={this.id}>
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <div className="row">
                                <div className="col my-auto">
                                    <h6 className="text-primary m-0 font-weight-bold">Daftar Ukuran</h6>
                                </div>
                                <div className="col text-right">
                                    {Helper.getAccessStatus(this.role_name, "add") ? <button className="btn btn-primary btn-utama" id="btn-grade-a-requirement" onClick={()=>this.handleShow()}>Tambah&nbsp;</button> : null}
                                </div>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="row my-3">
                                <div className="col">
                                    <select key="1" className="custom-select max-view mb-3" defaultValue={this.state.size} onChange={this.handleChange} >
                                        {this.sizes.map((item) => (
                                            <option key={item} value={item}>{item} baris</option>
                                        ))}
                                    </select>&nbsp;
                                    <select key="2" className="custom-select max-view mb-3" defaultValue={this.state.manufakturid} onChange={this.handleChangeManufaktur}>
                                        <option value="">Semua</option>
                                        {this.state.manufaktures.map((item) => (
                                            <option key={item.id} value={item.id}>{item.nama}</option>
                                        ))}
                                    </select>
                                </div>
                                <div className="col">
                                    <div className="input-group">
                                        <input onChange={this.searchChange} className="form-control" type="text" placeholder="Cari Produk" name="cari-produk"/>
                                        <div className="input-group-append"><button onClick={this.handleSearch} className="btn btn-info" type="button"><i className="fa fa-search"></i></button></div>
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                <table className="table dataTable my-0" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Manufaktur</th>
                                            <th>Size</th>
                                            <th>UK</th>
                                            <th>US</th>
                                            <th>CM</th>
                                            <th>MM(P)</th>
                                            <th>MM(L)</th>
                                            <th>MM(G)</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.isLoading ? (
                                            <tr>
                                                <td colSpan="8">
                                                    Loading
                                                </td>
                                            </tr>
                                        ) : 
                                        this.state.items.map((item) => (
                                            <tr key={item.id}>
                                                <td>{item.nama}</td>
                                                <td>{item.size}</td>
                                                <td>{item.uk}</td>
                                                <td>{item.us}</td>
                                                <td>{item.cm}</td>
                                                <td>{item.mm_p}</td>
                                                <td>{item.mm_l}</td>
                                                <td>{item.mm_g}</td>
                                                <td>
                                                    {Helper.getAccessStatus(this.role_name, "edit") ? <a href="##" onClick={()=>this.edit(item.id)} className="m-2 link-green">Edit</a> : null}
                                                    {Helper.getAccessStatus(this.role_name, "delete") ? <a onClick={()=>this.delete(item.id)} href="##" className="m-2 link-red">Hapus</a> : null}
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                            <div className="row">
                                <div className="col-md-6 align-self-center">
                                    <div></div>
                                    <div className="mb-4">
                                        {Helper.getAccessStatus(this.role_name, "import") ? <button onClick={()=>this.handleShowImport()} className="btn btn-success mr-2">Impor Data</button> : null}
                                        {Helper.getAccessStatus(this.role_name, "export") ? <button onClick={()=>this.exportData()} className="btn btn-danger mr-2">Ekspor Data</button> : null}
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <nav className="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers" style={{'float':'right'}}>
                                        <ul className="pagination">
                                            <li className={"page-item " + (this.state.pages[0] === 1 ? "disabled" : "")}><button data={this.state.pages[0]-1} className="page-link" aria-label="Previous" onClick={this.pageChange}>«</button></li>
                                            {this.state.pages.map((page) => (
                                                <li key={page} className={"page-item " + ((parseInt(this.state.page) === page ? "active" : ""))}><button data={page} className="page-link" onClick={this.pageChange}>{page}</button></li>
                                            ))}
                                            <li className="page-item"><button data={this.state.pages[this.state.pages.length-1]+1} className="page-link" aria-label="Next" onClick={this.pageChange}>»</button></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <Modal show={this.state.show} onHide={this.handleClose} animation={true}>
                <Modal.Header closeButton>
                <Modal.Title>{this.state.id === 0 ? "Buat" : "Ubah"} Ukuran</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="form-group">
                        <label htmlFor="qty-label">Manufaktur</label>
                        <DropdownList
                            data={this.state.manufaktures}
                            textField={item => item === false ? "Pilih Manufaktur" : item.nama}
                            filter
                            busy={this.state.isLoading2}
                            onChange={value => this.setState({manufaktur:value})}
                            value={this.state.manufaktur}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="qty-label">Size</label>
                        <input type="text" className="form-control" name="qty-label" placeholder="Size" value={this.state.ukuran} onChange={(e)=> this.setState({ukuran:e.target.value})}/>
                    </div>
                    <div className="row">
                        <div className="col-md-4">
                            <div className="form-group">
                                <label htmlFor="qty-label">UK</label>
                                <input step="0.01" type="text" className="form-control" name="qty-label" placeholder="UK" value={this.state.uk} onChange={(e)=> this.setState({uk:e.target.value})}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="qty-label">MM(P)</label>
                                <input step="0.01" type="text" className="form-control" name="qty-label" placeholder="MM(P)" value={this.state.mm_p} onChange={(e)=> this.setState({mm_p:e.target.value})}/>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group">
                                <label htmlFor="qty-label">US</label>
                                <input step="0.01" type="text" className="form-control" name="qty-label" placeholder="US" value={this.state.us} onChange={(e)=> this.setState({us:e.target.value})}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="qty-label">MM(L)</label>
                                <input step="0.01" type="text" className="form-control" name="qty-label" placeholder="MM(L)" value={this.state.mm_l} onChange={(e)=> this.setState({mm_l:e.target.value})}/>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group">
                                <label htmlFor="qty-label">CM</label>
                                <input step="0.01" type="text" className="form-control" name="qty-label" placeholder="CM" value={this.state.cm} onChange={(e)=> this.setState({cm:e.target.value})}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="qty-label">MM(G)</label>
                                <input step="0.01" type="text" className="form-control" name="qty-label" placeholder="MM(G)" value={this.state.mm_g} onChange={(e)=> this.setState({mm_g:e.target.value})}/>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <button onClick={()=> this.save()} className="btn btn-primary btn-utama ml-3">Simpan</button>
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showImport} onHide={this.handleCloseImport} animation={true}>
                <Modal.Header closeButton>
                <Modal.Title>Import Ukuran</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="form-group">
                        <label htmlFor="qty-label">CSV File</label>
                        <input accept=".csv" onChange={(e) => this.uploadCsv(e)} type="file" className="form-control" name="file" style={{height:'auto'}}/>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <button onClick={()=> this.downlaodTemplate()} className="btn btn-primary ml-3">Download Template</button>
                    <button onClick={()=> this.handleSave()} className="btn btn-primary btn-utama ml-3">Simpan</button>
                </Modal.Footer>
            </Modal>
        </Fragment>
        )
    }
}