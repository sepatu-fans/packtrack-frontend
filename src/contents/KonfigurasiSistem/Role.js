import React, {Component, Fragment} from 'react';
import axios from 'axios';
import Modal from 'react-bootstrap/Modal';
import Helper from '../../Helper'

export default class Role extends Component {
    state = {
        items : [],
        size : 10,
        pages : [1],
        page : 1,
        isLoading : false,
        search : '',
        selecteds : [],
        show : false,
        showAccess : false,
        nama : "",
        id : 0,
        access : []
    }

    role_name = "Konfigurasi Sistem - Role Pengguna"

    constructor(props) {
        super(props);
        this.id = props.id;
        this.active = props.active;
    }

    roles = [
        ["Beranda", ["view"]],
        ["Pengiriman Produk", ["view","add","edit","delete","verifikasi","import","cancel"]],
        ["Penerimaan Produk", ["view","add","edit","delete","verifikasi","import","cancel"]],
        ["Pemindahan Langsung", ["view","add","edit","delete","verifikasi","import","cancel"]],
        ["Pengisian Produk - Grade A", ["view","add","edit","delete","open"]],
        ["Pengisian Produk - Grade Other", ["view","add","edit","delete","open"]],
        ["Pengisian Produk - No Box", ["view","add","delete"]],
        ["Laporan - IBXTrack", ["view", "export"]],
        ["Data Produk", ["view","add","edit","delete","import","export"]],
        ["Kelompok Produk", ["view","add","edit","delete"]],
        ["Profil Karton", ["view","add","edit","delete"]],
        ["Manajemen Label Inner", ["view","add","delete", "print"]],
        ["Manajemen Label Outer", ["view","add","delete", "print"]],
        ["Open Close Box", ["view","add","close"]],
        ["Manajemen User", ["view","add","edit","delete","location"]],
        ["Konfigurasi Sistem - Manufaktur", ["view","add","edit","delete"]],
        ["Konfigurasi Sistem - Ukuran", ["view","add","edit","delete","import","export"]],
        ["Konfigurasi Sistem - Kategori", ["view","add","edit","delete"]],
        ["Konfigurasi Sistem - Warna", ["view","add","edit","delete"]],
        ["Konfigurasi Sistem - Role Pengguna", ["view","add","edit","delete","access"]],
        ["Konfigurasi Sistem - Prefix Label", ["view","add","edit","delete"]],
        ["Konfigurasi Sistem - Site", ["view","add","edit","delete"]],
        ["Konfigurasi Sistem - Lokasi", ["view","add","edit","delete"]],
        ["Konfigurasi Sistem - Grade", ["view","add","edit","delete"]],
        ["Konfigurasi Sistem - Tipe Pengiriman", ["view","add","edit","delete"]],
        ["Konfigurasi Sistem - Tipe Penerimaan", ["view","add","edit","delete"]]
    ]

    handleClose = () => this.setState({
        nama : "",
        show:false
    });
    handleShow = () => this.setState({id:0,show:true,access:[]});

    handleCloseAccess = () => this.setState({showAccess:false});
    handleShowAccess = (id) => this.setState({id:id,showAccess:true});
    
    sizes = [
        10,20,50,100
    ]

    componentDidMount() {
        // if(!Helper.getAccessStatus(this.role_name, "view")) {
        //     window.location.href = "/notfound";
        // }
        this.loadData();
        var access = this.state.access;
        if(access.length === 0) {
            this.roles.forEach((e) => {
                e[1].forEach((f) => {
                    access.push([e[0],f,false])
                })
            })
        }

        this.setState({access:access})
    }

    loadData() {
        this.setState({isLoading:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/role?page=' + this.state.page + '&size=' + this.state.size + '&search=' + this.state.search)
            .then(res => res.json())
            .then((data) => {
            this.setState({ items: data.data, pages: data.pages })
            this.setState({isLoading:false})
        })
        .catch(console.log)
    }

    handleChange = (event) => {
        console.log(event.target.value);
        this.setState({ size: event.target.value }, function() {
            this.loadData();
        });
    };

    searchChange = (event) => {
        console.log(event.target.value);
        this.setState({search:event.target.value,page:1}, function() {
            this.loadData();
        });
    }

    pageChange = (event) => {
        console.log(event.target.getAttribute('data'));
        this.setState({ page: event.target.getAttribute('data') }, function() {
            this.loadData();
        });
    };

    edit(id) {
        fetch(process.env.REACT_APP_SERVER_URL + '/role-by-id?id=' + id)
                .then(res => res.json())
                .then((data) => {
                    if(data.status) {
                        this.setState({
                            id:id,
                            nama:data.data.nama,
                            show:true
                        })
                    }else {
                        alert(data.error);
                    }
                })
            .catch(console.log)
    }

    editAccess(id) {
        fetch(process.env.REACT_APP_SERVER_URL + '/role-by-id?id=' + id)
                .then(res => res.json())
                .then((data) => {
                    if(data.status) {
                        var access = []
                        if(data.data.access === "" || data.data.access === null) {
                            access = [];
                            this.roles.forEach((e) => {
                                e[1].forEach((f) => {
                                    if(e[0] === 'Produk Retur' || e[0] === 'Produk Repark' || e[0] === 'Produk Opname') {
                                    }else {
                                        access.push([e[0],f,false])
                                    }
                                })
                            })
                        }else {
                            var accessDB = JSON.parse(data.data.access)
                            console.log(accessDB);
                            access = [];
                            this.roles.forEach((e) => {
                                e[1].forEach((f) => {
                                    if(e[0] === 'Produk Retur' || e[0] === 'Produk Repark' || e[0] === 'Produk Opname') {
                                    }else {
                                        access.push([e[0],f,false])
                                    }
                                })
                            })
                            console.log(access);
                            accessDB.forEach((a) => {
                                var index = access.findIndex((item) => item[0] === a[0] && item[1] === a[1]);
                                if(index >= 0) {
                                    access.splice(index,1)
                                    access.push(a)
                                }
                            })
                        }
                        this.setState({
                            id:id,
                            access:access,
                            showAccess:true
                        })
                    }else {
                        alert(data.error);
                    }
                })
            .catch(console.log)
    }

    save() {
        if(this.state.nama === '') {
            alert('Nama harus diisi');
            return;
        }

        var data = {
            nama:this.state.nama,
            created_at:Helper.getCurrentTime(null, 7),
            created_by:localStorage.getItem("USERID"),
        }
        if(this.state.id !== 0) {
            data.id = this.state.id;
        }
        axios.post(process.env.REACT_APP_SERVER_URL+'/role-add', data)
            .then(response => response.data)
            .then((res) => {
                    // console.log(res);
                if(res.status) {
                    alert("Data berhasil disimpan");  
                    this.loadData();
                    this.setState({
                        nama : "",
                        show : this.state.id === 0
                    })
                }else {
                    alert(res.error);
                }
            }
        );
    }

    saveAccess() {
        
        var data = {
            id:this.state.id,
            access:JSON.stringify(this.state.access)
        }
        axios.post(process.env.REACT_APP_SERVER_URL+'/role-access', data)
            .then(response => response.data)
            .then((res) => {
                    // console.log(res);
                if(res.status) {
                    alert("Data berhasil disimpan");  
                    this.handleCloseAccess();
                }else {
                    alert(res.error);
                }
            }
        );
    }

    delete = (id) => {
        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            var data = {
                id:id
            }
            axios.post(process.env.REACT_APP_SERVER_URL+'/role-delete', data)
                .then(response => response.data)
                .then((res) => {
                        // console.log(res);
                        if(res.status) {
                            // alert("Data berhasil dihapus");  
                            this.loadData();
                        }else {
                            alert(res.error);
                        }
                    }
                );
        }
    }

    changeAccess(e,f,g) {
        var access = this.state.access;
        var isSkip = false;
        access.forEach((a) => {
            if(a[0] === e && a[1] === f) {
                a[2] = g;
                isSkip = true;
            }
        });
        if(!isSkip) {
            access.push([e,f,g]);
        }

        if(f === 'view' && g === false) {
            access.forEach((a) => {
                if(a[0] === e) {
                    a[2] = false;
                }
            });
        }else {
            access.forEach((a) => {
                if(a[0] === e) {
                    if(!this.viewEnabled(e)) {
                        a[2] = false;
                    }
                }
            });
        }

        console.log(access);
        this.setState({
            access:access
        })
    }

    haveAccess(e, f) {
        var access = this.state.access;
        var result = false;
        access.forEach((a) => {
            if(a[0] === e && a[1] === f) {
                result = a[2];
            }
        });

        return result;
    }

    viewEnabled(e) {
        var access = this.state.access;
        var result = false;
        access.forEach((a) => {
            if(a[0] === e && a[1] === "view") {
                result = a[2];
            }
        });

        return result;
    }

    checkAll(e) {
        var access = this.state.access;
        var statusAll = this.state.status_all;
        if(statusAll === undefined) {
            statusAll = true;
        }else {
            statusAll = !statusAll;
        }

        access.forEach((a) => {
            if(a[0] === e || e === "") {
                a[2] = statusAll;
            }
        });
        this.setState({
            access:access,
            status_all:statusAll
        })
    }

    render(){
        return (
        <Fragment>
            <div className={"tab-pane " + this.active} role="tabpanel" id={this.id}>
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <div className="row">
                                <div className="col my-auto">
                                    <h6 className="text-primary m-0 font-weight-bold">Daftar Role</h6>
                                </div>
                                <div className="col text-right">
                                    {Helper.getAccessStatus(this.role_name, "add") ? <button className="btn btn-primary btn-utama" id="btn-grade-a-requirement" onClick={()=>this.handleShow()}>Tambah&nbsp;</button> : null}
                                </div>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="row my-3">
                                <div className="col">
                                    <select className="custom-select max-view mb-3" defaultValue={this.state.size} onChange={this.handleChange} >
                                        {this.sizes.map((item) => (
                                            <option key={item} value={item}>{item} baris</option>
                                        ))}
                                    </select>
                                </div>
                                <div className="col">
                                    <div className="input-group">
                                        <input onChange={this.searchChange} className="form-control" type="text" placeholder="Cari Produk" name="cari-produk"/>
                                        <div className="input-group-append"><button onClick={this.handleSearch} className="btn btn-info" type="button"><i className="fa fa-search"></i></button></div>
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                <table className="table dataTable my-0" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Nama Role</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.isLoading ? (
                                            <tr>
                                                <td colSpan="2">
                                                    Loading
                                                </td>
                                            </tr>
                                        ) : 
                                        this.state.items.map((item) => (
                                            <tr key={item.id}>
                                                <td>{item.nama}</td>
                                                <td>
                                                    {Helper.getAccessStatus(this.role_name, "edit") ? <a href="##" onClick={()=>this.edit(item.id)} className="m-2 link-green">Edit</a> : null}
                                                    {Helper.getAccessStatus(this.role_name, "delete") ? <a onClick={()=>this.delete(item.id)} href="##" className="m-2 link-red">Hapus</a> : null}
                                                    {Helper.getAccessStatus(this.role_name, "access") ? <a onClick={()=>this.editAccess(item.id)} href="##" className="m-2 link">Access</a> : null}
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                            <div className="row">
                                <div className="col-md-6 align-self-center">
                                </div>
                                <div className="col-md-6">
                                    <nav className="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers" style={{'float':'right'}}>
                                        <ul className="pagination">
                                            <li className={"page-item " + (this.state.pages[0] === 1 ? "disabled" : "")}><button data={this.state.pages[0]-1} className="page-link" aria-label="Previous" onClick={this.pageChange}>«</button></li>
                                            {this.state.pages.map((page) => (
                                                <li key={page} className={"page-item " + ((parseInt(this.state.page) === page ? "active" : ""))}><button data={page} className="page-link" onClick={this.pageChange}>{page}</button></li>
                                            ))}
                                            <li className="page-item"><button data={this.state.pages[this.state.pages.length-1]+1} className="page-link" aria-label="Next" onClick={this.pageChange}>»</button></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <Modal show={this.state.show} onHide={this.handleClose} animation={true}>
                <Modal.Header closeButton>
                <Modal.Title>Buat Role</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="form-group">
                        <label htmlFor="qty-label">Nama Role</label>
                        <input type="text" className="form-control" name="qty-label" placeholder="Nama" value={this.state.nama} onChange={(e)=> this.setState({nama:e.target.value})}/>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <button onClick={()=> this.save()} className="btn btn-primary btn-utama ml-3">Simpan</button>
                </Modal.Footer>
            </Modal>

            <Modal show={this.state.showAccess} onHide={this.handleCloseAccess} animation={true} size="lg">
                <Modal.Header closeButton>
                <Modal.Title>Akses Role <button onClick={()=> this.checkAll("")} className="btn btn-sm btn-primary btn-utama ml-3">All</button></Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <table style={{width:"100%"}}>
                        <tbody>
                            {this.roles.map((e) => (
                                <Fragment key={e}>
                                    <tr>
                                        <td>
                                            <button onClick={()=> this.checkAll(e[0])} className="btn btn-sm btn-primary btn-utama ml-3"><i className="fa fa-check"/></button> {this.roles.indexOf(e) + 1}. {e[0]}
                                        </td>
                                        <td>
                                            {e[1].map((f) => (
                                                <label key={f}>
                                                    <input checked={this.haveAccess(e[0],f)} onChange={(g)=>this.changeAccess(e[0], f, g.target.checked)} type="checkbox"></input> {f}&nbsp;&nbsp;
                                                </label>
                                            ))}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colSpan="2"><hr/></td>
                                    </tr>
                                </Fragment>
                            ))}
                        </tbody>
                    </table>
                </Modal.Body>
                <Modal.Footer>
                    <button onClick={()=> this.saveAccess()} className="btn btn-primary btn-utama ml-3">Simpan</button>
                </Modal.Footer>
            </Modal>
        </Fragment>
        )
    }
}