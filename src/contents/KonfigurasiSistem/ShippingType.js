import React, {Component, Fragment} from 'react';
import axios from 'axios';
import Modal from 'react-bootstrap/Modal';
import Helper from '../../Helper'

export default class ShippingType extends Component {
    state = {
        items : [],
        size : 10,
        pages : [1],
        page : 1,
        isLoading : false,
        search : '',
        selecteds : [],
        show : false,
        nama : "",
        keterangan : "",
        id : 0
    }

    role_name = "Konfigurasi Sistem - Tipe Pengiriman"

    constructor(props) {
        super(props);
        this.id = props.id;
        this.active = props.active;
    }

    handleClose = () => this.setState({
        kode : "",
        nama : "",
        category : [],
        show:false
    });
    handleShow = () => this.setState({id:0,show:true});
    
    sizes = [
        10,20,50,100
    ]

    componentDidMount() {
        // if(!Helper.getAccessStatus(this.role_name, "view")) {
        //     window.location.href = "/notfound";
        // }
        this.loadData();
    }

    loadData() {
        this.setState({isLoading:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/shipping-type?page=' + this.state.page + '&size=' + this.state.size + '&search=' + this.state.search)
            .then(res => res.json())
            .then((data) => {
            this.setState({ items: data.data, pages: data.pages })
            this.setState({isLoading:false})
        })
        .catch(console.log)
    }

    handleChange = (event) => {
        console.log(event.target.value);
        this.setState({ size: event.target.value }, function() {
            this.loadData();
        });
    };

    searchChange = (event) => {
        console.log(event.target.value);
        this.setState({search:event.target.value,page:1}, function() {
            this.loadData();
        });
    }

    pageChange = (event) => {
        console.log(event.target.getAttribute('data'));
        this.setState({ page: event.target.getAttribute('data') }, function() {
            this.loadData();
        });
    };

    edit(id) {
        fetch(process.env.REACT_APP_SERVER_URL + '/shipping-type-by-id?id=' + id)
                .then(res => res.json())
                .then((data) => {
                    if(data.status) {
                        this.setState({
                            id:id,
                            nama:data.data.nama,
                            keterangan:data.data.keterangan,
                            show:true
                        })
                    }else {
                        alert(data.error);
                    }
                })
            .catch(console.log)
    }

    save() {
        if(this.state.nama === '') {
            alert('Nama harus diisi');
            return;
        }

        var data = {
            nama:this.state.nama,
            keterangan:this.state.keterangan,
            created_at:Helper.getCurrentTime(null, 7),
            created_by:localStorage.getItem("USERID"),
        }
        if(this.state.id !== 0) {
            data.id = this.state.id;
        }
        axios.post(process.env.REACT_APP_SERVER_URL+'/shipping-type-add', data)
            .then(response => response.data)
            .then((res) => {
                    // console.log(res);
                if(res.status) {
                    alert("Data berhasil disimpan");  
                    this.loadData();
                    this.setState({
                        nama : "",
                        keterangan : "",
                        show : this.state.id === 0
                    })
                }else {
                    alert(res.error);
                }
            }
        );
    }

    delete = (id) => {
        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            var data = {
                id:id
            }
            axios.post(process.env.REACT_APP_SERVER_URL+'/shipping-type-delete', data)
                .then(response => response.data)
                .then((res) => {
                        // console.log(res);
                        if(res.status) {
                            // alert("Data berhasil dihapus");  
                            this.loadData();
                        }else {
                            alert(res.error);
                        }
                    }
                );
        }
    }

    render(){
        return (
        <Fragment>
            <div className={"tab-pane " + this.active} role="tabpanel" id={this.id}>
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <div className="row">
                                <div className="col my-auto">
                                    <h6 className="text-primary m-0 font-weight-bold">Daftar Tipe Pengiriman</h6>
                                </div>
                                <div className="col text-right">
                                    {Helper.getAccessStatus(this.role_name, "add") ? <button className="btn btn-primary btn-utama" id="btn-grade-a-requirement" onClick={()=>this.handleShow()}>Tambah&nbsp;</button> : null}
                                </div>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="row my-3">
                                <div className="col">
                                    <select className="custom-select max-view mb-3" defaultValue={this.state.size} onChange={this.handleChange} >
                                        {this.sizes.map((item) => (
                                            <option key={item} value={item}>{item} baris</option>
                                        ))}
                                    </select>
                                </div>
                                <div className="col">
                                    <div className="input-group">
                                        <input onChange={this.searchChange} className="form-control" type="text" placeholder="Cari Produk" name="cari-produk"/>
                                        <div className="input-group-append"><button onClick={this.handleSearch} className="btn btn-info" type="button"><i className="fa fa-search"></i></button></div>
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                <table className="table dataTable my-0" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Keterangan</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.isLoading ? (
                                            <tr>
                                                <td colSpan="3">
                                                    Loading
                                                </td>
                                            </tr>
                                        ) : 
                                        this.state.items.map((item) => (
                                            <tr key={item.id}>
                                                <td>{item.nama}</td>
                                                <td>{item.keterangan}</td>
                                                <td>
                                                    {Helper.getAccessStatus(this.role_name, "edit") ? <a href="##" onClick={()=>this.edit(item.id)} className="m-2 link-green">Edit</a> : null}
                                                    {Helper.getAccessStatus(this.role_name, "delete") ? <a onClick={()=>this.delete(item.id)} href="##" className="m-2 link-red">Hapus</a> : null}
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                            <div className="row">
                                <div className="col-md-6 align-self-center">
                                </div>
                                <div className="col-md-6">
                                    <nav className="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers" style={{'float':'right'}}>
                                        <ul className="pagination">
                                            <li className={"page-item " + (this.state.pages[0] === 1 ? "disabled" : "")}><button data={this.state.pages[0]-1} className="page-link" aria-label="Previous" onClick={this.pageChange}>«</button></li>
                                            {this.state.pages.map((page) => (
                                                <li key={page} className={"page-item " + ((parseInt(this.state.page) === page ? "active" : ""))}><button data={page} className="page-link" onClick={this.pageChange}>{page}</button></li>
                                            ))}
                                            <li className="page-item"><button data={this.state.pages[this.state.pages.length-1]+1} className="page-link" aria-label="Next" onClick={this.pageChange}>»</button></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

            <Modal show={this.state.show} onHide={this.handleClose} animation={true}>
                <Modal.Header closeButton>
                <Modal.Title>{this.state.id === 0 ? "Buat" : "Ubah"} Tipe Pengiriman</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="form-group">
                        <label htmlFor="qty-label">Nama</label>
                        <input type="text" className="form-control" name="qty-label" placeholder="Nama" value={this.state.nama} onChange={(e)=> this.setState({nama:e.target.value})}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="qty-label">Keterangan</label>
                        <input type="text" className="form-control" name="qty-label" placeholder="Keterangan" value={this.state.keterangan} onChange={(e)=> this.setState({keterangan:e.target.value})}/>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <button onClick={()=> this.save()} className="btn btn-primary btn-utama ml-3">Simpan</button>
                </Modal.Footer>
            </Modal>
        </Fragment>
        )
    }
}