// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BaseComponent from '../components/BaseComponent'
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';
import Helper from '../Helper'
import queryString from 'query-string';

export default class GradesA extends Component {
    
    constructor(props) {
        super(props);
        
        this.params = queryString.parse(window.location.search)
    }

    render(){
        return (
        <Fragment>
            <Sidebar active="grades-a"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <GradesASub params={this.params}/>
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class GradesASub extends BaseComponent {
    pageTitle = "Produk Grade A"
    
    state = {
        profil_kartons: [],
        profil_karton: '',
        items: [],
        size : 10,
        page : 1,
        pages : [1],
        isLoading : false,
        isLoading1 : true,
        search : '',
        show : false,
        show1 : false,
        outerbox: '',
        grade: '',
        details: '',
        filter: 1,
        date1 : '',
        date2 : '',
        is_filter : false,
    }

    constructor(props) {
        super(props);
        this.params = props.params;
        console.log(this.params);

        // if(this.params.isadd === "1") {
        //     this.state.show = 1;
        //     // this.loadProfilKarton();
        // }

        this.loadProfilKarton();
    }

    role_name = "Pengisian Produk - Grade A"

    handleClose = () => this.setState({show:false});
    handleShow = () => {
        this.setState({show:true,isLoading1:true}, () => {
            this.nameInput.focus();
        });
        this.loadProfilKarton();
    };
    handleClose1 = () => {
        this.setState({show1:false,is_filter:false}, function() {
            this.loadData();
        })
    };
    handleShow1 = () => {
        this.setState({show1:true});
    };

    sizes = [
        10,20,50,100
    ]

    componentDidMount() {
        super.componentDidMount()
        if(!Helper.getAccessStatus(this.role_name, "view")) {
            window.location.href = "/notfound";
        }
        if(this.params.isadd === "1") {
            this.handleShow()
        }
        this.loadData();
    }

    loadData() {
        this.setState({isLoading:true})
        var url = process.env.REACT_APP_SERVER_URL + '/grades-a?page=' + this.state.page + '&size=' + this.state.size + '&search=' + this.state.search + '&lokasi=' + localStorage.getItem("LOCATION_ID") + '&profil_karton=' + this.state.profil_karton;
        if(this.state.is_filter && this.state.filter === 2) {
            url = url + "&date1=" + this.state.date1;
            url = url + "&date2=" + this.state.date2;
        }
        fetch(url)
            .then(res => res.json())
            .then((data) => {
                this.setState({ items: data.data, pages: data.pages })
                this.setState({isLoading:false})
            })
        .catch(console.log)
    }

    handleChange = (event) => {
        console.log(event.target.value);
        this.setState({ size: event.target.value }, function() {
            this.loadData();
        });
    };

    searchChange = (event) => {
        console.log(event.target.value);
        this.setState({search:event.target.value,page:1}, function() {
            this.loadData();
        });
    }

    pageChange = (event) => {
        console.log(event.target.getAttribute('data'));
        this.setState({ page: event.target.getAttribute('data') }, function() {
            this.loadData();
        });
    };

    handleProfilChange = (event) => {
        console.log(event.target.value);
        localStorage.setItem("PROFIL_KARTON", event.target.value)
        this.setState({ profil_karton: event.target.value }, function() {
            this.loadData();
        });
    };

    handleFilterChange = () => {
        this.setState({ is_filter: true, show1 : false }, function() {
            this.loadData();
        });
    };

    delete = (id) => {
        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            var data = {
                id:id
            }
            axios.post(process.env.REACT_APP_SERVER_URL+'/grades-a-delete', data)
                .then(response => response.data)
                .then((res) => {
                        // console.log(res);
                        if(res.status) {
                            // alert("Data berhasil dihapus");  
                            this.loadData();
                        }else {
                            alert(res.error);
                        }
                    }
                );
        }
    }
    
    loadProfilKarton() {
        fetch(process.env.REACT_APP_SERVER_URL + '/profil-karton-list')
            .then(res => res.json())
            .then((data) => {
                var profil_karton = '';
                data.data.forEach((e) => {
                    if(e.id.toString() === localStorage.getItem("PROFIL_KARTON")) {
                        profil_karton = e.id;
                        this.setState({})
                    }
                })
                this.setState({isLoading1:false,profil_karton:profil_karton,profil_kartons:data.data}, ()=>{this.loadData()})
            })
        .catch(console.log)
    }
    
    loadOuterLabel() {
        this.setState({isLoading1:true})
        if(this.state.profil_karton === '') {
            alert('Profil Karton harus diisi');
            return;
        }
        fetch(process.env.REACT_APP_SERVER_URL + '/labels-outer-by-code?code=' + this.state.outerbox)
            .then(res => res.json())
            .then((res) => {
                this.setState({isLoading1:false})
                if(res.status) {
                    var outerboxid = res.data.id;
                    fetch(process.env.REACT_APP_SERVER_URL + '/grades-outerbox-check?outerbox=' + outerboxid)
                        .then(res => res.json())
                        .then((res) => {
                            this.setState({isLoading1:false})
                            if(res.status) {
                                localStorage.setItem("PROFIL_KARTON", this.state.profil_karton)
                                window.location.href = '/grades-a-detail?id=0&outerbox=' + outerboxid + '&serial_no=' + this.state.outerbox + '&profil_karton=' + this.state.profil_karton;
                            }else {
                                alert(res.error);
                            }
                        })
                    .catch(console.log)
                }else {
                    alert(res.error);
                }
            })
        .catch(console.log)
    }

    loadGradeADetail(gradea) {
        fetch(process.env.REACT_APP_SERVER_URL + '/grades-a-by-id?id=' + gradea)
                .then(res => res.json())
                .then((res) => {
                    if(res.status) {
                        this.setState({'grade':gradea,'details' : res.data.details})
                    }else {
                        alert(res.error);
                    }
                })
            .catch(console.log)
    }

    onEnterPress = (f) => {
        if(f.keyCode === 13 && f.shiftKey === false) {
            f.preventDefault();
            this.loadOuterLabel()
        }
    }

    getTitle = (item) => {
        if(item.tipe === 1 || item.tipe === 2) {
            return (
                <div>
                    <strong>{Helper.getCurrentTime(item.created_at)} / {item.serial_no} / {item.info} - {item.color} / {item.nama} / {item.is_close === 1 ? "CLOSE":"OPEN"}</strong>
                </div>
            )
        } else if(item.tipe === 3 || item.tipe === 4) {
            return (
                <div>
                    <strong>{Helper.getCurrentTime(item.created_at)} / {item.serial_no} / {item.nama} / {item.is_close === 1 ? "CLOSE":"OPEN"}</strong>
                </div>
            )
        }
        return (
            <div>
                <strong>{item.nama} / {item.serial_no}</strong>
            </div>
        )
    }

    getFormattedDate1() {
        var curr = new Date();
        var date = curr.toISOString().substr(0,10);
        if(this.state.date1 !== '') {
            return this.state.date1.slice(0, 10);
        }else {
            // this.setState({date1:date})
        }
        return date;
    }

    getFormattedDate2() {
        var curr = new Date();
        var date = curr.toISOString().substr(0,10);
        if(this.state.date2 !== '') {
            return this.state.date2.slice(0, 10);
        }else {
            // this.setState({date2:date})
        }
        return date;
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/grades-a"><span className="bc-link">Produk Grade A</span></a></li>
                        </ol>
                    </div>
                    <div className="col text-right">
                        {
                            !Helper.getAccessStatus(this.role_name, "add") ? null : 
                            <button onClick={()=>{this.handleShow()}} className="btn btn-primary btn-utama" id="btn-grade-a-requirement">Tambah</button>
                        }
                    </div>
                </div>
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <h6 className="text-primary m-0 font-weight-bold">Daftar Produk</h6>
                        </div>
                        <div className="card-body">
                            <div className="row my-3">
                                <div className="col">
                                    <select className="custom-select max-view mb-3" defaultValue={this.state.size} onChange={this.handleChange} >
                                        {this.sizes.map((item) => (
                                            <option key={item} value={item}>{item} baris</option>
                                        ))}
                                    </select>&nbsp;
                                    <select key="2" className="custom-select max-view mb-3" value={this.state.profil_karton} onChange={this.handleProfilChange}>
                                        <option value="">Semua Profil</option>
                                        {this.state.profil_kartons.map((item) => (
                                            <option key={item.id} value={item.id}>{item.nama}</option>
                                        ))}
                                    </select>
                                </div>
                                <div className="col">
                                    <div className="input-group">
                                        <input onChange={this.searchChange} className="form-control" type="text" placeholder="Cari Produk" name="cari-produk"/>
                                        <div className="input-group-append"><button onClick={this.handleSearch} className="btn btn-info" type="button"><i className="fa fa-search"></i></button></div>
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                <table className="table dataTable my-0" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Waktu Input / Outerbox / PLU/SKU Name / Warna / Profil</th>
                                            <th>
                                                <div className="text-right"><a onClick={()=>this.handleShow1()} id="btn-filter-data" className="text-white" href="##"><i className="fa fa-filter"></i>&nbsp;<strong><span style={{textDecoration: "underline"}}>Filter Khusus</span></strong></a></div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div id="accordion-data" className="accordion m-0">
                                
                                    {this.state.items.map((item) => (
                                        <div key={item.id} className="card my-2">
                                            <div id={item.serial_no} className="card-header m-0 py-0">
                                                <div className="d-flex justify-content-between align-items-center align-items-xl-center">
                                                    <h1><button onClick={()=>this.loadGradeADetail(item.id)} className="btn btn-link" type="button" data-toggle="collapse" data-target={"#collapse-" + item.serial_no} aria-expanded="true" aria-controls={"collapse-" + item.serial_no}>{this.getTitle(item)}</button></h1>
                                                    {item.last_status !== "GRADE-A" ? null : 
                                                    <div>
                                                        {Helper.getAccessStatus(this.role_name, "edit") ? <a className="text-primary" mina1="#" href={"/grades-a-detail?id=" + item.id}><i className="fa fa-pencil"></i>&nbsp;Edit</a> : null}&nbsp;&nbsp;&nbsp;
                                                        {Helper.getAccessStatus(this.role_name, "delete") ? <a className="text-danger" mina1="#" onClick={()=>this.delete(item.id)} href="##"><i className="fa fa-trash-o"></i>&nbsp;Hapus</a> : null}
                                                    </div>
                                                    }
                                                </div>
                                            </div>
                                            <div id={"collapse-" + item.serial_no} className="collapse hide" aria-labelledby={item.serial_no} data-parent="#accordion-data">
                                                    <div className="card-body">
                                                        <div className="table-responsive">
                                                            <table className="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Nama Produk</th>
                                                                        <th>Barcode</th>
                                                                        <th>Warna</th>
                                                                        <th>Ukuran</th>
                                                                        <th>Innerbox</th>
                                                                        <th>Keterangan</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    {(this.state.grade !== item.id) ? (
                                                                        <tr>
                                                                        </tr>
                                                                    ) : this.state.details.map((subitem) => (
                                                                        <tr style={{background:(subitem.status === 1 || subitem.shipping_status === 0 ? "white" : "lightgrey")}} key={subitem.innerbox}>
                                                                            <td>{subitem.name}</td>
                                                                            <td>{subitem.barcode}</td>
                                                                            <td>{subitem.color}</td>
                                                                            <td>{subitem.size}</td>
                                                                            <td>{subitem.serial_no}</td>
                                                                            <td><div dangerouslySetInnerHTML={{__html:subitem.status === 0 ? ((subitem.shipping_status === 1 ? "Digunakan di " : "Batal digunakan di ") + subitem.note) : ""}}></div></td>
                                                                        </tr>
                                                                    ))}
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    ))}
                            </div>
                            <div className="row">
                                <div className="col-md-6 align-self-center">
                                    <div></div>
                                    <div className="mb-4">
                                        {/* <a href="/grades-a-import" className="btn btn-success mr-2" role="button">Impor Data</a> */}
                                        {/* <a href="/grades-a" className="btn btn-danger mr-2" role="button">Ekspor Data</a> */}
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <nav className="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers" style={{'float':'right'}}>
                                        <ul className="pagination">
                                            <li className={"page-item " + (this.state.pages[0] === 1 ? "disabled" : "")}><button data={this.state.pages[0]-1} className="page-link" aria-label="Previous" onClick={this.pageChange}>«</button></li>
                                            {this.state.pages.map((page) => (
                                                <li key={page} className={"page-item " + ((parseInt(this.state.page) === page ? "active" : ""))}><button data={page} className="page-link" onClick={this.pageChange}>{page}</button></li>
                                            ))}
                                            <li className="page-item"><button data={this.state.pages[this.state.pages.length-1]+1} className="page-link" aria-label="Next" onClick={this.pageChange}>»</button></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
                <Modal show={this.state.show} onHide={this.handleClose} animation={true} className="">
                    <Modal.Header closeButton>
                    <Modal.Title>Pilih Tipe Karton {this.state.isLoading1 ? ' (Loading ...)' : ''}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="form-group">
                            <label htmlFor="profil-selector">Pilih Profil</label>
                            <select onChange={(e)=>this.setState({profil_karton:e.target.value})} className="custom-select form-control" name="profil-selector" value={this.state.profil_karton}>
                                <option value=''>Pilihan Profil</option>
                                {this.state.profil_kartons.map((item) => (
                                    <option key={item.id} value={item.id}>{item.nama}</option>
                                ))}
                            </select>
                            <label htmlFor="kode-karton">Serial Outerbox</label>
                            <input ref={(input) => { this.nameInput = input; }} onKeyDown={(f)=>this.onEnterPress(f)} onChange={(e)=>this.setState({outerbox:e.target.value})} type="tel" className="form-control" name="kode-karton" placeholder="Mis: OBX00001"/>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.handleClose()} className="btn btn-warning">Batal</button>
                        <button onClick={()=> this.loadOuterLabel()} className="btn btn-primary btn-utama">Lanjutkan</button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.show1} onHide={this.handleClose1} animation={true} className="">
                    <Modal.Header closeButton>
                    <Modal.Title>Filter Data</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="form-group">
                            <div className="form-check my-2">
                                <input className="form-check-input" type="radio" id="formCheck-1" checked={this.state.filter === 1} onChange={(e)=>this.setState({filter:e.target.checked?1:2})}/>
                                <label className="form-check-label" htmlFor="formCheck-1">Tampilkan Semua Data</label>
                            </div>
                            <div className="form-check my-2">
                                <input className="form-check-input" type="radio" id="formCheck-2" checked={this.state.filter === 2} onChange={(e)=>this.setState({filter:e.target.checked?2:1})}/>
                                <label className="form-check-label" htmlFor="formCheck-2">Tampilkan Rentang Khusus</label>
                            </div>
                            <div className="form-group" style={{display:this.state.filter === 1 ? "none":""}}>
                                <div className="mb-2">
                                    <label htmlFor="alamat-surat">Mulai Tanggal</label>
                                    <input defaultValue={this.getFormattedDate1()} onChange={(e)=>this.setState({date1:e.target.value})} className="form-control" type="date"/>
                                </div>
                                <div className="mb-2">
                                    <label htmlFor="alamat-surat">Hingga Tanggal</label>
                                    <input defaultValue={this.getFormattedDate2()} onChange={(e)=>this.setState({date2:e.target.value})} className="form-control" type="date"/>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.handleClose1()} className="btn btn-warning">Batal</button>
                        <button onClick={()=> this.handleFilterChange()} className="btn btn-primary btn-utama">Lanjutkan</button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}