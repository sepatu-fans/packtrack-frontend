// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Helper from '../Helper'
import axios from 'axios';
import BaseComponent from '../components/BaseComponent'

export default class Items extends Component {

    render(){
        return (
        <Fragment>
            <Sidebar active="items"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <ItemsSub />
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class ItemsSub extends BaseComponent {
    
    pageTitle = "Data Produk"
    
    state = {
        items: [],
        size : 10,
        page : 1,
        pages : [1],
        isLoading : false,
        search : '',
        status : '',
        discontinue : ''
    }

    role_name = "Data Produk"

    sizes = [
        10,20,50,100
    ]

    componentDidMount() {
        super.componentDidMount()
        if(!Helper.getAccessStatus(this.role_name, "view")) {
            window.location.href = "/notfound";
        }
        this.loadData();
    }

    loadData() {
        this.setState({isLoading:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/items?page=' + this.state.page + '&size=' + this.state.size + '&status=' + this.state.status + '&discontinue=' + this.state.discontinue + '&search=' + this.state.search)
            .then(res => res.json())
            .then((data) => {
            this.setState({ items: data.data, pages: data.pages })
            this.setState({isLoading:false})
        })
        .catch(console.log)
    }

    handleChange = (event) => {
        console.log(event.target.value);
        this.setState({ size: event.target.value }, function() {
            this.loadData();
        });
    };

    handleStatusChange = (event) => {
        console.log(event.target.value);
        this.setState({ status: event.target.value }, function() {
            this.loadData();
        });
    };

    handleDiscontinueChange = (event) => {
        console.log(event.target.value);
        this.setState({ discontinue: event.target.value }, function() {
            this.loadData();
        });
    };

    searchChange = (event) => {
        console.log(event.target.value);
        this.setState({search:event.target.value,page:1}, function() {
            this.loadData();
        });
    }

    pageChange = (event) => {
        console.log(event.target.getAttribute('data'));
        this.setState({ page: event.target.getAttribute('data') }, function() {
            this.loadData();
        });
    };

    exportData() {
        window.open(process.env.REACT_APP_SERVER_URL + '/item-download')
    }

    delete = (id) => {
        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            var data = {
                id:id
            }
            axios.post(process.env.REACT_APP_SERVER_URL+'/item-delete', data)
                .then(response => response.data)
                .then((res) => {
                        // console.log(res);
                        if(res.status) {
                            // alert("Data berhasil dihapus");  
                            this.loadData();
                        }else {
                            alert(res.error);
                        }
                    }
                );
        }
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/items"><span className="bc-link">Daftar Data Item</span></a></li>
                        </ol>
                    </div>
                    <div className="col text-right">
                        {Helper.getAccessStatus(this.role_name, "add") ? <a href="/items-add" className="btn btn-primary btn-utama" role="button" id="btn-grade-a-requirement">Tambah</a> : null}
                    </div>
                </div>
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <h6 className="text-primary m-0 font-weight-bold">Daftar Produk</h6>
                        </div>
                        <div className="card-body">
                            <div className="row my-3">
                                <div className="col">
                                    <select className="custom-select max-view mb-3" defaultValue={this.state.size} onChange={this.handleChange} >
                                        {this.sizes.map((item) => (
                                            <option key={item} value={item}>{item} baris</option>
                                        ))}
                                    </select>&nbsp;
                                    <select key="2" className="custom-select max-view mb-3" onChange={this.handleStatusChange}>
                                        <option value="">Semua Produk</option>
                                        <option value="1">Produk Aktif</option>
                                        <option value="0">Produk Tidak Aktif</option>
                                    </select>&nbsp;
                                    <select key="3" className="custom-select max-view mb-3" onChange={this.handleDiscontinueChange}>
                                        <option value="">Semua Produk</option>
                                        <option value="0">Produk Belum Discontinue</option>
                                        <option value="1">Produk Discontinue</option>
                                    </select>
                                </div>
                                <div className="col">
                                    <div className="input-group">
                                        <input onChange={this.searchChange} className="form-control" type="text" placeholder="Cari Produk" name="cari-produk"/>
                                        <div className="input-group-append"><button onClick={this.handleSearch} className="btn btn-info" type="button"><i className="fa fa-search"></i></button></div>
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                <table className="table dataTable my-0" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Item Code</th>
                                            <th>SKU</th>
                                            <th>Nama SKU</th>
                                            <th>Barcode</th>
                                            <th>Warna</th>
                                            <th>Manufaktur</th>
                                            <th>Ukuran</th>
                                            <th>Kategori</th>
                                            <th>Status</th>
                                            <th>Discountinue</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.isLoading ? (
                                            <tr>
                                                <td colSpan="11">
                                                    Loading
                                                </td>
                                            </tr>
                                        ) : 
                                        this.state.items.map((item) => (
                                            <tr key={item.id}>
                                                <td>{item.code}</td>
                                                <td>{item.sku}</td>
                                                <td>{item.sku_name}</td>
                                                <td>{item.barcode}</td>
                                                <td>{item.color}</td>
                                                <td>{item.manufacturer}</td>
                                                <td>{item.size}</td>
                                                <td>{item.category_code}</td>
                                                <td>{item.status === 1 ? "Aktif" : "Tidak Aktif"}</td>
                                                <td>{item.discontinue === 0 ? "Tidak" : "Iya"}</td>
                                                <td>
                                                    {Helper.getAccessStatus(this.role_name, "edit") ? <a href={"/items-add?id=" + item.id} className="m-2 link-green">Edit</a> : null}
                                                    {Helper.getAccessStatus(this.role_name, "delete") ? <a onClick={()=>this.delete(item.id)} href="##" className="m-2 link-red">Hapus</a> : null}
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                            <div className="row">
                                <div className="col-md-6 align-self-center">
                                    <div></div>
                                    <div className="mb-4">
                                        {Helper.getAccessStatus(this.role_name, "import") ? <a href="/items-import" className="btn btn-success mr-2" role="button">Impor Data</a> : null}
                                        {Helper.getAccessStatus(this.role_name, "export") ? <button onClick={()=>this.exportData()} className="btn btn-danger mr-2">Ekspor Data</button> : null}
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <nav className="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers" style={{'float':'right'}}>
                                        <ul className="pagination">
                                            <li className={"page-item " + (this.state.pages[0] === 1 ? "disabled" : "")}><button data={this.state.pages[0]-1} className="page-link" aria-label="Previous" onClick={this.pageChange}>«</button></li>
                                            {this.state.pages.map((page) => (
                                                <li key={page} className={"page-item " + ((parseInt(this.state.page) === page ? "active" : ""))}><button data={page} className="page-link" onClick={this.pageChange}>{page}</button></li>
                                            ))}
                                            <li className="page-item"><button data={this.state.pages[this.state.pages.length-1]+1} className="page-link" aria-label="Next" onClick={this.pageChange}>»</button></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}