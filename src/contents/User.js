// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BaseComponent from '../components/BaseComponent'
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';
import DropdownList from 'react-widgets/lib/DropdownList'
import md5 from 'md5'
import Helper from '../Helper'

export default class User extends Component {

    render(){
        return (
        <Fragment>
            <Sidebar active="user"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <UserSub />
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class UserSub extends BaseComponent {
    pageTitle = 'Manajemen User'
    
    state = {
        items: [],
        size : 10,
        page : 1,
        pages : [1],
        isLoading : false,
        search : '',
        show : false,
        roles : [],
        locations : [],
        role : false,
        username : "",
        name : "",
        id : 0,
    }

    role_name = "Manajemen User"

    handleClose = () => this.setState({show:false});
    handleShow = () => this.setState({id:0,show:true});

    handleCloseAccess = () => this.setState({showAccess:false});
    handleShowAccess = (id) => this.setState({id:id,showAccess:true});

    sizes = [
        10,20,50,100
    ]

    componentDidMount() {
        super.componentDidMount()
        if(!Helper.getAccessStatus(this.role_name, "view")) {
            window.location.href = "/notfound";
        }
        this.loadRoles();
        this.loadLocation();
        this.loadData();
    }

    loadData() {
        this.setState({isLoading:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/user?page=' + this.state.page + '&size=' + this.state.size + '&search=' + this.state.search)
            .then(res => res.json())
            .then((data) => {
                this.setState({ items: data.data, pages: data.pages })
                this.setState({isLoading:false})
            })
        .catch(console.log)
    }

    loadRoles() {
        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/role-list')
            .then(res => res.json())
            .then((data) => {
                this.setState({roles: data.data })
                this.setState({isLoading1:false})
            })
        .catch(console.log)
    }

    loadLocation() {
        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/location')
            .then(res => res.json())
            .then((data) => {
                this.setState({locations: data.data })
                this.setState({isLoading1:false})
            })
        .catch(console.log)
    }

    handleChange = (event) => {
        console.log(event.target.value);
        this.setState({ size: event.target.value }, function() {
            this.loadData();
        });
    };

    searchChange = (event) => {
        console.log(event.target.value);
        this.setState({search:event.target.value,page:1}, function() {
            this.loadData();
        });
    }

    pageChange = (event) => {
        console.log(event.target.getAttribute('data'));
        this.setState({ page: event.target.getAttribute('data') }, function() {
            this.loadData();
        });
    };

    save() {
        if(!this.state.username) {
            alert('Nama User harus diisi');
            return;
        }
        if(this.state.id === 0 && !this.state.password) {
            alert('Password harus diisi');
            return;
        }
        if(this.state.password !== this.state.repassword) {
            alert('Password dan Ulangi Password tidak sama');
            return;
        }
        if(!this.state.role) {
            alert('Role harus diisi');
            return;
        }

        var data = {
            role_id:this.state.role.id,
            username:this.state.username,
            password:!this.state.password || this.state.password === '' ? '' : md5(this.state.password),
            created_at:Helper.getCurrentTime(null, 7),
            created_by:localStorage.getItem("USERID"),
        }
        if(this.state.id !== 0) {
            data.id = this.state.id;
        }
        axios.post(process.env.REACT_APP_SERVER_URL+'/user-add', data)
            .then(response => response.data)
            .then((res) => {
                    // console.log(res);
                if(res.status) {
                    // alert("Data berhasil disimpan");  
                    this.loadData();
                    this.setState({
                        username : "",
                        password : "",
                        repassword : "",
                        role : false,
                        show : this.state.id === 0
                    })
                }else {
                    alert(res.error);
                }
            }
        );
    }

    edit(id) {
        fetch(process.env.REACT_APP_SERVER_URL + '/user-by-id?id=' + id)
                .then(res => res.json())
                .then((data) => {
                    if(data.status) {
                        var role = false;
                        this.state.roles.forEach((e) => {
                            if(data.data.role_id === e.id) {
                                role = e;
                            }
                        })
                        this.setState({
                            id:id,
                            username:data.data.username,
                            pasword:"",
                            role:role,
                            show:true
                        })
                    }else {
                        alert(data.error);
                    }
                })
            .catch(console.log)
    }

    delete = (id) => {
        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            var data = {
                id:id
            }
            axios.post(process.env.REACT_APP_SERVER_URL+'/user-delete', data)
                .then(response => response.data)
                .then((res) => {
                        // console.log(res);
                        if(res.status) {
                            // alert("Data berhasil dihapus");  
                            this.loadData();
                        }else {
                            alert(res.error);
                        }
                    }
                );
        }
    }

    changeAccess(e,g) {
        var access = this.state.access;
        var isSkip = false;
        access.forEach((a) => {
            if(a[0] === e) {
                a[1] = g;
                isSkip = true;
            }
        });
        if(!isSkip) {
            access.push([e,g]);
        }

        console.log(access);
        this.setState({
            access:access
        })
    }

    haveAccess(e) {
        var access = this.state.access;
        if(!access) {
            return;
        }
        var result = false;
        access.forEach((a) => {
            if(a[0] === e) {
                result = a[1];
            }
        });

        return result;
    }

    saveAccess() {
        
        var data = {
            id:this.state.id,
            access:JSON.stringify(this.state.access)
        }
        axios.post(process.env.REACT_APP_SERVER_URL+'/user-location', data)
            .then(response => response.data)
            .then((res) => {
                    // console.log(res);
                if(res.status) {
                    // alert("Data berhasil disimpan");  
                    this.handleCloseAccess();
                    this.loadData();
                }else {
                    alert(res.error);
                }
            }
        );
    }

    editAccess(id,lokasi) {
        fetch(process.env.REACT_APP_SERVER_URL + '/location')
                .then(res => res.json())
                .then((data) => {
                    if(data.data) {
                        var locations = data.data;
                        var lokasi_akses = [];
                        if(lokasi === "" || lokasi === null) {
                            lokasi_akses = [];
                        }else {
                            lokasi_akses = JSON.parse(lokasi)
                        }
                        console.log(locations);
                        locations.forEach((e) => {
                            var isSkip = false;
                            lokasi_akses.forEach((f) => {
                                if(f[0] === e.id) {
                                    isSkip = true
                                }
                            })
                            if(!isSkip) {
                                lokasi_akses.push([e.id,false])
                            }
                        })
                        console.log(lokasi_akses);
                        this.setState({
                            id:id,
                            access:lokasi_akses,
                            locations:locations,
                            showAccess:true
                        })
                    }else {
                        alert(data.error);
                    }
                })
            .catch(console.log)
    }

    checkAll(e) {
        var access = this.state.access;
        var statusAll = this.state.status_all;
        if(statusAll === undefined) {
            statusAll = true;
        }else {
            statusAll = !statusAll;
        }

        access.forEach((a) => {
            
            if(a[0] === e || e === "") {
                a[1] = statusAll;
            }
        });
        this.setState({
            access:access,
            status_all:statusAll
        })
    }

    getLocation(lokasi) {
        var result = "";
        if(lokasi !== "" && lokasi !== "null" && lokasi !== null) {
            var locations = JSON.parse(lokasi);
            if(locations !== null) {
                locations.forEach((e) => {
                    this.state.locations.forEach((f) => {
                        if(e[0] === f.id && e[1]) {
                            result += f.nama_site + " - " + f.nama + "<br/>";
                        }
                    })
                })
            }
        }
        return {__html: result};
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/user"><span className="bc-link">Manajemen User</span></a></li>
                        </ol>
                    </div>
                    <div className="col text-right">
                        {Helper.getAccessStatus(this.role_name, "add") ? <button onClick={()=>{this.handleShow()}} className="btn btn-primary btn-utama" id="btn-grade-a-requirement">Tambah</button> : null}
                    </div>
                </div>
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <h6 className="text-primary m-0 font-weight-bold">Daftar Manajemen User</h6>
                        </div>
                        <div className="card-body">
                            <div className="row my-3">
                                <div className="col">
                                    <select className="custom-select max-view mb-3" defaultValue={this.state.size} onChange={this.handleChange} >
                                        {this.sizes.map((item) => (
                                            <option key={item} value={item}>{item} baris</option>
                                        ))}
                                    </select>
                                </div>
                                <div className="col">
                                    <div className="input-group">
                                        <input onChange={this.searchChange} className="form-control" type="text" placeholder="Cari Produk" name="cari-produk"/>
                                        <div className="input-group-append"><button onClick={this.handleSearch} className="btn btn-info" type="button"><i className="fa fa-search"></i></button></div>
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                <table className="table dataTable my-0" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Nama User</th>
                                            <th>Role</th>
                                            <th>Lokasi</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.isLoading ? (
                                            <tr>
                                                <td colSpan="4">
                                                    Loading
                                                </td>
                                            </tr>
                                        ) : 
                                        this.state.items.map((item) => (
                                            <tr key={item.id}>
                                                <td>{item.username}</td>
                                                <td>{item.role}</td>
                                                <td><div dangerouslySetInnerHTML={this.getLocation(item.lokasi)}></div></td>
                                                <td>
                                                    {Helper.getAccessStatus(this.role_name, "edit") ? <a onClick={()=>this.edit(item.id)} href="##" className="m-2 link-green">Edit</a> : null}
                                                    {Helper.getAccessStatus(this.role_name, "delete") ? <a onClick={()=>this.delete(item.id)} href="##" className="m-2 link-red">Hapus</a> : null}
                                                    {Helper.getAccessStatus(this.role_name, "location") ? <a onClick={()=>this.editAccess(item.id, item.lokasi)} href="##" className="m-2 link">Lokasi</a> : null}
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                            <div className="row">
                                <div className="col-md-6 align-self-center">
                                    <div></div>
                                    <div className="mb-4">
                                        {/* <a href="/user-import" className="btn btn-success mr-2" role="button">Impor Data</a> */}
                                        {/* <a href="/user" className="btn btn-danger mr-2" role="button">Ekspor Data</a> */}
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <nav className="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers" style={{'float':'right'}}>
                                        <ul className="pagination">
                                            <li className={"page-item " + (this.state.pages[0] === 1 ? "disabled" : "")}><button data={this.state.pages[0]-1} className="page-link" aria-label="Previous" onClick={this.pageChange}>«</button></li>
                                            {this.state.pages.map((page) => (
                                                <li key={page} className={"page-item " + ((parseInt(this.state.page) === page ? "active" : ""))}><button data={page} className="page-link" onClick={this.pageChange}>{page}</button></li>
                                            ))}
                                            <li className="page-item"><button data={this.state.pages[this.state.pages.length-1]+1} className="page-link" aria-label="Next" onClick={this.pageChange}>»</button></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
                <Modal show={this.state.show} onHide={this.handleClose} animation={true} className="">
                    <Modal.Header closeButton>
                    <Modal.Title>{this.state.id === 0 ? "Buat" : "Ubah"} User</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="form-group">
                            <label htmlFor="qty-label">Nama User</label>
                            <input type="text" className="form-control" name="qty-label" placeholder="Nama User" value={this.state.username} onChange={(e)=> this.setState({username:e.target.value})}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="qty-label">Password {this.state.id === '' ? '' : '(Kosongi jika tidak ingin mengubah password)'}</label>
                            <input type="password" className="form-control" name="qty-label" placeholder="Password" value={this.state.password} onChange={(e)=> this.setState({password:e.target.value})}/>
                            <label htmlFor="qty-label">Ulangi Password {this.state.id === '' ? '' : '(Kosongi jika tidak ingin mengubah password)'}</label>
                            <input type="password" className="form-control" name="qty-label" placeholder="Ulangi Password" value={this.state.repassword} onChange={(e)=> this.setState({repassword:e.target.value})}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="qty-label">Role</label>
                            <DropdownList
                                data={this.state.roles}
                                textField={item => item === false ? "Pilih Role" : item.nama}
                                filter
                                busy={this.state.isLoading2}
                                onChange={value => this.setState({role:value})}
                                value={this.state.role}
                            />
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.handleClose()} className="btn btn-warning">Batal</button>
                        <button onClick={()=> this.save()} className="btn btn-primary btn-utama ml-3">Simpan</button>
                    </Modal.Footer>
                </Modal>
            
                <Modal show={this.state.showAccess} onHide={this.handleCloseAccess} animation={true} size="lg">
                    <Modal.Header closeButton>
                    <Modal.Title>Akses Lokasi <button onClick={()=> this.checkAll("")} className="btn btn-sm btn-primary btn-utama ml-3">All</button></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <table style={{width:"100%"}}>
                            <tbody>
                                {this.state.locations.map((e) => (
                                    <Fragment key={e.id}>
                                        <tr>
                                            <td>
                                                {e.nama_site} - {e.nama}
                                            </td>
                                            <td>
                                                <label>
                                                    <input checked={this.haveAccess(e.id)} onChange={(g)=>this.changeAccess(e.id, g.target.checked)} type="checkbox"></input> Akses&nbsp;&nbsp;
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colSpan="2"><hr/></td>
                                        </tr>
                                    </Fragment>
                                ))}
                            </tbody>
                        </table>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.saveAccess()} className="btn btn-primary btn-utama ml-3">Simpan</button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}