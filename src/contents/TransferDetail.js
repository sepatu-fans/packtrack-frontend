// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BaseComponent from '../components/BaseComponent'
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';
import queryString from 'query-string';
import Helper from '../Helper'
import DropdownList from 'react-widgets/lib/DropdownList'

export default class TransferDetail extends Component {

    constructor(props) {
        super(props);
        this.params = queryString.parse(window.location.search)
    }

    render(){
        return (
        <Fragment>
            <Sidebar active="transfer"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <TransferDetailSub params={this.params}/>
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class TransferDetailSub extends BaseComponent {

    pageTitle = 'Tambah Pemindahan'
    
    state = {
        id: 0,
        verifikasi: false,
        view: false,
        items: [],
        items_inner: [],
        details: [],
        details_original: [],
        deletes: [],
        details_inner: [],
        details_inner_original: [],
        deletes_inner: [],
        subitems: [],
        isLoading1 : false,
        search : '',
        show : false,
        outerbox: '',
        innerbox: '',
        code : '',
        no_so: '',
        no_surat: '',
        nama_toko: '',
        tujuan: '',
        grade1: '',
        no_so1: '',
        no_surat1: '',
        tujuan1: '',
        grade2: '',
        locations: [],
        location: false,
        grades: [],
        grade: false,
        resume_items: []
    }

    componentDidMount() {
        super.componentDidMount()
    }

    constructor(props) {
        super(props);
        this.params = props.params;
        console.log(this.params);

        console.log(Helper.getCurrentTime(null, 7));

        if(this.params.id !== undefined && this.params.id !== '0') {
            if(!Helper.getAccessStatus("Pemindahan Langsung", "edit")) {
                window.location.href = "/notfound";
            }
            this.state.id = this.params.id;
            this.state.verifikasi = this.params.verifikasi !== undefined;
            this.state.cancel = this.params.cancel !== undefined;
            this.state.view = this.params.view !== undefined;

            fetch(process.env.REACT_APP_SERVER_URL + '/transfer-by-id?id=' + this.params.id)
                .then(res => res.json())
                .then((res) => {
                    if(res.status) {
                        console.log(res);
                        this.setState({
                            'tujuan':res.data.transfer.tujuan.toString(),
                            'tujuan_nama':res.data.transfer.tujuan_nama,
                            'grade1':res.data.transfer.grade.toString(),
                            'grade_nama':res.data.transfer.grade_nama,
                            'no_so':res.data.transfer.no_so,
                            'status':res.data.transfer.status,
                            'no_surat':res.data.transfer.no_surat,
                            'keterangan':res.data.transfer.keterangan,
                            'username':res.data.transfer.username,
                            'details':res.data.details,
                            'details_original':res.data.details.map((x)=>x),
                            'details_inner':res.data.details_inner,
                            'details_inner_original':res.data.details_inner.map((x)=>x),
                        });
                        this.loadTransferDetail(res.data.details);
                        this.loadTransferDetailInner(res.data.details_inner);
                    }else {
                        alert(res.error);
                        window.history.back();  
                    }
                })
            .catch(console.log)
        }else {
            if(!Helper.getAccessStatus("Pemindahan Langsung", "add")) {
                window.location.href = "/notfound";
            }

            this.state.tujuan = this.params.tujuan;
            this.state.tujuan_nama = this.params.tujuan_nama;
            this.state.no_so = this.params.no_so;
            this.state.no_surat = this.params.no_surat;
            this.state.grade1 = this.params.grade;
            this.state.grade_nama = this.params.grade_nama;
            this.state.keterangan = this.params.keterangan;
        }
    }

    handleClose = () => this.setState({show:false});
    handleShow = () => {
        this.setState({
            no_so1:this.state.no_so,
            no_surat1:this.state.no_surat,
            nama_toko1:this.state.nama_toko,
            alamat1:this.state.alamat,
            keterangan1:this.state.keterangan,
        })
        this.loadLocations();
        this.loadGrades();
        this.setState({show:true});
    };

    loadLocations() {
        fetch(process.env.REACT_APP_SERVER_URL + '/location')
            .then(res => res.json())
            .then((res) => {
                this.setState({ locations: res.data})
                res.data.forEach((e) => {
                    if(e.id.toString() === this.state.tujuan) {
                        this.setState({location : e})
                    }
                });
            })
        .catch(console.log)
    }

    loadGrades() {
        fetch(process.env.REACT_APP_SERVER_URL + '/grades')
            .then(res => res.json())
            .then((res) => {
                var grades = res.data
                grades.unshift({id:0,nama:"Grade A"})
                grades.unshift({id:-1,nama:"Tidak Ada Perubahan"})
                this.setState({ grades: grades})
                res.data.forEach((e) => {
                    if(e.id.toString() === this.state.grade1) {
                        this.setState({grade : e})
                    }
                });
            })
        .catch(console.log)
    }

    saveSurat = () => {
        // if(this.state.no_surat1 === '') {
        //     alert('No Surat harus diisi');
        //     return;
        // }
        if(this.state.no_so1 === '') {
            alert('No. Permintaan Order harus diisi');
            return;
        }
        if(!this.state.location) {
            alert('Lokasi Tujuan harus diisi');
            return;
        }
        if(!this.state.grade) {
            alert('Grade harus diisi');
            return;
        }

        this.setState({
            no_so:this.state.no_so1,
            no_surat:this.state.no_surat1,
            tujuan:this.state.location.id.toString(),
            tujuan_nama:this.state.location.nama_site + " - " + this.state.location.nama,
            grade:this.state.grade.id,
            grade_nama:this.state.grade.nama,
            keterangan:this.state.keterangan1,
            grade1:this.state.grade.id.toString(),
        })
        this.handleClose();
    }

    delete = (outerbox) => {
        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            var newdetails = [];
            var deletes = this.state.deletes;
            this.state.details.forEach(elem => {
                console.log(elem);
                console.log(outerbox)
                if(elem[0] === outerbox) {
                    deletes.push(elem);
                }else {
                    newdetails.push(elem);
                }
            });
            console.log(deletes);
            console.log(newdetails);
            this.setState({'items':[],'details':newdetails,'deletes':deletes});
            if(newdetails.length === 0) {
                this.loadResumeItems();
            }else {
                this.loadTransferDetail(newdetails)
            }
        }
    }

    deleteInner = (innerbox) => {
        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            if(innerbox === 0) {
                this.setState({'items_inner':[],'details_inner':[]});
            }else {
                var newdetails = [];
                var deletes_inner = this.state.deletes_inner;
                this.state.details_inner.forEach(elem => {
                    console.log(elem);
                    console.log(innerbox)
                    if(elem[0] === innerbox) {
                        deletes_inner.push(elem);
                    }else {
                        newdetails.push(elem);
                    }
                });
                console.log(deletes_inner);
                console.log(newdetails);
                if(newdetails.length === 0) {
                    this.setState({'items_inner':[],'details_inner':newdetails,'deletes_inner':deletes_inner}, function() {
                        this.loadResumeItems();
                    });
                }else {
                    this.setState({'items_inner':[],'details_inner':newdetails,'deletes_inner':deletes_inner}, function() {
                        this.loadTransferDetailInner(newdetails)
                    });
                }
            }
        }
    }

    loadLabel() {
        if(this.state.code === "") {
            alert("Outerbox / Innerbox harus diisi");
            return;
        }
        
        this.setState({'outerbox':this.state.code}, (e)=> {
            this.loadOuterLabel();
        });
    }

    onEnterPress = (f) => {
        if(f.keyCode === 13 && f.shiftKey === false) {
            f.preventDefault();
            this.loadOuterLabel()
        }
    }

    onEnterPress1 = (f) => {
        if(f.keyCode === 13 && f.shiftKey === false) {
            f.preventDefault();
            this.loadInnerLabel()
        }
    }
    
    loadOuterLabel() {
        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/transfer-labels-outer-by-code?code=' + this.state.outerbox + '&transfer=' + this.state.id)
            .then(res => res.json())
            .then((res) => {
                this.setState({isLoading1:false})
                if(res.status) {
                    var outerboxid = res.data.outerbox;
                    if(res.data.lokasi.toString() !== localStorage.getItem("LOCATION_ID")) {
                        alert("Lokasi Barang tidak sama dengan lokasi user");
                        return;
                    }
                    if(res.data.is_close === 0) {
                        alert("Outerbox masih terbuka");
                        return;
                    }
                    var details = this.state.details;
                    var isExist = false;
                    details.forEach((e) => {
                        if(e[0] === outerboxid) {
                            alert("Outerbox sudah pernah dimasukkan");
                            isExist = true;
                            return
                        }
                    })
                    if(isExist) {
                        return;
                    }
                    var deletes = this.state.deletes;
                    if(deletes.indexOf(outerboxid) >= 0) {
                        deletes.splice(deletes.indexOf(outerboxid), 1);
                    }

                    details.push([outerboxid]);
                    this.setState({details:details,deletes:deletes,outerbox:""})
                    this.loadTransferDetail(details)
                }else {
                    alert(res.error);
                    this.setState({'outerbox':''});
                }
            })
        .catch(console.log)
    }

    loadInnerLabel() {
        if(this.isDisable) {
            return
        }
        this.isDisable = true;

        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/labels-inner-by-code?code=' + this.state.innerbox + '')
            .then(res => res.json())
            .then((res) => {
                this.setState({isLoading1:false})
                if(res.status) {
                    var innerboxid = res.data.innerbox;
                    var details_inner = this.state.details_inner;

                    var isExist = false;
                    details_inner.forEach((e) => {
                        if(e[0] === innerboxid) {
                            alert("Innerbox sudah pernah dimasukkan");
                            isExist = true;
                            this.isDisable = false;
                            this.setState({'innerbox':''});
                            return
                        }
                    })
                    if(isExist) {
                        return;
                    }

                    fetch(process.env.REACT_APP_SERVER_URL + '/transfer-innerbox-check?innerbox=' + innerboxid + '&transfer=' + this.state.id + '&lokasi=' + localStorage.getItem("LOCATION_ID"))
                        .then(res => res.json())
                        .then((res) => {
                            if(res.status) {
                                details_inner.push([innerboxid]);
                                var deletes_inner = this.state.deletes_inner;
                                if(deletes_inner.indexOf(innerboxid) >= 0) {
                                    deletes_inner.splice(deletes_inner.indexOf(innerboxid), 1);
                                }

                                console.log(details_inner);

                                this.setState({'details_inner':details_inner,'deletes_inner':deletes_inner,'innerbox':''})
                                this.loadTransferDetailInner(details_inner)
                                this.isDisable = false;
                            }else {
                                if(res.error) {
                                    alert(res.error);
                                    this.isDisable = false;
                                }else {
                                    var data = {
                                        id:res.data.reffid,
                                        innerbox:res.data.innerbox,
                                        tipe:res.data.type,
                                        note: "SO nomor "+this.state.no_so+"<br/>pada "+Helper.getCurrentTime(null, 7)
                                    }
                                    axios.post(process.env.REACT_APP_SERVER_URL + '/transfer-innerbox-out', data)
                                        .then(res => res.data)
                                        .then((res) => {
                                            if(res.status) {
                                                details_inner.push([innerboxid]);
                                                var deletes_inner = this.state.deletes_inner;
                                                if(deletes_inner.indexOf(innerboxid) >= 0) {
                                                    deletes_inner.splice(deletes_inner.indexOf(innerboxid), 1);
                                                }

                                                console.log(details_inner);

                                                this.setState({'details_inner':details_inner,'deletes_inner':deletes_inner,'innerbox':''})
                                                this.loadTransferDetailInner(details_inner)
                                            }else {
                                                alert(res.error);
                                            }
                                            this.isDisable = false;
                                            this.setState({innerbox:""})
                                        })
                                    .catch(console.log)
                                }
                                this.setState({'innerbox':''});
                            }
                        })
                    .catch(console.log)
                }else {
                    alert(res.error);
                    this.setState({'innerbox':''});
                    this.isDisable = false;
                }
            })
        .catch(console.log)
    }

    loadTransferDetail(details) {
        if(details.length === 0) {
            return;
        }

        var param = "";
        details.forEach(elem => {
            param += "id=" + elem[0] + "&";
        });
        fetch(process.env.REACT_APP_SERVER_URL + '/transfer-details-by-ids?' + param)
                .then(res => res.json())
                .then((data) => {
                    if(data.status) {
                        this.setState({items:data.data})
                        this.loadResumeItems()
                    }
                })
            .catch(console.log)
    }

    loadTransferDetailInner(details) {
        if(details.length === 0) {
            return;
        }

        var param = "";
        details.forEach(elem => {
            param += "id=" + elem[0] + "&";
        });
        fetch(process.env.REACT_APP_SERVER_URL + '/labels-inner-by-ids?' + param)
                .then(res => res.json())
                .then((data) => {
                    if(data.status) {
                        this.setState({items_inner:data.data})
                        this.loadResumeItems();
                    }
                })
            .catch(console.log)
    }

    loadTransferDetailItem(outerbox) {
        fetch(process.env.REACT_APP_SERVER_URL + '/transfer-detail-items-by-outerbox?outerbox=' + outerbox)
                .then(res => res.json())
                .then((res) => {
                    if(res.status) {
                        this.setState({'subitems':res.data})
                    }else {
                        alert(res.error);
                    }
                })
            .catch(console.log)
    }

    getTotalBarang() {
        var total = 0;
        this.state.items.forEach((e) => {
            total += parseInt(e.total);
        })
        // total += this.state.items_inner.length;
        return total;
    }

    getGrade(id, grade) {
        var result = grade;
        console.log(this.state.details_inner);
        this.state.details_inner.forEach((e) => {
            if(e[0] === id && e[2] !== undefined) {
                result = e[2];
            }
        })

        return result;
    }

    getGradeObx(id, grade) {
        var result = grade;
        console.log(this.state.details);
        this.state.details.forEach((e) => {
            if(e[0] === id && e[4] !== undefined) {
                result = e[4];
            }
        })

        return result;
    }

    save = (status) => {
        if(this.state.details.length === 0 && this.state.details_inner.length === 0) {
            alert("Belum ada outerbox / innerbox nobox");
            return;
        }
        if(true) {
            var details = [];
            this.state.details.forEach((e) => {
                if(this.state.id === 0) {
                    details.push(e);
                }else if(this.state.details_original.indexOf(e) < 0) {
                    details.push(e);
                }
            });
            var details_inner = [];
            this.state.details_inner.forEach((e) => {
                if(this.state.id === 0) {
                    details_inner.push(e);
                }else if(this.state.details_inner_original.indexOf(e) < 0) {
                    details_inner.push(e);
                }
            });
            var data = {
                transfer:this.state.id,
                no_so:this.state.no_so,
                no_surat:this.state.no_surat,
                tujuan:this.state.tujuan,
                grade:this.state.grade1,
                keterangan:this.state.keterangan,
                details:details,
                deletes:this.state.deletes,
                details_inner:details_inner,
                deletes_inner:this.state.deletes_inner,
                status:status,
                created_at:Helper.getCurrentTime(null, 7),
                created_by:localStorage.getItem("USERID"),
                lokasi:localStorage.getItem("LOCATION_ID")
            }
            console.log(details_inner);
            console.log(data);
            axios.post(process.env.REACT_APP_SERVER_URL+'/transfer-detail-save', data)
                .then(response => response.data)
                .then((res) => {
                        // console.log(res);
                        if(res.status) {
                            // alert("Data berhasil disimpan");
                            window.location.href='/pemindahan';
                        }else {
                            alert(res.error);
                        }
                    }
                );
        }
    }

    getOuterBoxTitle = (item) => {
        var title = item.grade + " / " + item.serial_no + " / " + item.total;
        if(item.tipe === "A") {
            title += " / " + item.info + " / " + item.color + " / " + item.profil_karton;
        }
        return title;
    }

    loadResumeItems() {
        var param = "";
        this.state.details.forEach(elem => {
            param += "outerbox=" + elem[0] + "&";
        });
        if(param === "") {
            var items = []
            this.state.items_inner.forEach((subitem) => {
                items.push(subitem);
            })
            this.setState({resume_items:items})
        }else {
            fetch(process.env.REACT_APP_SERVER_URL + '/transfer-detail-items-by-outerboxs?' + param)
                    .then(res => res.json())
                    .then((res) => {
                        if(res.status) {
                            var items = []
                            this.state.items_inner.forEach((subitem) => {
                                items.push(subitem);
                            })
                            res.data.forEach((subitem) => {
                                items.push(subitem);
                            })
                            this.setState({resume_items:items})
                        }
                    })
                .catch(console.log)
        }
    }

    getResumeItems() {
        var items = [];
        this.state.resume_items.forEach((subitem) => {
            var itemExisting = false;
            items.forEach((i) => {
                if(i.sku_name === subitem.sku_name && i.color === subitem.color) {
                    itemExisting = i;
                    return;
                }
            });

            if(!itemExisting) {
                itemExisting = {
                    sku_name: subitem.sku_name,
                    color: subitem.color,
                    items: []
                };
                items.push(itemExisting);
            }

            var isExist = false;
            itemExisting.items.forEach((i) => {
                if(i.size === subitem.size) {
                    isExist = true;
                    i.total = i.total + 1;
                    return;
                }
            })

            if(!isExist) {
                itemExisting.items.push({
                    size: subitem.size,
                    total: 1
                })
            }

        });

        return items;
    }

    getTotal(e) {
        var result = 0;
        e.forEach((f) => {
            result = result + f.total;
        })

        return result;
    }

    getTotalAll() {
        var resume = this.getResumeItems();
        var total = 0;
        resume.forEach((i) => {
            i.items.forEach((j) => {
                total = total + j.total;
            })
        })

        return total;
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/pemindahan"><span className="bc-link">Pemindahan Produk</span></a></li>
                            <li className="breadcrumb-item"><a href="/pemindahan-detail"><span className="bc-link">{this.state.id === 0 ? 'Tambah' : 'Ubah'} Daftar Permintaan Pemindahan</span></a></li>
                        </ol>
                    </div>
                </div>
                
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <h6 className="text-primary m-0 font-weight-bold">Permintaan Pemindahan</h6>
                        </div>
                        <div className="card-body">
                            <h4 className="mb-4"><strong>Informasi Pemindahan</strong></h4>
                            <div className="row">
                                <div className="col-12 col-lg-3 col-xl-3">
                                    <div className="mb-3">
                                        <label htmlFor="id-surat">No. Permintaan/Order</label>
                                        <input value={this.state.no_so} type="text" className="form-control" name="id-surat" readOnly/>
                                    </div>
                                </div>
                                <div className="col-12 col-lg-3 col-xl-3">
                                    <div className="mb-3">
                                        <label htmlFor="id-surat">No. Surat</label>
                                        <input value={this.state.no_surat} type="text" className="form-control" name="id-surat" readOnly/>
                                    </div>
                                </div>
                                <div className="col-md-12 col-lg-3 col-xl-3">
                                    <div className="mb-3">
                                        <label htmlFor="tujuan">Tujuan</label>
                                        <input value={this.state.tujuan_nama} type="text" className="form-control" name="tujuan" readOnly/>
                                    </div>
                                </div>
                                <div className="col-md-12 col-lg-3 col-xl-3">
                                    <div className="mb-3">
                                        <label htmlFor="grade">Grade</label>
                                        <input value={this.state.grade_nama} type="text" className="form-control" name="grade" readOnly/>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12 col-lg-3 col-xl-6">
                                    <div className="mb-3">
                                        <label htmlFor="keterangan">Keterangan</label>
                                        <input value={this.state.keterangan} type="text" className="form-control" name="keterangan" readOnly/>
                                    </div>
                                </div>
                                <div className="col-md-12 col-lg-3 col-xl-3">
                                    <div className="mb-3">
                                        <label htmlFor="tujuan">Dibuat Oleh</label>
                                        <input value={this.state.username} type="text" className="form-control" name="tujuan" readOnly/>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div className="text-right">
                                {this.state.view ? null : <button onClick={()=>this.handleShow()} className="btn btn-primary btn-utama" id="btn-id-surat">Ubah Data Pemindahan</button>}
                            </div>

                            <br/>
                            <h4 className="mb-4"><strong>Ringkasan Data Barang</strong></h4>
                            
                            <div className="table-responsive table mt-2">
                                <table className="table">
                                    <thead>
                                        <tr style={{backgroundColor:"var(--red)"}}>
                                            <th>Nama Barang</th>
                                            <th>Warna</th>
                                            <th>Rincian Jumlah Barang (ukuran/jumlah)	</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {(this.getResumeItems().map((subitem) => (
                                            <tr key={subitem.sku_name}>
                                                <td>{subitem.sku_name}</td>
                                                <td>{subitem.color}</td>
                                                <td>
                                                    {subitem.items.map((e) => (
                                                        <span key={e.size}>
                                                            {e.size}/{e.total},&nbsp;
                                                        </span>
                                                    ))}
                                                </td>
                                                <td>{this.getTotal(subitem.items)}</td>
                                            </tr>
                                        )))}
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colSpan="4" className="text-right"><h5>Total Semua barang: <strong>{this.getTotalAll()} Barang</strong></h5></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <br/>

                            <div className="row">
                                <div className="col text-left">
                                    <a className="btn btn-danger" role="button" href="/pemindahan">Batal</a>&nbsp;&nbsp;
                                </div>
                                {this.state.verifikasi ?
                                    <div className="col text-right">
                                        <button className="btn btn-danger" onClick={()=>this.save(2)}>Tolak</button>&nbsp;&nbsp;
                                        <button className="btn btn-primary btn-utama" onClick={()=>this.save(1)}>Setujui</button>
                                    </div> : null
                                }
                                {this.state.view || this.state.verifikasi || this.state.cancel ? null :
                                    <div className="col text-right">
                                        <button className="btn btn-primary btn-utama" onClick={()=>this.save(0)}>Simpan</button>
                                    </div>
                                }
                                {this.state.cancel ?
                                    <div className="col text-right">
                                        <button className="btn btn-danger" onClick={()=>this.save(0)}>Batal Verifikasi</button>
                                    </div>
                                    : null
                                }
                            </div>
                        </div>
                    </div>
                </div>
                
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <h6 className="text-primary m-0 font-weight-bold">Daftar Produk</h6>
                        </div>
                        <div className="card-body">
                            <div id="accordion-data" className="accordion m-0">
                                <ul className="nav nav-tabs">
                                    <li className="nav-item"><a className={"nav-link active"} role="tab" data-toggle="tab" href="#tab-1">Outerbox</a></li>
                                    <li className="nav-item"><a className={"nav-link"} role="tab" data-toggle="tab" href="#tab-2">Innerbox</a></li>
                                </ul>
                                <div className="tab-content">
                                    <div role="tabpanel" id="tab-1" className={"tab-pane active"}>
                                        {this.state.view ? null :
                                            <div className="row my-3">
                                                <div className="col">
                                                    <div className="input-group">
                                                        <input onKeyDown={(f)=>this.onEnterPress(f)} value={this.state.outerbox} onChange={(e) => this.setState({outerbox:e.target.value})} className="form-control" type="text" placeholder="Cari Outerbox" name="cari-produk"/>
                                                        <div className="input-group-append"><button onClick={()=>this.loadOuterLabel()} className="btn btn-info" type="button">TAMBAH</button></div>
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                        <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                            <table className="table dataTable my-0" id="dataTable">
                                                <thead>
                                                    <tr style={{backgroundColor:"var(--red)"}}>
                                                        <th>Grade / Outerbox / Total Innerbox / PLU/SKU Name / Color / Profile</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                        {this.state.items.map((item) => (
                                        <div key={item.outerbox} className="card my-2">
                                            <div id={item.serial_no} className="card-header m-0 py-0">
                                                <div className="d-flex justify-content-between align-items-center align-items-xl-center">
                                                    <h1><button onClick={()=>this.loadTransferDetailItem(item.outerbox)} className="btn btn-link" type="button" data-toggle="collapse" data-target={"#collapse-" + item.serial_no} aria-expanded="true" aria-controls={"collapse-" + item.serial_no}><strong>{this.getOuterBoxTitle(item)}</strong></button></h1>
                                                    {this.state.view ? null :
                                                    <div>
                                                        <a className="text-danger" mina1="#" onClick={()=>this.delete(item.outerbox)} href="##"><i className="fa fa-trash-o"></i>&nbsp;Hapus</a>
                                                    </div>
                                                    }
                                                </div>
                                            </div>
                                            <div id={"collapse-" + item.serial_no} className="collapse hide" aria-labelledby={item.serial_no} data-parent="#accordion-data">
                                                <div className="card-body">
                                                    <div className="table-responsive">
                                                        <table className="table">
                                                            <thead>
                                                                <tr style={{backgroundColor:"var(--red)"}}>
                                                                    <th>Nama Produk</th>
                                                                    <th>Warna</th>
                                                                    <th>Ukuran</th>
                                                                    <th>Innerbox</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {(this.state.subitems.length === 0) ? (
                                                                    <tr>
                                                                        <td colSpan="4">Belum ada data</td>
                                                                    </tr>
                                                                ) : this.state.subitems.map((subitem) => (
                                                                    <tr key={subitem.innerbox}>
                                                                        <td>{subitem.name}</td>
                                                                        <td>{subitem.color}</td>
                                                                        <td>{subitem.size}</td>
                                                                        <td>{subitem.serial_no}</td>
                                                                    </tr>
                                                                ))}
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                    <hr/>
                                    <h5 className="mb-4">Total Barang dalam Daftar:<strong> {this.getTotalBarang()} Barang&nbsp;</strong>dalam <strong>{this.state.items.length} Outerbox.</strong></h5>
                            
                                    </div>
                                    <div role="tabpanel" id="tab-2" className={"tab-pane"}>
                                        {this.state.view ? null :
                                            <div className="row my-3">
                                                <div className="col">
                                                    <div className="input-group">
                                                        <input onKeyDown={(f)=>this.onEnterPress1(f)} value={this.state.innerbox} onChange={(e) => this.setState({innerbox:e.target.value})} className="form-control" type="text" placeholder="Cari Innerbox" name="cari-produk"/>
                                                        <div className="input-group-append"><button onClick={()=>this.loadInnerLabel()} className="btn btn-info" type="button">TAMBAH</button></div>
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                        <div className="table-responsive table mt-2">
                                            <table className="table">
                                                <thead>
                                                    <tr style={{backgroundColor:"var(--red)"}}>
                                                        <th>Nama Produk</th>
                                                        <th>Warna</th>
                                                        <th>Ukuran</th>
                                                        <th>Innerbox</th>
                                                        <th>Grade</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {(this.state.items_inner.map((subitem) => (
                                                        <tr key={subitem.id}>
                                                            <td>{subitem.name}</td>
                                                            <td>{subitem.color}</td>
                                                            <td>{subitem.size}</td>
                                                            <td>{subitem.serial_no}</td>
                                                            <td>{this.getGrade(subitem.id, subitem.grade)}</td>
                                                            <td>
                                                             {this.state.view ? null :<a className="text-danger" onClick={()=>this.deleteInner(subitem.id)} mina1="#" href="##"><i className="fa fa-trash-o"></i>&nbsp;Hapus</a>}
                                                            </td>
                                                        </tr>
                                                    )))}
                                                </tbody>
                                            </table>
                                        </div>
                                        <hr/>
                                        <h5 className="mb-4">Total Innerbox: <strong>{this.state.items_inner.length} Barang.</strong></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
                <Modal show={this.state.show} onHide={this.handleClose} animation={true} className="">
                    <Modal.Header closeButton>
                        <Modal.Title>Ubah Data Toko {this.state.isLoading1 ? ' (Loading ...)' : ''}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="form-group">
                            <div className="mb-2">
                                <label htmlFor="id-surat">No. Permintaan Order</label>
                                <input value={this.state.no_so1} onChange={(e)=>this.setState({no_so1:e.target.value})} type="text" className="form-control" name="id-surat" placeholder="Mis: SO-FG01/2006/00001"/>
                            </div>
                            <div className="mb-2">
                                <label htmlFor="id-surat">No. Surat</label>
                                <input value={this.state.no_surat1} onChange={(e)=>this.setState({no_surat1:e.target.value})} type="text" className="form-control" name="id-surat" placeholder="Mis: DO-FG01/2006/00001"/>
                            </div>
                            <div className="mb-2">
                                <label htmlFor="id-surat">Lokasi Tujuan</label>
                                <DropdownList
                                    data={this.state.locations}
                                    textField={item => item === undefined ? "Pilih" : item.nama_site + " - " + item.nama}
                                    filter
                                    onChange={value => this.setState({location:value})}
                                    value={this.state.location}
                                />
                            </div>
                            <div className="mb-2">
                                <label htmlFor="id-surat">Grade Barang</label>
                                <DropdownList
                                    data={this.state.grades}
                                    textField={item => item === undefined ? "Pilih" : item.nama}
                                    filter
                                    onChange={value => this.setState({grade:value})}
                                    value={this.state.grade}
                                />
                            </div>
                            <div className="mb-2">
                                <label htmlFor="alamat-surat">Keterangan Pemindahan</label>
                                <textarea type="text" className="form-control" name="qty-label" placeholder="Keterangan Pemindahan" value={this.state.keterangan1} onChange={(e)=> this.setState({keterangan1:e.target.value})}/>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.handleClose()} className="btn btn-light">Batal</button>
                        <button onClick={()=> this.saveSurat()} className="btn btn-primary btn-utama">Lanjutkan</button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}