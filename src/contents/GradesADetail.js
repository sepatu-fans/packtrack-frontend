// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BaseComponent from '../components/BaseComponent'
import axios from 'axios';
import queryString from 'query-string';
import Helper from '../Helper'

export default class GradesADetail extends Component {
    constructor(props) {
      super(props);
      
      this.params = queryString.parse(window.location.search)
    }

    render(){
        return (
        <Fragment>
            <Sidebar active="grades-a"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <GradesADetailSub params={this.params}/>
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class GradesADetailSub extends BaseComponent {
    pageTitle = 'Input Produk Grade A'
    
    state = {
        grade: {
            profil_karton:'',
            profil_karton_name:'',
            outerbox : 0, 
            serial_no : '',
            plu : [], 
            details : []
        },
        details: [],
        deletes : [], 
        total: 0,
        innerbox : ''
    }

    isDisable = false;

    constructor(props) {
        super(props);
        this.params = props.params;
        console.log(this.params);

        if(this.params.type === 1) {
            this.state.title = "Solid";
        }else {
            this.state.title = "Assort";
        }

        if(this.params.id !== undefined && this.params.id !== '0') {
            if(!Helper.getAccessStatus(['Pengisian Produk - Grade A'], "edit")) {
                window.location.href = "/notfound";
            }
            this.state.isEdit = true;
            this.state.id = this.params.id;

            fetch(process.env.REACT_APP_SERVER_URL + '/grades-a-by-id?id=' + this.params.id)
                .then(res => res.json())
                .then((res) => {
                    if(res.status) {
                        var plu = [];
                        JSON.parse(res.data.grade.plu).forEach((e) => {
                            res.details.forEach((e1) => {
                                if(e[0] === e1.id) {
                                    plu.push({'item':e1, 'jumlah' : e[1]});
                                }
                            });
                        });
                        var size = [];
                        if(res.data.grade.size !== null) {
                            JSON.parse(res.data.grade.size).forEach((e) => {
                                size.push({'item':e[0], 'jumlah' : e[1]});
                            });
                        }
                        var details = [];
                        res.data.details.forEach((e) => {
                            if(e.status === 1) {
                                details.push(e);
                            }
                        })
                        var grade = {
                            id : res.data.grade.id,
                            outerbox : res.data.grade.outerbox,
                            serial_no : res.data.grade.serial_no,
                            profil_karton : res.data.grade.profil_karton,
                            profil_karton_name : res.data.grade.profil_karton_name,
                            plu:plu,
                            size:size,
                            tipe:res.data.grade.tipe,
                            sku:res.data.grade.sku,
                            jumlah:res.data.grade.jumlah
                        }
                        this.setState({'grade':grade,'details' : details})
                    }else {
                        alert(res.error);
                        window.history.back();  
                    }
                })
            .catch(console.log)

        }else if(this.params.id === '0' && this.params.outerbox !== '' && this.params.profil_karton !== '') {
            if(!Helper.getAccessStatus(['Pengisian Produk - Grade A'], "add")) {
                window.location.href = "/notfound";
            }
            fetch(process.env.REACT_APP_SERVER_URL + '/profil-karton-by-id?id=' + this.params.profil_karton)
                .then(res => res.json())
                .then((res) => {
                    if(res.status) {
                        var plu = [];
                        JSON.parse(res.data.plu).forEach((e) => {
                            res.details.forEach((e1) => {
                                if(e[0] === e1.id) {
                                    plu.push({'item':e1, 'jumlah' : e[1]});
                                }
                            });
                        });
                        var size = [];
                        JSON.parse(res.data.size).forEach((e) => {
                            size.push({'item':e[0], 'jumlah' : e[1]});
                        });
                        var grade = {
                            id : this.params.id,
                            outerbox : this.params.outerbox,
                            serial_no : this.params.serial_no,
                            profil_karton : this.params.profil_karton,
                            profil_karton_name : res.data.nama,
                            tipe : res.data.tipe,
                            plu:plu,
                            size:size,
                            sku:res.data.sku,
                            jumlah:res.data.jumlah
                        }
                        this.setState({'grade':grade,'details' : []})
                    }else {
                        alert(res.error);
                        window.history.back();  
                    }
                })
            .catch(console.log)
        }else {
            alert("Data tidak ditemukan");
            window.history.back();
        }
    }

    componentDidMount() {
        super.componentDidMount()
    }

    delete = (serial) => {

        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            var newdetails = [];
            var deletes = this.state.deletes;
            this.state.details.forEach(elem => {
                if(elem.innerbox === serial) {
                    deletes.push(elem.innerbox);
                }else {
                    newdetails.push(elem);
                }
            });
            console.log(deletes);
            console.log(newdetails);
            this.setState({'details':newdetails,'deletes':deletes});
        }
    }
    
    loadInnerLabel() {
        if(this.isDisable) {
            return
        }
        this.isDisable = true;
        
        this.setState({isLoading1:true})
        if(this.state.innerbox === '') {
            alert('Serial innerbox harus diisi');
            this.isDisable = false;
            return;
        }
        var isSkip = false;
        this.state.details.forEach(elem => {
            if(elem.serial_no === this.state.innerbox) {
                alert("Data sudah pernah dimasukkan");
                this.setState({'innerbox':''});
                isSkip = true;
                return;
            }
        });
        if(isSkip) {
            this.isDisable = false;
            return;
        }
        fetch(process.env.REACT_APP_SERVER_URL + '/labels-inner-by-code?code=' + this.state.innerbox + '&profil_karton=' + this.state.grade.profil_karton)
            .then(res => res.json())
            .then((res) => {
                this.setState({isLoading1:false})
                if(res.status) {
                    var isSkip = false;
                    if(this.state.grade.tipe === 3) {
                        var itemid = res.data.id;
                        this.state.grade.plu.forEach(e => {
                            if(itemid === e.item.id) {
                                var total = 0;
                                this.state.details.forEach(e1 => {
                                    if(e1.id === itemid) {
                                        total++;
                                    }
                                });
                                if(total >= e.jumlah) {
                                    alert("Jumlah Innerbox Barcode " + e.item.barcode + " nama item " + e.item.name + " harus " + e.jumlah);
                                    isSkip = true;
                                }
                                return 0;
                            }
                        });
                    }else if(this.state.grade.tipe === 2) {
                        var size = res.data.size;
                        this.state.grade.size.forEach(e => {
                            if(size === e.item.toString()) {
                                var total = 0;
                                this.state.details.forEach(e1 => {
                                    if(e1.size === size) {
                                        total++;
                                    }
                                });
                                if(total >= e.jumlah) {
                                    alert("Jumlah Innerbox Size " + e.item + " harus " + e.jumlah);
                                    isSkip = true;
                                }
                                return 0;
                            }
                        });

                        if(this.state.details.length > 0) {
                            var sku = this.state.details[0].sku
                            var name = this.state.details[0].name
                            if(res.data.sku !== sku) {
                                alert("SKU tidak sama dengan data yang tersimpan. SKU harus " + sku + " nama item " + name);
                                isSkip = true;
                            }
                        }

                    }else {
                        if(this.state.details.length >= this.state.grade.jumlah) {
                            alert("Jumlah Innerbox harus " + this.state.grade.jumlah);
                            isSkip = true;
                        }

                        if(this.state.grade.tipe === 1 && this.state.details.length > 0) {
                            var barcode = this.state.details[0].barcode
                            var name1 = this.state.details[0].name
                            if(res.data.barcode !== barcode) {
                                alert("Barcode tidak sama dengan data yang tersimpan. Barcode harus " + barcode + " nama item " + name1);
                                isSkip = true;
                            }
                        }
                    }
                    if(isSkip) {
                        this.isDisable = false;
                        return;
                    }
                    var innerboxid = res.data.innerbox;
                    var data = res.data;
                    fetch(process.env.REACT_APP_SERVER_URL + '/grades-innerbox-check?innerbox=' + innerboxid + '&gradea=A-' + this.state.grade.id)
                        .then(res => res.json())
                        .then((res) => {
                            this.setState({isLoading1:false})
                            if(res.status) {
                                var details = this.state.details;
                                var deletes = this.state.deletes;
                                console.log(details.indexOf(data));
                                if(details.indexOf(data) < 0) {
                                    details.push(data);
                                }
                                if(deletes.indexOf(innerboxid) >= 0) {
                                    deletes.splice(deletes.indexOf(innerboxid), 1);
                                }
                                this.setState({'details':details,'deletes':deletes,'innerbox':''}, ()=>{
                                    this.save(0)
                                });
                            }else {
                                alert(res.error);
                                this.setState({'innerbox':''});
                            }
                            this.isDisable = false;
                        })
                    .catch(console.log)
                }else {
                    alert(res.error);
                    this.setState({'innerbox':''});
                    this.isDisable = false;
                }
            })
        .catch(console.log)
    }

    save = (is_close) => {
        var isSkip = false;
        if(this.state.grade.tipe === 3) {
            this.state.grade.plu.map(e => {
                var total = 0;
                this.state.details.forEach(e1 => {
                    if(e1.id === e.item.id) {
                        total++;
                    }
                });
                if(total !== e.jumlah) {
                    if(is_close) {
                        alert("Jumlah Innerbox Barcode " + e.item.barcode + " nama item " + e.item.name  + " harus " + e.jumlah);
                    }
                    isSkip = true;
                }
                return 0;
            });
        }else if(this.state.grade.tipe === 2) {
            this.state.grade.size.map(e => {
                var total = 0;
                this.state.details.forEach(e1 => {
                    if(e1.size === e.item.toString()) {
                        total++;
                    }
                });
                if(total !== e.jumlah) {
                    if(is_close) {
                        alert("Jumlah Innerbox Size " + e.item + " harus " + e.jumlah);
                    }
                    isSkip = true;
                }
                return 0;
            });
        }else {
            if(this.state.details.length !== this.state.grade.jumlah) {
                if(is_close) {
                    alert("Jumlah Innerbox harus " + this.state.grade.jumlah);
                }
                isSkip = true;
            }
        }
        if(isSkip) {
            return;
        }
        if(true) {
            var details = [];
            this.state.details.forEach((e) => {
                if(this.state.grade.id === '0') {
                    details.push(e.innerbox);
                } else if(e.shipping_status === undefined) {
                    details.push(e.innerbox);
                }
            });
            var data = {
                grade_a:this.state.grade.id,
                outerbox:this.state.grade.outerbox,
                profil_karton:this.state.grade.profil_karton,
                details:details,
                deletes:this.state.deletes,
                created_at:Helper.getCurrentTime(null, 7),
                created_by:localStorage.getItem("USERID"),
                lokasi:localStorage.getItem("LOCATION_ID"),
                is_close:1
            }
            axios.post(process.env.REACT_APP_SERVER_URL+'/grades-a-detail-save', data)
                .then(response => response.data)
                .then((res) => {
                        // console.log(res);
                        if(res.status) {
                            // alert("Data berhasil disimpan");
                            if(this.state.isEdit) {
                                window.location.href='/grades-a'
                            }else {
                                window.location.href='/grades-a?isadd=1'
                            }
                        }else {
                            alert(res.error);
                        }
                    }
                );
        }
    }

    getDetailsbyItem(id) {
        var result = [];
        var details = this.state.details;
        details.forEach(e => {
            if(e.id === id) {
                result.push(e);
            }
        })
        return result;
    }

    getDetailsbySize(size) {
        var result = [];
        this.state.details.forEach(e => {
            if(e.size === size.toString()) {
                result.push(e);
            }
        })
        return result;
    }

    onEnterPress = (f) => {
        if(f.keyCode === 13 && f.shiftKey === false) {
            f.preventDefault();
            this.loadInnerLabel()
        }
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/grades-a"><span className="bc-link">Produk Grade A</span></a></li>
                            <li className="breadcrumb-item"><a href="/grades-a"><span className="bc-link">Input Produk Grade A</span></a></li>
                        </ol>
                    </div>
                </div>
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <h6 className="text-primary m-0 font-weight-bold">Produk</h6>
                        </div>
                        <div className="card-body">
                            <div className="row my-3">
                                <div className="col-xl-4">
                                    <h4 className="mb-4"><strong>Profil Packing</strong><br/></h4>
                                    <div className="form-group">
                                        <div className="mb-3"><label htmlFor="profil-sepatu">Profil Sepatu</label><input type="text" className="form-control" readOnly value={this.state.grade.profil_karton_name} name="profil-sepatu"/></div>
                                        <div className="mb-3"><label htmlFor="kode-karton">Serial Outerbox</label><input type="text" className="form-control" name="kode-karton" value={this.state.grade.serial_no} readOnly disabled=""/></div>
                                        <div className="mb-3">
                                            <label htmlFor="kode-produk">Serial Innerbox</label>
                                            <input autoFocus={true} onKeyDown={(f)=>this.onEnterPress(f)} type="text" className="form-control" name="kode-produk" placeholder="Masukkan Serial Innerbox" value={this.state.innerbox} onChange={(e)=>this.setState({'innerbox':e.target.value})}/>
                                        </div>
                                        <div className="text-right mt-2">
                                            <button onClick={(e)=>{this.loadInnerLabel();}} className="btn btn-primary btn-utama" type="button">Tambah</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="col">
                                    <h4 className="mb-2"><strong>Data Inner Box</strong></h4>
                                    {this.state.grade.tipe === 3 ?
                                        (this.state.grade.plu.map((e) => (
                                            <div key={e.item.id}>
                                                <p>{this.getDetailsbyItem(e.item.id).length} dari {e.jumlah} Innerbox Barcode <b>{e.item.barcode}</b> Nama Item <b>{e.item.name}</b> dimasukkan</p>
                                                <div className="table-responsive">
                                                    <table className="table">
                                                        <thead>
                                                            <tr>
                                                                <th>Nama Produk</th>
                                                                <th>Warna</th>
                                                                <th>Ukuran</th>
                                                                <th>Innerbox</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {(this.getDetailsbyItem(e.item.id).length === 0) ? 
                                                                (
                                                                    <tr>
                                                                        <td colSpan="5">Belum ada data</td>
                                                                    </tr>
                                                                ) : 
                                                                (this.getDetailsbyItem(e.item.id).map((item) => (
                                                                    <tr key={item.innerbox}>
                                                                        <td>{item.name}</td>
                                                                        <td>{item.color}</td>
                                                                        <td>{item.size}</td>
                                                                        <td>{item.serial_no}</td>
                                                                        <td><a onClick={()=>this.delete(item.innerbox)} className="shadow-none text-danger" href="##"><i className="fa fa-trash-o"></i>&nbsp;Hapus</a></td>
                                                                    </tr>
                                                                )))}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        )))
                                        :
                                        null
                                    }
                                    {this.state.grade.tipe === 2 ?
                                        <div> 
                                            {/* <b>SKU : {this.state.grade.sku}</b>  */}
                                            {(this.state.grade.size.map((e) => (
                                            <div key={e.item.id}>
                                                <p>{this.getDetailsbySize(e.item).length} dari {e.jumlah} Innerbox Size <b>{e.item}</b> dimasukkan</p>
                                                <div className="table-responsive">
                                                    <table className="table">
                                                        <thead>
                                                            <tr>
                                                                <th>Nama Produk</th>
                                                                <th>SKU</th>
                                                                <th>Warna</th>
                                                                <th>Ukuran</th>
                                                                <th>Innerbox</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {(this.getDetailsbySize(e.item).length === 0) ? 
                                                                (
                                                                    <tr>
                                                                        <td colSpan="6">Belum ada data</td>
                                                                    </tr>
                                                                ) : 
                                                                (this.getDetailsbySize(e.item).map((item) => (
                                                                    <tr key={item.innerbox}>
                                                                        <td>{item.name}</td>
                                                                        <td>{item.sku}</td>
                                                                        <td>{item.color}</td>
                                                                        <td>{item.size}</td>
                                                                        <td>{item.serial_no}</td>
                                                                        <td><a onClick={()=>this.delete(item.innerbox)} className="shadow-none text-danger" href="##"><i className="fa fa-trash-o"></i>&nbsp;Hapus</a></td>
                                                                    </tr>
                                                                )))}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        )))}
                                        </div>
                                        :
                                        null
                                    }
                                    {this.state.grade.tipe === 1 || this.state.grade.tipe === 4 ?
                                        <div>
                                            <p>{this.state.details.length} dari {this.state.grade.jumlah} Innerbox dimasukkan</p>
                                            <div className="table-responsive">
                                                <table className="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Nama Produk</th>
                                                            <th>Barcode</th>
                                                            <th>Warna</th>
                                                            <th>Ukuran</th>
                                                            <th>Innerbox</th>
                                                            <th>Aksi</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {(this.state.details.length === 0) ? 
                                                            (
                                                                <tr>
                                                                    <td colSpan="6">Belum ada data</td>
                                                                </tr>
                                                            ) : 
                                                            (this.state.details.map((item) => (
                                                                <tr key={item.innerbox}>
                                                                    <td>{item.name}</td>
                                                                    <td>{item.barcode}</td>
                                                                    <td>{item.color}</td>
                                                                    <td>{item.size}</td>
                                                                    <td>{item.serial_no}</td>
                                                                    <td><a onClick={()=>this.delete(item.innerbox)} className="shadow-none text-danger" href="##"><i className="fa fa-trash-o"></i>&nbsp;Hapus</a></td>
                                                                </tr>
                                                            )))}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        :
                                        null
                                    }
                                </div>
                            </div>  
                            <div className="row my-4">
                                <div className="col text-right d-flex justify-content-between">
                                    <a className="btn btn-warning" role="button" href="/grades-a">Batal</a>
                                    <div>
                                        <button className="btn btn-primary btn-utama" onClick={()=>this.save(1)}>Simpan</button>&nbsp;&nbsp;
                                        {/* <button className="btn btn-primary btn-utama" onClick={()=>this.save(1)}>Simpan dan Tutup</button> */}
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}