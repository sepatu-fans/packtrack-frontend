import React, {Component, Fragment} from 'react';
import Helper from '../../Helper'

export default class IBXTrack extends Component {
    state = {
        items : [],
        size : 10,
        pages : [1],
        page : 1,
        isLoading : false,
        search : '',
        selecteds : [],
        show : false,
        nama : "",
        id : 0,
        manufaktures : [],
        sites : [],
    }

    role_name = "Laporan - IBXTrack"

    constructor(props) {
        super(props);
        this.id = props.id;
        this.active = props.active;
    }

    exportData() {
        window.open(process.env.REACT_APP_SERVER_URL + '/reports-ibx-download')
    }

    handleClose = () => this.setState({
        nama: "",
        show:false
    });
    handleShow = () => {
        this.setState({
            id:0,
            show:true
        });
    }
    
    sizes = [
        10,20,50,100
    ]

    componentDidMount() {
        this.loadData();
    }

    loadData() {
        this.setState({isLoading:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/reports-ibx?page=' + this.state.page + '&size=' + this.state.size + '&search=' + this.state.search)
            .then(res => res.json())
            .then((data) => {
            this.setState({ items: data.data, pages: data.pages })
            this.setState({isLoading:false})
        })
        .catch(console.log)
    }

    handleChange = (event) => {
        console.log(event.target.value);
        this.setState({ size: event.target.value }, function() {
            this.loadData();
        });
    };

    searchChange = (event) => {
        console.log(event.target.value);
        this.setState({search:event.target.value,page:1}, function() {
            this.loadData();
        });
    }

    pageChange = (event) => {
        console.log(event.target.getAttribute('data'));
        this.setState({ page: event.target.getAttribute('data') }, function() {
            this.loadData();
        });
    };

    render(){
        return (
        <Fragment>
            <div className={"tab-pane " + this.active} role="tabpanel" id={this.id}>
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <div className="row">
                                <div className="col my-auto">
                                    <h6 className="text-primary m-0 font-weight-bold">Laporan - IBXTrack</h6>
                                </div>
                                <div className="col text-right">
                                </div>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="row my-3">
                                <div className="col">
                                    <select className="custom-select max-view mb-3" defaultValue={this.state.size} onChange={this.handleChange} >
                                        {this.sizes.map((item) => (
                                            <option key={item} value={item}>{item} baris</option>
                                        ))}
                                    </select>
                                </div>
                                <div className="col">
                                    <div className="input-group">
                                        <input onChange={this.searchChange} className="form-control" type="text" placeholder="Cari Produk" name="cari-produk"/>
                                        <div className="input-group-append"><button onClick={this.handleSearch} className="btn btn-info" type="button"><i className="fa fa-search"></i></button></div>
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                <table className="table dataTable my-0" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Serial No</th>
                                            <th>Barcode</th>
                                            <th>SKU Name</th>
                                            <th>Color</th>
                                            <th>Size</th>
                                            <th>CM</th>
                                            <th>UK</th>
                                            <th>US</th>
                                            <th>Outerbox</th>
                                            <th>Outerbox Status</th>
                                            <th>Grade</th>
                                            <th>Site</th>
                                            <th>Location</th>
                                            <th>Last Trans</th>
                                            <th>Trans Date</th>
                                            <th>Verified Date</th>
                                            <th>Reference</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.isLoading ? (
                                            <tr>
                                                <td colSpan="12">
                                                    Loading
                                                </td>
                                            </tr>
                                        ) : 
                                        this.state.items.map((item) => (
                                            <tr key={item.serial_no}>
                                                <td>{item.serial_no}</td>
                                                <td>{item.barcode}</td>
                                                <td style={{minWidth:"200px"}}>{item.sku_name}</td>
                                                <td>{item.color}</td>
                                                <td>{item.size}</td>
                                                <td>{item.cm}</td>
                                                <td>{item.uk}</td>
                                                <td>{item.us}</td>
                                                <td>{item.outerbox}</td>
                                                <td>{item.outerbox_status}</td>
                                                <td>{item.grade}</td>
                                                <td>{item.site}</td>
                                                <td>{item.location}</td>
                                                <td>{item.last_trans}</td>
                                                <td>{item.last_trans_date}</td>
                                                <td></td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                            <div className="row">
                                <div className="col-md-6 align-self-center">
                                    <div></div>
                                    <div className="mb-4">
                                        {Helper.getAccessStatus(this.role_name, "export") ? <button onClick={()=>this.exportData()} className="btn btn-danger mr-2">Ekspor Data</button> : null}
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <nav className="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers" style={{'float':'right'}}>
                                        <ul className="pagination">
                                            <li className={"page-item " + (this.state.pages[0] === 1 ? "disabled" : "")}><button data={this.state.pages[0]-1} className="page-link" aria-label="Previous" onClick={this.pageChange}>«</button></li>
                                            {this.state.pages.map((page) => (
                                                <li key={page} className={"page-item " + ((parseInt(this.state.page) === page ? "active" : ""))}><button data={page} className="page-link" onClick={this.pageChange}>{page}</button></li>
                                            ))}
                                            <li className="page-item"><button data={this.state.pages[this.state.pages.length-1]+1} className="page-link" aria-label="Next" onClick={this.pageChange}>»</button></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
        )
    }
}