// Header.js
import React, {Fragment} from 'react';
import BaseComponent from '../components/BaseComponent'
import axios from 'axios';
import md5 from 'md5'

export default class Login extends BaseComponent {
    pageTitle = 'Login'
    componentDidMount() {
        super.componentDidMount()
        document.body.classList.add('body-login');
    }
    
    componentWillUnmount() {
        document.body.classList.remove('body-login');
    }

    state = {
        username : "",
        name : "",
        password : ""
    }

    login() {
        if(!this.state.username) {
            alert('Nama User harus diisi');
            return;
        }
        if(!this.state.password) {
            alert('Password harus diisi');
            return;
        }

        var data = {
            username:this.state.username,
            password:md5(this.state.password)
        }
        axios.post(process.env.REACT_APP_SERVER_URL+'/user-login', data)
            .then(response => response.data)
            .then((res) => {
                console.log(res);
                if(res.status) { 
                    localStorage.setItem("IS-LOGIN","1");
                    localStorage.setItem("USERID",res.data.id);
                    localStorage.setItem("USERNAME",res.data.username);
                    localStorage.setItem("ROLE",res.data.role);
                    localStorage.setItem("ACCESS",res.data.access);
                    localStorage.setItem("LOCATION",res.data.location);

                    if(res.data.access !== "" && res.data.access !== null) {
                        JSON.parse(res.data.access).forEach((e) => {
                            localStorage.setItem(e[0] + '-' + e[1], e[2] ? "1":"0");
                        })
                    }

                    console.log(res);

                    if(res.locations) {
                        localStorage.setItem("LOCATIONS", JSON.stringify(res.locations));
                        var defaultLocation = res.defaultLocation;
                        localStorage.setItem("LOCATION_NAME",defaultLocation.nama_site + " - " + defaultLocation.nama);
                        localStorage.setItem("LOCATION_ID",defaultLocation.id);

                        window.location.href="/";
                    }
                }else {
                    alert(res.error);
                }
            }
        );
    }

    render(){
        return (
        <Fragment>
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-9 col-lg-12 col-xl-10">
                        <div className="card shadow-lg o-hidden border-0 my-5" style={{backgroundImage: "url('linear-gradient(180deg,#4e73df 10%,#224abe))'"}}>
                            <div className="card-body p-0">
                                <div className="row">
                                    <div className="col-lg-6 d-none d-lg-flex p-0 m-0">
                                        <div className="flex-grow-1 bg-login-image" style={{backgroundImage: "url('/img/login-illustrations.png')",backgroundRepeat: 'no-repeat',backgroundPosition: "bottom",backgroundSize: "cover"}}></div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="p-5" style={{backgroundColor: "#ffffff;color: #4e73df !important"}}>
                                            <div className="text-center"><img alt="" className="m-3 mb-5 w-75" src="/img/logo.svg"/></div>
                                            <div className="user">
                                                <div className="form-group">
                                                    <input value={this.state.username} onChange={(e) => this.setState({username:e.target.value})} className="border rounded-0 form-control form-control-user" type="text" name="username" placeholder="Username" required="" pattern="^[a-zA-Z0-9_.-]*$" minLength="3" maxLength="20"/>
                                                </div>
                                                <div className="form-group">
                                                    <input value={this.state.password} onChange={(e) => this.setState({password:e.target.value})} className="border rounded-0 form-control form-control-user" type="password" id="exampleInputPassword" placeholder="Sandi" name="password"/>
                                                </div>
                                                <button onClick={()=>this.login()} className="btn btn-primary btn-block text-white border rounded-0 btn-user btn-utama" style={{color: '#ffffff'}} >Masuk</button>
                                                <hr/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
        )
    }
}