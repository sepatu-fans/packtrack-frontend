import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import queryString from 'query-string';
import axios from 'axios';
import DropdownList from 'react-widgets/lib/DropdownList'
import Helper from '../Helper'

export default class ProfilKartonAdd extends Component {
    constructor(props) {
      super(props);
      
      this.params = queryString.parse(window.location.search)
    }
    render(){
        return (
        <Fragment>
            <Sidebar active="profil-karton"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <ProfilKartonAddSub params={this.params}/>
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class ProfilKartonAddSub extends Component {
    state = {
        isEdit : false,
        title : '',
        nama: '',
        id: '',
        isLoading: true,
        items : [],
        sku : '',
        jumlah : 0,
        details : []
    }

    constructor(props) {
        super(props);
        this.params = props.params;

        if(this.params.type === '1') {
            this.state.title = "Solid";;
        }else if(this.params.type === '2') {
            this.state.title = "Assort";
            this.state.details = [[undefined,'']];
        }else if(this.params.type === '3') {
            this.state.title = "Spesific";
            this.state.details = [[undefined,'']];
        }else if(this.params.type === '4') {
            this.state.title = "Mix";
        }

        if(this.params.id !== undefined) {
            if(!Helper.getAccessStatus("Profil Karton", "edit")) {
                window.location.href = "/notfound";
            }
            this.state.isEdit = true;
            this.state.id = this.params.id;

            fetch(process.env.REACT_APP_SERVER_URL + '/profil-karton-by-id?id=' + this.params.id)
                .then(res => res.json())
                .then((res) => {
                    if(res.status) {
                        this.setState({
                            'nama':res.data.nama,
                            'plu':res.data.plu,
                            'size':res.data.size,
                            'jumlah':res.data.jumlah,
                            'sku':res.data.sku,
                        })

                        if(res.data.tipe === 1) {
                            this.setState({'title':'Solid'});
                        }else if(res.data.tipe === 2) {
                            this.setState({'title':'Assort'});
                        }else if(res.data.tipe === 3) {
                            this.setState({'title':'Spesific'});
                        }else if(res.data.tipe === 4) {
                            this.setState({'title':'Mix'});
                        }

                        this.loadItems();
                    }else {
                        alert(res.error);
                        window.history.back();  
                    }
                })
            .catch(console.log)
        }else {
            if(!Helper.getAccessStatus("Profil Karton", "add")) {
                window.location.href = "/notfound";
            }
            this.loadItems();
        }
    }

    componentDidMount() {
    }

    save() {
        if(this.state.nama === '') {
            alert('Nama harus diisi');
            return;
        }

        // if(this.state.title === 'Assort' && this.state.sku === '') {
        //     alert('SKU harus diisi');
        //     return;
        // }

        if(this.state.title === 'Mix' && (this.state.jumlah === '')) {
            alert('Jumlah harus diisi');
            return;
        }

        var plu = [];
        var size = [];
        console.log(this.state.details);
        this.state.details.forEach((e,i) => {
            if(e[0] === undefined) {
                if(this.state.title === 'Assort') {
                    alert('Ukuran ' + (i+1) + ' harus diisi');
                }else {
                    alert('Barcode ke ' + (i+1) + ' harus diisi');
                }
                return 0;
            }
            if(e[1] === undefined || e[1] === "" || e[1] === null) {
                alert('Jumlah ke ' + (i+1) + ' harus diisi');
                return 0;
            }
            
            var isSkip = false;
            if(this.state.title === 'Solid' || this.state.title === 'Spesific') {
                isSkip = false;
                plu.forEach((e1) => {
                    if(e1[0] === e[0].id) {
                        isSkip = true;
                        e1[1] = parseInt(e1[1]) + parseInt(e[1]);
                    }
                })
                if(!isSkip) {
                    plu.push([e[0].id, parseInt(e[1])]);
                }
            }else if(this.state.title === 'Assort') {
                isSkip = false;
                size.forEach((e1) => {
                    if(e1[0] === parseInt(e[0])) {
                        isSkip = true;
                        e1[1] = parseInt(e1[1]) + parseInt(e[1]);
                    }
                })
                if(!isSkip) {
                    size.push([parseInt(e[0]), parseInt(e[1])]);
                }
            }
        })

        if(plu.length === 0 && size.length === 0 && this.state.jumlah === 0) {
            return;
        }

        var data = {
            tipe:this.params.type,
            plu:JSON.stringify(plu),
            size:JSON.stringify(size),
            nama:this.state.nama,
            jumlah:this.state.jumlah,
            sku:this.state.sku,
            created_at:Helper.getCurrentTime(null, 7),
            created_by:localStorage.getItem("USERID"),
        }
        if(this.state.id !== '') {
            data.id = this.state.id;
        }
        axios.post(process.env.REACT_APP_SERVER_URL+'/profil-karton-add', data)
            .then(response => response.data)
            .then((res) => {
                    // console.log(res);
                    if(res.status) {
                        // alert("Data berhasil disimpan");  
                        window.location.href = '/profil-karton';
                    }else {
                        alert(res.error);
                    }
            }
        );
    }

    loadItems() {
        this.setState({isLoading:true});
        fetch(process.env.REACT_APP_SERVER_URL + '/items-list')
            .then(res => res.json())
            .then((res) => {
            this.setState({ items: res.data })
            this.setState({isLoading:false})
            if(this.state.plu === '' && res.data.length > 0) {
            }else {
                var plu = JSON.parse(this.state.plu);
                var plus = [];
                plu.forEach((e) => {
                    plus.push(e[0]);
                })
                console.log(plus);
                var details = [];
                res.data.forEach((elem) => {
                    if(plus.indexOf(elem.id) >= 0) {
                        details.push([elem, plu[plus.indexOf(elem.id)][1]]);
                    }
                });

                var size = JSON.parse(this.state.size);
                if(size !== '') {
                    size.forEach((e) => {
                        details.push([e[0],e[1]]);
                    })
                }

                if(this.state.id === undefined) {
                    if(details.length === 0) {
                        details.push([undefined,'']);
                    }
                }
                this.setState({ details: details});
            }
        })
        .catch(console.log)
    }

    changeDetails(index, mode, val) {
        var details = this.state.details;
        if(mode === 0) {
            details[index][0] = val;
        }else {
            details[index][1] = val;
        }
        this.setState({details:details});
    }

    addDetails() {
        var details = this.state.details;
        details.push([undefined, 1]);
        this.setState({details:details});
    }

    deleteDetails(index) {
        var details = this.state.details;
        if(details.length > 1) {
            details.splice(index, 1);
        }
        this.setState({details:details});
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/profil-karton"><span className="bc-link">Profil Karton</span></a></li>
                            <li className="breadcrumb-item"><a href="/profil-karton-add"><span className="bc-link">{this.state.isEdit ? 'Ubah' : 'Tambah'} Profil Karton</span></a></li>
                        </ol>
                    </div>
                    <div className="col text-right"></div>
                </div>
                <div>
                    <div className="row">
                        <div className="col-lg-12 col-xl-12">
                            <div className="card shadow mb-4">
                                <div className="card-header py-3">
                                    <h6 className="text-primary m-0 font-weight-bold">Profil Karton {this.state.title} {this.state.isEdit ? 'Ubah' : 'Baru'}</h6>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <div className="mb-3">
                                            <div className="row">
                                                <div className="col-6">
                                                    <label htmlFor="nama-sepatu">Nama</label>
                                                    <input type="text" className="form-control" name="nama-sepatu" placeholder="Tentukan Nama Profil Karton" value={this.state.nama} onChange={(e)=> this.setState({nama:e.target.value})}/>
                                                </div>
                                                {/* {this.state.title === 'Assort' ?
                                                    <div className="col-6">
                                                        <label htmlFor="nama-sepatu">SKU</label>
                                                        <input type="text" className="form-control" name="nama-sepatu" placeholder="Tentukan SKU Produk" value={this.state.sku} onChange={(e)=> this.setState({sku:e.target.value})}/>
                                                    </div>
                                                    :
                                                    null
                                                } */}
                                                {this.state.title === 'Solid' || this.state.title === 'Mix' ?
                                                    <div className="col-6">
                                                        <label htmlFor="nama-sepatu">Jumlah</label>
                                                        <input type="number" min="1" className="form-control" placeholder="Minimal 1" name="kode-plu" value={this.state.jumlah} onChange={(e)=> this.setState({jumlah:e.target.value})}/>
                                                    </div>
                                                    :
                                                    null
                                                }
                                            </div>
                                        </div>
                                        {this.state.details.map((detail, index) => (
                                            <div className="mb-3" key={index}>
                                                <div className="row">
                                                    {this.state.title === 'Spesific' ?
                                                        <div className="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                            <label htmlFor="sku-produk">Barcode {(index === 0) ? "" : <a onClick={()=>this.deleteDetails(index)} href="##"><i className="fa fa-trash" style={{color:"red"}}></i></a>}</label>
                                                            <DropdownList 
                                                                data={this.state.items}
                                                                textField={item => item === undefined ? "Pilih Barcode" : item.barcode + " - " + item.name}
                                                                filter="contains"
                                                                busy={this.state.isLoading}
                                                                onChange={value => this.changeDetails(index, 0, value)}
                                                                value={detail[0]}
                                                            />
                                                        </div>
                                                        :
                                                        null
                                                    }
                                                    {this.state.title === 'Assort' ?
                                                        <div className="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                            <label htmlFor="sku-produk">Ukuran {(index === 0) ? "" : <a onClick={()=>this.deleteDetails(index)} href="##"><i className="fa fa-trash" style={{color:"red"}}></i></a>}</label>
                                                            <input type="number" min="1" className="form-control" placeholder="Ukuran" name="ukuran" value={(detail[0] === null ? '' : detail[0])} onChange={(e)=> this.changeDetails(index, 0, e.target.value)}/>
                                                        </div>
                                                        :
                                                        null
                                                    }
                                                    <div className="col">
                                                        <label htmlFor="kode-plu">Jumlah</label>
                                                        <input type="number" min="1" className="form-control" placeholder="Minimal 1" name="kode-plu" value={(detail[1] === null ? '' : detail[1])} onChange={(e)=> this.changeDetails(index, 1, e.target.value)}/>
                                                    </div>
                                                    <hr/>
                                                </div>
                                            </div>
                                        ))}
                                        {this.state.title === 'Assort' ? <span><a onClick={()=>this.addDetails()} href="##">Tambah Ukuran</a></span> : ''}
                                        {this.state.title === 'Spesific' ? <span><a onClick={()=>this.addDetails()} href="##">Tambah Produk</a></span> : ''}
                                    </div>
                                    <div className="row my-4">
                                        <div className="col">
                                            <a className="btn btn-warning" role="button" href="/profil-karton">Batal</a>
                                        </div>
                                        <div className="col text-right">
                                            <button className="btn btn-primary btn-utama" onClick={() => this.save() } >Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}