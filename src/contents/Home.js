// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Helper from '../Helper';

export default class Home extends Component {
    render(){
        return (
        <Fragment>
            <Sidebar active='home'/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <HomeSub />
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class HomeSub extends Component {
    state = {
        shippings : [],
        shipping : 0,
        item : 0
    }

    componentDidMount() {
        this.loadData();
    }
    
    loadData() {
        fetch(process.env.REACT_APP_SERVER_URL + '/shippings?page=1&size=5&search=&lokasi=' + localStorage.getItem("LOCATION_ID"))
            .then(res => res.json())
            .then((data) => {
                this.setState({ shippings: data.data })
            })
        .catch(console.log)

        fetch(process.env.REACT_APP_SERVER_URL + '/dashboard/shipping?lokasi=' + localStorage.getItem("LOCATION_ID"))
            .then(res => res.json())
            .then((data) => {
                this.setState({ shipping: data.data })
            })
        .catch(console.log)

        fetch(process.env.REACT_APP_SERVER_URL + '/dashboard/item?lokasi=' + localStorage.getItem("LOCATION_ID"))
            .then(res => res.json())
            .then((data) => {
                this.setState({ item: data.data })
            })
        .catch(console.log)
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="d-sm-flex justify-content-between align-items-center mb-4">
                    <h3 className="text-dark mb-0">Dashboard</h3>
                    <a href="/laporan" className="btn btn-primary btn-sm d-none d-sm-inline-block" role="button" id="btn-create-report">
                        <i className="fas fa-download fa-sm text-white-50"></i>&nbsp;Laporan
                    </a>
                </div>
                <div className="row">
                    <div className="col-md-6 col-xl-3 mb-4">
                        <a href="/pengiriman">
                            <div className="card shadow border-left-primary py-2">
                                <div className="card-body">
                                    <div className="row align-items-center no-gutters">
                                        <div className="col mr-2">
                                            <div className="text-uppercase text-primary font-weight-bold text-xs mb-1"><span>Permintaan kirim</span></div>
                                            <div className="text-dark font-weight-bold h5 mb-0"><span>{this.state.shipping}</span></div>
                                        </div>
                                        <div className="col-auto"><i className="fas fa-truck fa-2x text-gray-300"></i></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div className="col-md-6 col-xl-3 mb-4">
                        <a href="/opname">
                            <div className="card shadow border-left-info py-2">
                                <div className="card-body">
                                    <div className="row align-items-center no-gutters">
                                        <div className="col mr-2">
                                            <div className="text-uppercase text-info font-weight-bold text-xs mb-1"><span>Barang OPNAME</span></div>
                                            <div className="row no-gutters align-items-center">
                                                <div className="col-auto">
                                                    <div className="text-dark font-weight-bold h5 mb-0 mr-3"><span>{0}</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-auto"><i className="fas fa-solar-panel fa-2x text-gray-300"></i></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div className="col-md-6 col-xl-3 mb-4">
                        <a href="/items">
                            <div className="card shadow border-left-warning py-2">
                                <div className="card-body">
                                    <div className="row align-items-center no-gutters">
                                        <div className="col mr-2">
                                            <div className="text-uppercase text-warning font-weight-bold text-xs mb-1"><span>DaFtar &nbsp;produk</span></div>
                                            <div className="text-dark font-weight-bold h5 mb-0"><span>{this.state.item.toLocaleString()}</span></div>
                                        </div>
                                        <div className="col-auto"><i className="fas fa-comments fa-2x text-gray-300"></i></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12 col-xl-7">
                        <div className="card shadow mb-4">
                            <div className="card-header d-flex justify-content-between align-items-center">
                                <h6 className="text-primary font-weight-bold m-0">Daftar Permintaan Kirim</h6>
                                <div className="dropdown no-arrow"><button className="btn btn-link btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button"><i className="fas fa-ellipsis-v text-gray-400"></i></button>
                                    <div className="dropdown-menu shadow dropdown-menu-right animated--fade-in" role="menu">
                                        <p className="text-center dropdown-header">dropdown header:</p><a href="/" className="dropdown-item" role="presentation">&nbsp;Action</a><a href="/" className="dropdown-item" role="presentation">&nbsp;Another action</a>
                                        <div className="dropdown-divider"></div><a href="/" className="dropdown-item" role="presentation">&nbsp;Something else here</a></div>
                                </div>
                            </div>
                            <div className="card-body">
                                <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                    <table className="table dataTable my-0" id="dataTable">
                                        <thead>
                                            <tr>
                                                <th>Tanggal</th>
                                                <th>ID Surat Jalan</th>
                                                <th>Satatus</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                            this.state.shippings.length === 0 ?
                                                <tr>
                                                    <td colSpan="4">
                                                        Belum ada data
                                                    </td>
                                                </tr>
                                                :
                                            this.state.shippings.map((item) => (
                                                <tr key={item}>
                                                    <td>{Helper.getCurrentTime(item.created_at)}</td>
                                                    <td>{item.no_surat}</td>
                                                    {item.status === 0 ? <td className="text-secondary">Belum Diverifikasi</td> : null}
                                                    {item.status === 1 ? <td className="text-success">Sudah Diverifikasi</td> : null}
                                                    {item.status === 2 ? <td className="text-danger">Ditolak</td> : null}
                                                    <td>{item.status !== 0 && Helper.getAccessStatus("Pengiriman Produk", "view") ? <a href={"/pengiriman-detail?view=1&id=" + item.id} className="m-2 link-green">Lihat</a> : null}</td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                                <div className="row">
                                    <div className="col-md-6 align-self-center">
                                        <div></div>
                                    </div>
                                    <div className="col-md-6 text-right"><a className="btn btn-primary btn-utama" role="button" href="/pengiriman">Lihat Semua</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-5">
                        <div className="card shadow mb-4">
                            <div className="card-header d-flex justify-content-between align-items-center">
                                <h6 className="text-primary font-weight-bold m-0">Pintasan</h6>
                            </div>
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-lg-6 col-xl-6 mb-4">
                                        <a href="/pengiriman" id="btn-id-surat">
                                            <div className="card text-white bg-primary shadow">
                                                <div className="card-body">
                                                    <p className="m-0">Permintaan Pengiriman</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div className="col-lg-6 col-xl-6 mb-4">
                                        <a href="/grades-a" id="btn-grade-a-requirement">
                                            <div className="card text-white shadow bg-success">
                                                <div className="card-body">
                                                    <p className="m-0">&nbsp;Tambah Produk Grade A</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div className="col-lg-6 col-xl-6 mb-4">
                                        <a href="/grades-other">
                                            <div className="card text-white shadow bg-warning">
                                                <div className="card-body">
                                                    <p className="m-0">Tambah Produk Non-Grade A</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" role="dialog" tabIndex="-1" id="download-dialog">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Pilih Data</h4><button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                            <div className="modal-body">
                                <div className="form-group">
                                    <div className="my-2"><label><strong>Pilih Rentang</strong></label>
                                        <div className="row">
                                            <div className="col"><label>Dari Tanggal</label><input className="form-control" type="date"/></div>
                                            <div className="col"><label>Sampai</label><input className="form-control" type="date"/></div>
                                        </div>
                                    </div>
                                    <div className="mt-4"><label><strong>Pilih Data</strong></label>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-check"><input className="form-check-input" type="checkbox" id="formCheck-1"/><label className="form-check-label" htmlFor="formCheck-1">Produk</label></div>
                                            </div>
                                            <div className="col">
                                                <div className="form-check"><input className="form-check-input" type="checkbox" id="formCheck-1"/><label className="form-check-label" htmlFor="formCheck-1">Output</label></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer"><button className="btn btn-light" type="button" data-dismiss="modal">Batal</button><a className="btn btn-primary btn-utama" role="button" href="index.html">Unduh</a></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}