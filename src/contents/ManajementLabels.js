// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BaseComponent from '../components/BaseComponent'
import axios from 'axios';
import Modal from 'react-bootstrap/Modal';
import Helper from '../Helper'

export default class ManajementLabels extends Component {

    render(){
        return (
        <Fragment>
            <Sidebar active="manajement-labels"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <ManajementLabelsSub />
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class ManajementLabelsSub extends BaseComponent {

    pageTitle = "Manajemen Label"
    
    state = {
        items: [],
        size : 10,
        page : 1,
        pages : [1],
        isLoading : false,
        isLoading1 : false,
        serialNo : "",
        logs : [],
        search : '',
        selecteds : [],
        items1: [],
        size1 : 10,
        page1 : 1,
        pages1 : [1],
        search1 : '',
        selecteds1 : [],
        show : false,
        show1 : false,
        show2 : false,
        isOuter : false,
        jumlah : 1,
        cetakStart : 1,
        cetakEnd : 1
    }
    
    handleClose = () => this.setState({show:false});
    handleShow = () => this.setState({show:true});
    handleClose1 = () => this.setState({show1:false});
    handleShow1 = () => this.setState({show1:true});
    handleClose2 = () => this.setState({show2:false});
    handleShow2 = (serial_no, id) => {
        this.setState({serialNo: serial_no, show2:true});
        this.loadLogs(id);
    };
    handleShow2Outer = (serial_no, id) => {
        this.setState({serialNo: serial_no, show2:true});
        this.loadOuterLogs(id);
    };

    sizes = [
        10,20,50,100
    ]

    innerActive() {
        this.loadData();
    }

    outerActive() {
        this.loadDataOuter();
    }

    componentDidMount() {
        super.componentDidMount()
        if(!Helper.getAccessStatus(['Manajemen Label Inner','Manajemen Label Outer'], "view")) {
            window.location.href = "/notfound";
        }
        this.loadData();
    }

    loadData() {
        this.setState({isLoading:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/labels-inner?page=' + this.state.page + '&size=' + this.state.size + '&search=' + this.state.search)
        .then(res => res.json())
        .then((data) => {
          this.setState({ items: data.data, pages: data.pages })
          this.setState({isLoading:false})
        })
        .catch(console.log)
    }

    loadLogs(id) {
        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/label-logs?id=' + id)
        .then(res => res.json())
        .then((data) => {
          this.setState({ logs: data.data })
          this.setState({isLoading1:false})
        })
        .catch(console.log)
    }

    loadOuterLogs(id) {
        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/label-outer-logs?id=' + id)
        .then(res => res.json())
        .then((data) => {
          this.setState({ logs: data.data })
          this.setState({isLoading1:false})
        })
        .catch(console.log)
    }

    handleChange = (event) => {
        console.log(event.target.value);
        this.setState({ size: event.target.value }, function() {
            this.loadData();
        });
    };

    searchChange = (event) => {
        console.log(event.target.value);
        this.setState({search:event.target.value,page:1}, function() {
            this.loadData();
        });
    }

    pageChange = (event) => {
        console.log(event.target.getAttribute('data'));
        this.setState({ page: event.target.getAttribute('data') }, function() {
            this.loadData();
        });
    };

    labelAdd = (id, status) => {
        var selecteds = this.state.selecteds;
        if(!status && selecteds.indexOf(id) !== -1) {
            const index = selecteds.indexOf(id);
            selecteds.splice(index,1)
        }else if(status) {
            selecteds.push(id)
        }

        this.setState({selecteds:selecteds})
        console.log(selecteds);
    }

    print = (param) => {
        var urlPrint = process.env.REACT_APP_SERVER_URL + '/print-labels-inner-by-ids?id=' + param;
        fetch(urlPrint)
            .then(res => res.json())
            .then((data) => {
                if(data.status) {
                    window.open(process.env.REACT_APP_SERVER_URL +"/barcode/label-inner-by-ids?id=" + param)
                }else {
                    alert(data.error)
                }
            })
            .catch(() => {
                this.setState({isLoading:false})
            })
    }

    printOuter = (param) => {
        var urlPrint = process.env.REACT_APP_SERVER_URL + '/print-labels-outer-by-ids?id=' + param;
        fetch(urlPrint)
            .then(res => res.json())
            .then((data) => {
                if(data.status) {
                    window.open(process.env.REACT_APP_SERVER_URL +"/barcode/label-outer-by-ids?id=" + param)
                }else {
                    alert(data.error)
                }
            })
            .catch(() => {
                this.setState({isLoading:false})
            })
    }

    printSelecteds = ()=> {
        if(this.state.selecteds.length === 0) {
            alert("Belum ada data yang dipilih");
        }else{
            var param = "";
            this.state.selecteds.forEach(elem => {
                param += "id=" + elem + "&";
            });
            var urlPrint = process.env.REACT_APP_SERVER_URL + '/print-labels-inner-by-ids?' + param;
            fetch(urlPrint)
                .then(res => res.json())
                .then((data) => {
                    if(data.status) {
                        window.open(process.env.REACT_APP_SERVER_URL +"/barcode/label-inner-by-ids?" + param)
                    }else {
                        alert(data.error)
                    }
                })
                .catch(() => {
                    this.setState({isLoading:false})
                })
        }
    }

    cetakKhusus = ()=> {
        if(this.state.cetakStart === "" || this.state.cetakEnd === "") {
            alert("Data harus diisi");
            return;
        }
        var urlPrint = process.env.REACT_APP_SERVER_URL + '/print-labels-inner-by-serialno-min-max?idmin=' + this.state.cetakStart + '&idmax=' + this.state.cetakEnd;
        if(this.state.isOuter) {
            urlPrint = process.env.REACT_APP_SERVER_URL + '/print-labels-outer-by-serialno-min-max?idmin=' + this.state.cetakStart + '&idmax=' + this.state.cetakEnd;
        }

        fetch(urlPrint)
            .then(res => res.json())
            .then((data) => {
                if(data.status) {
                    if(this.state.isOuter) {
                        window.open(process.env.REACT_APP_SERVER_URL + '/barcode/label-outer-by-serialno-min-max?idmin=' + this.state.cetakStart + '&idmax=' + this.state.cetakEnd)
                    }else {
                        window.open(process.env.REACT_APP_SERVER_URL + '/barcode/label-inner-by-serialno-min-max?idmin=' + this.state.cetakStart + '&idmax=' + this.state.cetakEnd)
                    }
                }else {
                    alert(data.error)
                }
            })
            .catch(() => {
                this.setState({isLoading:false})
            })
    }

    exportCSV = ()=> {
        if(this.state.cetakStart === "" || this.state.cetakEnd === "") {
            alert("Data harus diisi");
            return;
        }
        if(this.state.isOuter) {
            window.open(process.env.REACT_APP_SERVER_URL + '/labels-outer-by-serialno-min-max?csv=1&idmin=' + this.state.cetakStart + '&idmax=' + this.state.cetakEnd)
        }else {
            window.open(process.env.REACT_APP_SERVER_URL + '/labels-inner-by-serialno-min-max?csv=1&idmin=' + this.state.cetakStart + '&idmax=' + this.state.cetakEnd)
        }
    }

    delete = (id) => {
        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            var data = {
                id:id
            }
            axios.post(process.env.REACT_APP_SERVER_URL+'/labels-inner-delete', data)
                .then(response => response.data)
                .then((res) => {
                        // console.log(res);
                        if(res.status) {
                            // alert("Data berhasil dihapus");  
                            this.loadData();
                        }else {
                            alert(res.error);
                        }
                    }
                );
        }
    }

    // LABEL OUTER
    loadDataOuter() {
        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/labels-outer?page=' + this.state.page1 + '&size=' + this.state.size1 + '&search=' + this.state.search1)
            .then(res => res.json())
            .then((data) => {
                this.setState({ items1: data.data, pages1: data.pages })
                this.setState({isLoading1:false})
            })
            .catch(console.log)
    }

    generate(redirect) {
        var data = {
            jumlah:this.state.jumlah,
            created_at:Helper.getCurrentTime(null, 7),
            created_by:localStorage.getItem("USERID"),
        }
        axios.post(process.env.REACT_APP_SERVER_URL+'/labels-outer-add', data)
            .then(response => response.data)
            .then((res) => {
                    // console.log(res);
                    if(res.status) {
                        // alert("Data berhasil disimpan");  
                        this.handleClose();
                        if(!redirect) {
                            // window.location.href = '/manajement-labels';
                            this.loadDataOuter()
                        }else {
                            var idmin = res.data;
                            var idmax = parseInt(res.data)+parseInt(this.state.jumlah)-1;
                            var urlPrint = process.env.REACT_APP_SERVER_URL + '/print-labels-outer-by-id-min-max?idmin=' + idmin + '&idmax=' + idmax;
                            fetch(urlPrint)
                                .then(res => res.json())
                                .then((data) => {
                                    if(data.status) {
                                        window.open(process.env.REACT_APP_SERVER_URL +'/barcode/label-outer-by-id-min-max?idmin=' + idmin + '&idmax=' + idmax)
                                    }else {
                                        alert(data.error)
                                    }
                                })
                                .catch(() => {
                                    this.setState({isLoading:false})
                                })
                        }   
                    }else {
                        alert(res.error);
                    }
                }
            );
    }

    handleChangeOuter = (event) => {
        console.log(event.target.value);
        this.setState({ size1: event.target.value }, function() {
            this.loadDataOuter();
        });
    };

    pageChangeOuter = (event) => {
        console.log(event.target.getAttribute('data'));
        this.setState({ page1: event.target.getAttribute('data') }, function() {
            this.loadDataOuter();
        });
    };

    searchChangeOuter = (event) => {
        console.log(event.target.value);
        this.setState({search1:event.target.value,page:1}, function() {
            this.loadDataOuter();
        });
    }

    labelAddOuter = (id, status) => {
        var selecteds = this.state.selecteds1;
        if(!status && selecteds.indexOf(id) !== -1) {
            const index = selecteds.indexOf(id);
            selecteds.splice(index,1)
        }else if(status) {
            selecteds.push(id)
        }

        this.setState({selecteds1:selecteds})
        console.log(selecteds);
    }

    printSelectedsOuter = ()=> {
        if(this.state.selecteds1.length === 0) {
            alert("Belum ada data yang dipilih");
        }else{
            var param = "";
            this.state.selecteds1.forEach(elem => {
                param += "id=" + elem + "&";
            });
            var urlPrint = process.env.REACT_APP_SERVER_URL + '/print-labels-outer-by-ids?' + param;
            fetch(urlPrint)
                .then(res => res.json())
                .then((data) => {
                    if(data.status) {
                        window.open(process.env.REACT_APP_SERVER_URL + '/barcode/label-outer-by-ids?' + param)
                    }else {
                        alert(data.error)
                    }
                })
                .catch(() => {
                    this.setState({isLoading:false})
                })
        }
    }

    deleteOuter = (id) => {
        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            var data = {
                id:id
            }
            axios.post(process.env.REACT_APP_SERVER_URL+'/labels-outer-delete', data)
                .then(response => response.data)
                .then((res) => {
                        // console.log(res);
                        if(res.status) {
                            // alert("Data berhasil dihapus");  
                            this.loadDataOuter();
                        }else {
                            alert(res.error);
                        }
                    }
                );
        }
    }

    getMaxMin() {
        this.setState({isLoading:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/labels-inner-max-min')
            .then(res => res.json())
            .then((data) => {
                this.setState({isLoading:false})
                this.setState({cetakStart:data.min.serial_no,cetakEnd:data.max.serial_no,show1:true,isOuter:false})
            })
            .catch(console.log)
    }

    getMaxMinOuter() {
        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/labels-outer-max-min')
            .then(res => res.json())
            .then((data) => {
                this.setState({isLoading1:false})
                this.setState({cetakStart:data.min.serial_no,cetakEnd:data.max.serial_no,show1:true,isOuter:true})
            })
            .catch(console.log)
    }

    getOuterBoxTitle = (item) => {
        var title = ""
        if(item.receiving_status === 1) {
            title += "Receiving : " + item.no_so
        }else if(item.shipping_status === 1) {
            title += "Shipping : " + item.no_so
        }else {
            var label = "Packing";
            if(item.is_close === 0) {
                label = "Open Box";
            }else if(item.is_close === 1 && item.open_at !== "") {
                label = "Close Box";
            }

            if(item.tipe === 2) {
                title = label + " - " + item.grade;
            }else {
                if(item.profil_karton_type === 1 || item.profil_karton_type === 2) {
                    title = label + " -  Grade A - " + item.info + "/" + item.color + "/" + item.profil_karton;
                }else {
                    title = label + " -  Grade A - " + item.profil_karton;
                }
            }
        }
        return title;
    }


    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/manajement-labels"><span className="bc-link">Manajemen Label</span></a></li>
                        </ol>
                    </div>
                </div>
                <div>
                    <ul className="nav nav-tabs">
                        <li style={{display:Helper.getAccessStatus('Manajemen Label Inner', 'view') ? 'inline' : 'none'}} className="nav-item"><a onClick={()=>this.innerActive()} className={"nav-link " + (Helper.getAccessStatus('Manajemen Label Inner', 'view') ? 'active' : '')}role="tab" data-toggle="tab" href="#tab-1">Innerbox</a></li>
                        <li style={{display:Helper.getAccessStatus('Manajemen Label Outer', 'view') ? 'inline' : 'none'}} className="nav-item"><a onClick={()=>this.outerActive()} className="nav-link" role="tab" data-toggle="tab" href="#tab-2">Outerbox</a></li>
                    </ul>
                    <div className="tab-content">
                        <div className={"tab-pane " + (Helper.getAccessStatus('Manajemen Label Inner', 'view') ? 'active' : '')} role="tabpanel" id="tab-1">
                            <div>
                                <div className="card shadow mb-4">
                                    <div className="card-header py-3">
                                        <div className="row">
                                            <div className="col my-auto">
                                                <h6 className="text-primary m-0 font-weight-bold">Daftar Label</h6>
                                            </div>
                                            <div className="col text-right">
                                                {Helper.getAccessStatus('Manajemen Label Inner', 'add') ? <a className="btn btn-primary btn-utama" id="btn-grade-a-requirement" href="/manajement-labels-add">Tambah&nbsp;</a> : null}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        <div className="row my-3">
                                            <div className="col-lg-7">
                                                <select className="custom-select max-view mb-3" defaultValue={this.state.size} onChange={this.handleChange} >
                                                    {this.sizes.map((item) => (
                                                        <option key={item} value={item}>{item} baris</option>
                                                    ))}
                                                </select>
                                                {/* <select
                                                    className="custom-select max-view mb-3">
                                                    <option value="12" selected="">Semua</option>
                                                    <option value="13">ID Inner Box</option>
                                                    <option value="14">ID karton</option>
                                                    </select> */}
                                            </div>
                                            <div className="col">
                                                <div className="input-group">
                                                    <input onChange={this.searchChange} className="form-control" type="text" placeholder="Cari Produk" name="cari-produk"/>
                                                    <div className="input-group-append"><button className="btn btn-info" type="button"><i className="fa fa-search"></i></button></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                            <table className="table dataTable my-0" id="dataTable">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Tanggal Dibuat</th>
                                                        <th>No. Serial</th>
                                                        <th>Item Code</th>
                                                        <th>Nama Sepatu</th>
                                                        <th>Warna</th>
                                                        <th>Ukuran (EU/US/UK/CM)</th>
                                                        <th>Barcode</th>
                                                        <th>Cetak</th>
                                                        <th>Cetak ke-</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.isLoading ? (
                                                        <tr>
                                                            <td colSpan="9">
                                                                Loading
                                                            </td>
                                                        </tr>
                                                    ) : 
                                                    this.state.items.map((item) => (
                                                        <tr key={item.id}>
                                                            <td><input type="checkbox" onChange={(e)=>this.labelAdd(item.id, e.target.checked)}/></td>
                                                            <td>{Helper.getCurrentTime(item.created_at)}</td>
                                                            <td>{item.serial_no}</td>
                                                            <td>{item.code}</td>
                                                            <td>{item.name}</td>
                                                            <td>{item.color}</td>
                                                            <td>{item.size} / {item.us} / {item.uk} / {item.cm}<br/></td>
                                                            <td>{item.barcode}</td>
                                                            <td>
                                                                <div></div>
                                                                <div></div>
                                                                {Helper.getAccessStatus('Manajemen Label Inner', 'print') ? <a className="d-block view-barcode" href="##" onClick={()=>this.print(item.id)}><i className="fa fa-print"></i>&nbsp;Cetak</a> : null}
                                                            </td>
                                                            <td>{item.cetak_ke}</td>
                                                            <td>
                                                                <a onClick={()=>this.handleShow2(item.serial_no, item.id)} style={{"cursor":"pointer"}} href='##'><span className="link" dangerouslySetInnerHTML={{__html:item.last_status}}></span></a>
                                                            </td>
                                                        </tr>
                                                    ))}
                                                </tbody>
                                            </table>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6 align-self-center">
                                                <div>
                                                    {Helper.getAccessStatus('Manajemen Label Inner', 'print') ? <button onClick={()=>this.getMaxMin()} className="btn btn-success btn-cetak-khusus"><i className="fa fa-print"></i>&nbsp;Cetak Khusus</button> : null}&nbsp;&nbsp;
                                                    {Helper.getAccessStatus('Manajemen Label Inner', 'print') ? <button onClick={()=>this.printSelecteds()} className="btn btn-danger" href="print-label.html"><i className="fa fa-print"></i>&nbsp;Cetak yang Terpilih</button> : null}
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <nav className="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers" style={{'float':'right'}}>
                                                    <ul className="pagination">
                                                        <li className={"page-item " + (this.state.pages[0] === 1 ? "disabled" : "")}><button data={this.state.pages[0]-1} className="page-link" aria-label="Previous" onClick={this.pageChange}>«</button></li>
                                                        {this.state.pages.map((page) => (
                                                            <li key={page} className={"page-item " + ((parseInt(this.state.page) === page ? "active" : ""))}><button data={page} className="page-link" onClick={this.pageChange}>{page}</button></li>
                                                        ))}
                                                        <li className="page-item"><button data={this.state.pages[this.state.pages.length-1]+1} className="page-link" aria-label="Next" onClick={this.pageChange}>»</button></li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={"tab-pane " + (!Helper.getAccessStatus('Manajemen Label Inner', 'view') && Helper.getAccessStatus('Manajemen Label Outer', 'view') ? 'active' : '')} role="tabpanel" id="tab-2">
                            <div>
                                <div className="card shadow mb-4">
                                    <div className="card-header py-3">
                                        <div className="row">
                                            <div className="col my-auto">
                                                <h6 className="text-primary m-0 font-weight-bold">Daftar Label</h6>
                                            </div>
                                            <div className="col text-right">
                                                {Helper.getAccessStatus('Manajemen Label Outer', 'add') ? <button className="btn btn-primary btn-utama btn-barcode-outer" onClick={()=>this.setState({show:true})}>Tambah&nbsp;</button> : null}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        <div className="row my-3">
                                            <div className="col-lg-7">
                                                <select className="custom-select max-view mb-3" defaultValue={this.state.size} onChange={this.handleChangeOuter} >
                                                    {this.sizes.map((item) => (
                                                        <option key={item} value={item}>{item} baris</option>
                                                    ))}
                                                </select>
                                                {/* <select
                                                    className="custom-select max-view mb-3">
                                                    <option value="12" selected="">Semua</option>
                                                    <option value="13">ID Inner Box</option>
                                                    <option value="14">ID karton</option>
                                                    </select> */}
                                            </div>
                                            <div className="col">
                                                <div className="input-group">
                                                    <input onChange={this.searchChangeOuter} className="form-control" type="text" placeholder="Cari Produk" name="cari-produk"/>
                                                    <div className="input-group-append"><button className="btn btn-info" type="button"><i className="fa fa-search"></i></button></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                            <table className="table dataTable my-0" id="dataTable">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Tanggal Dibuat</th>
                                                        <th>No. Serial</th>
                                                        <th>Unduh/Cetak</th>
                                                        <th>Cetakan ke-</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.isLoading1 ? (
                                                        <tr>
                                                            <td colSpan="8">
                                                                Loading
                                                            </td>
                                                        </tr>
                                                    ) : 
                                                    this.state.items1.map((item) => (
                                                        <tr key={item.id}>
                                                            <td><input type="checkbox" onChange={(e)=>this.labelAddOuter(item.id, e.target.checked)}/></td>
                                                            <td>{Helper.getCurrentTime(item.created_at)}</td>
                                                            <td>{item.serial_no}</td>
                                                            <td>
                                                                <div></div>
                                                                <div></div>
                                                                {Helper.getAccessStatus('Manajemen Label Outer', 'print') ? <a className="d-block view-barcode" href="##" onClick={()=>this.printOuter(item.id)}><i className="fa fa-print"></i>&nbsp;Cetak</a> : null}
                                                            </td>
                                                            <td>{item.cetak_ke}</td>
                                                            <td>
                                                                <a onClick={()=>this.handleShow2Outer(item.serial_no, item.id)} style={{"cursor":"pointer"}} href='##'><span className="link" dangerouslySetInnerHTML={{__html:item.last_status}}></span></a>
                                                            </td>
                                                        </tr>
                                                    ))}
                                                </tbody>
                                            </table>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6 align-self-center">
                                                <div>
                                                    {Helper.getAccessStatus('Manajemen Label Outer', 'print') ? <button onClick={()=>this.getMaxMinOuter()} className="btn btn-success btn-cetak-khusus"><i className="fa fa-print"></i>&nbsp;Cetak Khusus</button> : null}&nbsp;&nbsp;
                                                    {Helper.getAccessStatus('Manajemen Label Outer', 'print') ? <button onClick={()=>this.printSelectedsOuter()} className="btn btn-danger" href="print-label.html"><i className="fa fa-print"></i>&nbsp;Cetak yang Terpilih</button> : null}
                                                </div>                                            
                                            </div>
                                            <div className="col-md-6">
                                                <nav className="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers" style={{'float':'right'}}>
                                                    <ul className="pagination">
                                                        <li className={"page-item " + (this.state.pages1[0] === 1 ? "disabled" : "")}><button data={this.state.pages1[0]-1} className="page-link" aria-label="Previous" onClick={this.pageChangeOuter}>«</button></li>
                                                        {this.state.pages1.map((page) => (
                                                            <li key={page} className={"page-item " + ((parseInt(this.state.page1) === page ? "active" : ""))}><button data={page} className="page-link" onClick={this.pageChangeOuter}>{page}</button></li>
                                                        ))}
                                                        <li className="page-item"><button data={this.state.pages1[this.state.pages1.length-1]+1} className="page-link" aria-label="Next" onClick={this.pageChangeOuter}>»</button></li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <Modal show={this.state.show} onHide={this.handleClose} animation={true}>
                    <Modal.Header closeButton>
                    <Modal.Title>Buat Label Outterbox</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="form-group">
                            <label htmlFor="qty-label">Jumlah Label</label>
                            <input type="number" className="form-control" name="qty-label" placeholder="Minimal 1 Label" value={this.state.jumlah} min="1" onChange={(e)=> this.setState({jumlah:e.target.value})}/>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.generate(true)} className="btn btn-danger">Cetak Label</button>
                        <button onClick={()=> this.generate(false)} className="btn btn-primary btn-utama ml-3">Generate</button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.show1} onHide={this.handleClose1} animation={true}>
                    <Modal.Header closeButton>
                    <Modal.Title>Cetak Khusus</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="form-group"><label htmlFor="qty-label">Pilih Jangka:</label>
                            <div className="row">
                                <div className="col"><label>Dari No. Serial</label><input type="text" className="form-control" name="qty-label" min="1" placeholder="No. Serial" value={this.state.cetakStart} onChange={(e)=> this.setState({cetakStart:e.target.value})}/></div>
                                <div className="col"><label>Sampai No. Serial</label><input type="text" className="form-control" name="qty-label" placeholder="No. Serial" min="1" value={this.state.cetakEnd} onChange={(e)=> this.setState({cetakEnd:e.target.value})}/></div>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.exportCSV()} className="btn btn-primary">Export CSV</button>
                        <button onClick={()=> this.cetakKhusus()} className="btn btn-danger">Cetak Label</button>
                    </Modal.Footer>
                </Modal>

                <Modal size="lg" show={this.state.show2} onHide={this.handleClose2} animation={true}>
                    <Modal.Header closeButton>
                    <Modal.Title>Detail Tracking</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Riwayat : <b>{this.state.serialNo}</b><br/><br/>
                        <div className="form-group">
                            <table className="table">
                                <tbody>
                                {this.state.isloading1 ? 
                                <tr key={null}>
                                    <td>Loading ...</td>
                                </tr>
                                :
                                this.state.logs.map((log) => (
                                <tr key={log.created_at}>
                                    <td>{Helper.getCurrentTime(log.created_at)}</td>
                                    <td>{log.lokasi_info}</td>
                                    <td>{log.type} {log.info === '' ? '' : '-'} {log.info}</td>
                                    <td>{log.username}</td>
                                </tr>
                                ))}
                                </tbody>
                            </table>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.handleClose2()} className="btn btn-danger">Tutup</button>
                        {/* <button onClick={()=> this.generate(false)} className="btn btn-primary btn-utama ml-3">Generate</button> */}
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}