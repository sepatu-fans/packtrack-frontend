// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BaseComponent from '../components/BaseComponent'
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';
import Helper from '../Helper'
import DropdownList from 'react-widgets/lib/DropdownList'

export default class Transfer extends Component {

    render(){
        return (
        <Fragment>
            <Sidebar active="transfer"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <TransferSub />
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class TransferSub extends BaseComponent {

    pageTitle = "Pemindahan Langsung"
    
    state = {
        profil_kartons: [],
        items: [],
        size : 10,
        page : 1,
        pages : [1],
        isLoading : false,
        isLoading1 : false,
        search : '',
        show : false,
        tipe : 1,
        no_so: '',
        no_surat: '',
        keterangan: '',
        status: '',
        filter: 1,
        date1 : '',
        date2 : '',
        locations: [],
        location: false,
        grades: [],
        grade: false,
    }

    role_name = "Pemindahan Langsung"

    handleClose = () => this.setState({show:false});
    handleShow = () => {
        this.loadLocations();
        this.loadGrades();
        this.setState({show:true});
    };
    handleClose1 = () => {
        this.setState({show1:false,is_filter:false,filter:1,status:''}, function() {
            this.loadData();
        })
    };
    handleShow1 = () => {
        this.setState({show1:true});
    };
    handleFilterChange = () => {
        this.setState({ is_filter: true, show1 : false }, function() {
            this.loadData();
        });
    };

    sizes = [
        10,20,50,100
    ]

    componentDidMount() {
        super.componentDidMount()
        if(!Helper.getAccessStatus(this.role_name, "view")) {
            window.location.href = "/notfound";
        }
        this.loadData();
    }

    loadData() {
        this.setState({isLoading:true})
        var url = process.env.REACT_APP_SERVER_URL + '/transfers?page=' + this.state.page + '&size=' + this.state.size + '&search=' + this.state.search + '&lokasi=' + localStorage.getItem("LOCATION_ID");
        if(this.state.is_filter) {
            if(this.state.filter === 2) {
                url = url + "&date1=" + this.state.date1;
                url = url + "&date2=" + this.state.date2;
            }
            if(this.state.status === 'Belum Diverifikasi') {
                url = url + "&status=0"
            }else if(this.state.status === 'Sudah Diverifikasi') {
                url = url + "&status=1"
            }else if(this.state.status === 'Ditolak') {
                url = url + "&status=2"
            }
        }
        fetch(url)
            .then(res => res.json())
            .then((data) => {
            this.setState({ items: data.data, pages: data.pages })
            this.setState({isLoading:false})
        })
        .catch(console.log)
    }

    handleChange = (event) => {
        console.log(event.target.value);
        this.setState({ size: event.target.value }, function() {
            this.loadData();
        });
    };

    searchChange = (event) => {
        console.log(event.target.value);
        this.setState({search:event.target.value,page:1}, function() {
            this.loadData();
        });
    }

    pageChange = (event) => {
        console.log(event.target.getAttribute('data'));
        this.setState({ page: event.target.getAttribute('data') }, function() {
            this.loadData();
        });
    }

    loadShippingType(loadAll) {
        fetch(process.env.REACT_APP_SERVER_URL + '/shipping-type-list')
            .then(res => res.json())
            .then((res) => {
                let data = res.data;
                if(loadAll) {
                    data.unshift({id:0,nama:"Semua Data"});
                }
                let shipping_type = this.state.shipping_type;
                if(!shipping_type) {
                    shipping_type = res.data[0];
                }
                this.setState({ shipping_types: data, shipping_type: shipping_type })
                this.setState({isLoading1:false})
            })
        .catch(console.log)
    }

    loadLocations() {
        fetch(process.env.REACT_APP_SERVER_URL + '/location')
            .then(res => res.json())
            .then((res) => {
                let location = this.state.locaion;
                if(!location) {
                    location = res.data[0];
                }
                this.setState({locations: res.data, location: location })
            })
        .catch(console.log)
    }

    loadGrades() {
        fetch(process.env.REACT_APP_SERVER_URL + '/grades')
            .then(res => res.json())
            .then((res) => {
                let grade = this.state.grade;
                var grades = res.data;
                grades.unshift({id:0,nama:"Grade A"})
                grades.unshift({id:-1,nama:"Tidak Ada Perubahan"})
                if(!grade) {
                    grade = grades[0];
                }
                this.setState({grades: grades, grade: grade })
            })
        .catch(console.log)
    }

    save = () => {
        // if(this.state.no_surat === '') {
        //     alert('No Surat harus diisi');
        //     return;
        // }
        if(this.state.no_so === '') {
            alert('No. Permintaan Order harus diisi');
            return;
        }
        if(!this.state.location) {
            alert('Lokasi Tujuan harus diisi');
            return;
        }
        if(!this.state.grade) {
            alert('Grade harus diisi');
            return;
        }
        window.location.href = '/pemindahan-detail?id=0&no_so=' + this.state.no_so + '&no_surat=' + this.state.no_surat + '&tujuan=' + this.state.location.id + '&tujuan_nama=' + this.state.location.nama_site + ' - ' + this.state.location.nama + '&grade=' + this.state.grade.id + '&grade_nama=' + this.state.grade.nama + '&keterangan=' + this.state.keterangan;
    }

    delete = (id) => {
        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            var data = {
                id:id
            }
            axios.post(process.env.REACT_APP_SERVER_URL+'/transfer-delete', data)
                .then(response => response.data)
                .then((res) => {
                        // console.log(res);
                        if(res.status) {
                            // alert("Data berhasil dihapus");  
                            this.loadData();
                        }else {
                            alert(res.error);
                        }
                    }
                );
        }
    }

    getFormattedDate1() {
        var curr = new Date();
        var date = curr.toISOString().substr(0,10);
        if(this.state.date1 !== '') {
            return this.state.date1.slice(0, 10);
        }else {
            // this.setState({date1:date})
        }
        return date;
    }

    getFormattedDate2() {
        var curr = new Date();
        var date = curr.toISOString().substr(0,10);
        if(this.state.date2 !== '') {
            return this.state.date2.slice(0, 10);
        }else {
            // this.setState({date2:date})
        }
        return date;
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/pemindahan"><span className="bc-link">Pemindahan Produk</span></a></li>
                        </ol>
                    </div>
                    <div style={{display:Helper.getAccessStatus(this.role_name, "add") ? "inline" : "none"}} className="col text-right"><button onClick={()=>{this.handleShow()}} className="btn btn-primary btn-utama" id="btn-grade-a-requirement">Tambah</button></div>
                </div>
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <table className="table dataTable my-0" id="dataTable">
                                <tbody>
                                    <tr>
                                        <td style={{border:"0px"}}><h6 className="text-primary m-0 font-weight-bold">Daftar Permintaan Pemindahan</h6></td>
                                        <td style={{border:"0px"}}>
                                            <div className="text-right"><a onClick={()=>this.handleShow1()} id="btn-filter-data" href="##"><i className="fa fa-filter"></i>&nbsp;<strong><span style={{textDecoration: "underline"}}>Filter Khusus</span></strong></a></div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="card-body">
                            <div className="row my-3">
                                <div className="col">
                                    <select className="custom-select max-view mb-3" defaultValue={this.state.size} onChange={this.handleChange} >
                                        {this.sizes.map((item) => (
                                            <option key={item} value={item}>{item} baris</option>
                                        ))}
                                    </select>&nbsp;
                                    {/* <select className="custom-select max-view mb-3">
                                        <option value="12" selected="">Semua</option>
                                        <option value="13">Belum Diverifikasi</option>
                                        <option value="14">Sudah Diverifikasi</option>
                                    </select> */}
                                </div>
                                <div className="col">
                                    <div className="input-group">
                                        <input onChange={this.searchChange} className="form-control" type="text" placeholder="Cari Produk" name="cari-produk"/>
                                        <div className="input-group-append"><button onClick={this.handleSearch} className="btn btn-info" type="button"><i className="fa fa-search"></i></button></div>
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                <table className="table dataTable my-0" id="dataTable">
                                    <thead>
                                        <tr style={{backgroundColor:"var(--red)"}}>
                                            <th>Tanggal</th>
                                            <th>No. Order</th>
                                            <th>No. Surat</th>
                                            <th>Tujuan</th>
                                            <th>Grade</th>
                                            <th>Total Barang</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.isLoading ? (
                                            <tr>
                                                <td colSpan="8">
                                                    Loading
                                                </td>
                                            </tr>
                                        ) : 
                                        this.state.items.map((item) => (
                                            <tr key={item.id}>
                                                <td width="250">{Helper.getCurrentTime(item.created_at)}</td>
                                                <td width="250">{item.no_so}</td>
                                                <td width="250">{item.no_surat}</td>
                                                <td>{item.tujuan_nama}</td>
                                                <td>{item.grade_nama}</td>
                                                <td width="150" align="center">{item.total}</td>
                                                {item.status === 0 ? <td className="text-secondary">Belum Diverifikasi</td> : null}
                                                {item.status === 1 ? <td className="text-success">Sudah Diverifikasi</td> : null}
                                                {item.status === 2 ? <td className="text-danger">Ditolak</td> : null}
                                                {item.status === 3 ? <td className="text-danger">Batal Diverifikasi</td> : null}
                                                <td>
                                                    {item.status === 0 && Helper.getAccessStatus(this.role_name, "edit") ? <a href={"/pemindahan-detail?id=" + item.id} className="m-2 link-green">Edit</a> : null}
                                                    {item.status === 0 && Helper.getAccessStatus(this.role_name, "delete") ? <a onClick={()=>this.delete(item.id)} href="##" className="m-2 link-red">Hapus</a> : null}
                                                    {item.status === 0 && Helper.getAccessStatus(this.role_name, "verifikasi") ? <a href={"/pemindahan-detail?verifikasi=1&id=" + item.id} className="m-2 link-warning">Verifikasi</a> : null}
                                                    {item.status === 1 && item.transfer_active === 1 && Helper.getAccessStatus(this.role_name, "cancel") ? <a href={"/pemindahan-detail?cancel=1&id=" + item.id} className="m-2 link-red">Batal Verifikasi</a> : null}
                                                    {item.status !== 0 && Helper.getAccessStatus(this.role_name, "view") ? <a href={"/pemindahan-detail?view=1&id=" + item.id} className="m-2 link-green">Lihat</a> : null}
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                            <div className="row">
                                <div className="col-md-6 align-self-center">
                                    <div></div>
                                    <div className="mb-4">
                                        {Helper.getAccessStatus(this.role_name, "import") ? <a href="/pemindahan-import" className="btn btn-success mr-2" role="button">Impor Data</a> : null}
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <nav className="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers" style={{'float':'right'}}>
                                        <ul className="pagination">
                                            <li className={"page-item " + (this.state.pages[0] === 1 ? "disabled" : "")}><button data={this.state.pages[0]-1} className="page-link" aria-label="Previous" onClick={this.pageChange}>«</button></li>
                                            {this.state.pages.map((page) => (
                                                <li key={page} className={"page-item " + ((parseInt(this.state.page) === page ? "active" : ""))}><button data={page} className="page-link" onClick={this.pageChange}>{page}</button></li>
                                            ))}
                                            <li className="page-item"><button data={this.state.pages[this.state.pages.length-1]+1} className="page-link" aria-label="Next" onClick={this.pageChange}>»</button></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
                <Modal show={this.state.show} onHide={this.handleClose} animation={true} className="">
                    <Modal.Header closeButton>
                        <Modal.Title>Buat Permintaan {this.state.isLoading1 ? ' (Loading ...)' : ''}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="form-group">
                            <div className="mb-2">
                                <label htmlFor="id-surat">No. Permintaan Order</label>
                                <input value={this.state.no_so} onChange={(e)=>this.setState({no_so:e.target.value})} type="text" className="form-control" name="id-surat" placeholder="Mis: SO-FG01/2006/00001"/>
                            </div>
                            <div className="mb-2">
                                <label htmlFor="id-surat">No. Surat</label>
                                <input value={this.state.no_surat} onChange={(e)=>this.setState({no_surat:e.target.value})} type="text" className="form-control" name="id-surat" placeholder="Mis: DO-FG01/2006/00001"/>
                            </div>
                            <div className="mb-2">
                                <label htmlFor="id-surat">Lokasi Tujuan</label>
                                <DropdownList
                                    data={this.state.locations}
                                    textField={item => item === undefined ? "Pilih" : item.nama_site + " - " + item.nama}
                                    filter
                                    onChange={value => this.setState({location:value})}
                                    value={this.state.location}
                                />
                            </div>
                            <div className="mb-2">
                                <label htmlFor="id-surat">Grade Barang</label>
                                <DropdownList
                                    data={this.state.grades}
                                    textField={item => item === undefined ? "Pilih" : item.nama}
                                    filter
                                    onChange={value => this.setState({grade:value})}
                                    value={this.state.grade}
                                />
                            </div>
                            <div className="mb-2">
                                <label htmlFor="alamat-surat">Keterangan Pemindahan</label>
                                <textarea type="text" className="form-control" name="qty-label" placeholder="Keterangan Pemindahan" value={this.state.keterangan} onChange={(e)=> this.setState({keterangan:e.target.value})}/>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.handleClose()} className="btn btn-light">Batal</button>
                        <button onClick={()=> this.save()} className="btn btn-primary btn-utama">Lanjutkan</button>
                    </Modal.Footer>
                </Modal>
            
                <Modal show={this.state.show1} onHide={this.handleClose1} animation={true} className="">
                    <Modal.Header closeButton>
                    <Modal.Title>Filter Data</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="form-group">
                            <label htmlFor="id-surat">Tanggal Pemindahan</label>
                            <div className="form-check my-2">
                                <input className="form-check-input" type="radio" id="formCheck-1" checked={this.state.filter === 1} onChange={(e)=>this.setState({filter:e.target.checked?1:2})}/>
                                <label className="form-check-label" htmlFor="formCheck-1">Tampilkan Semua Data</label>
                            </div>
                            <div className="form-check my-2">
                                <input className="form-check-input" type="radio" id="formCheck-2" checked={this.state.filter === 2} onChange={(e)=>this.setState({filter:e.target.checked?2:1})}/>
                                <label className="form-check-label" htmlFor="formCheck-2">Tampilkan Rentang Khusus</label>
                            </div>
                            <div className="form-group" style={{display:this.state.filter === 1 ? "none":""}}>
                                <table style={{width:"100%"}}>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div className="mb-2">
                                                    <label htmlFor="alamat-surat">Mulai Tanggal</label>
                                                    <input defaultValue={this.getFormattedDate1()} onChange={(e)=>this.setState({date1:e.target.value})} className="form-control" type="date"/>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="mb-2">
                                                    <label htmlFor="alamat-surat">Hingga Tanggal</label>
                                                    <input defaultValue={this.getFormattedDate2()} onChange={(e)=>this.setState({date2:e.target.value})} className="form-control" type="date"/>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="mb-2">
                                <label htmlFor="id-surat">Status Pemindahan</label>
                                <DropdownList
                                    data={["Semua Data","Belum Diverifikasi","Sudah Diverifikasi","Ditolak"]}
                                    textField={item => item === "" ? "Semua Data" : item}
                                    filter
                                    onChange={value => this.setState({status:value})}
                                    value={this.state.status}
                                />
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.handleClose1()} className="btn btn-warning">Batal</button>
                        <button onClick={()=> this.handleFilterChange()} className="btn btn-primary btn-utama">Lanjutkan</button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}