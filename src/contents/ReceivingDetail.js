// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BaseComponent from '../components/BaseComponent'
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';
import queryString from 'query-string';
import Helper from '../Helper'
import DropdownList from 'react-widgets/lib/DropdownList'

export default class ReceivingDetail extends Component {
    state = {
        grades: [],
        grade: undefined
    }

    constructor(props) {
        super(props);
        
        this.params = queryString.parse(window.location.search)
    }

    render(){
        return (
        <Fragment>
            <Sidebar active="receiving"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <ShippingDetailSub params={this.params}/>
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class ShippingDetailSub extends BaseComponent {

    pageTitle = 'Tambah Penerimaan'
    
    state = {
        id: 0,
        verifikasi: false,
        view: false,
        items: [],
        items_inner: [],
        details: [],
        details_original: [],
        deletes: [],
        details_inner: [],
        details_inner_original: [],
        deletes_inner: [],
        shpping: false,
        subitems: [],
        isLoading1 : false,
        search : '',
        show : false,
        show1 : false,
        show2 : false,
        show3 : false,
        outerbox: '',
        innerbox: '',
        code : '',
        no_so: '',
        no_surat: '',
        nama_toko: '',
        alamat: '',
        keterangan: '',
        no_so1: '',
        no_surat1: '',
        nama_toko1: '',
        alamat1: '',
        receiving_types: [],
        receiving_type: false,
        resume_items: [],
        innerbox_id: undefined
    }

    componentDidMount() {
        super.componentDidMount()
        this.loadGrades()
    }

    constructor(props) {
        super(props);
        this.params = props.params;
        console.log(this.params);

        console.log(Helper.getCurrentTime(null, 7));

        if(this.params.id !== undefined && this.params.id !== '0') {
            if(!Helper.getAccessStatus("Penerimaan Produk", "edit")) {
                window.location.href = "/notfound";
            }
            this.state.id = this.params.id;
            this.state.verifikasi = this.params.verifikasi !== undefined;
            this.state.cancel = this.params.cancel !== undefined;
            this.state.view = this.params.view !== undefined;

            fetch(process.env.REACT_APP_SERVER_URL + '/receiving-by-id?id=' + this.params.id)
                .then(res => res.json())
                .then((res) => {
                    if(res.status) {
                        console.log(res);
                        this.setState({
                            'tipe':res.data.receiving.tipe.toString(),
                            'tipe_nama':res.data.receiving.receiving_type,
                            'no_so':res.data.receiving.no_so,
                            'status':res.data.receiving.status,
                            'no_surat':res.data.receiving.no_surat,
                            'nama_toko':res.data.receiving.nama_toko,
                            'alamat':res.data.receiving.alamat,
                            'keterangan':res.data.receiving.keterangan,
                            'username':res.data.receiving.username,
                            'details':res.data.details,
                            'details_original':res.data.details.map((x)=>x),
                            'details_inner':res.data.details_inner,
                            'details_inner_original':res.data.details_inner.map((x)=>x),
                        });
                        this.loadShippingDetail(res.data.details);
                        this.loadShippingDetailInner(res.data.details_inner);
                    }else {
                        alert(res.error);
                        window.history.back();  
                    }
                })
            .catch(console.log)
        }else {
            if(!Helper.getAccessStatus("Penerimaan Produk", "add")) {
                window.location.href = "/notfound";
            }

            this.state.tipe = this.params.tipe;
            this.state.tipe_nama = this.params.tipe_nama;
            this.state.no_so = this.params.no_so;
            this.state.no_surat = this.params.no_surat;
            this.state.nama_toko = this.params.nama_toko;
            this.state.alamat = this.params.alamat;
            this.state.keterangan = this.params.keterangan;
        }
    }

    handleClose = () => this.setState({show:false});
    handleShow = () => {
        this.setState({
            tipe1:this.state.tipe,
            no_so1:this.state.no_so,
            no_surat1:this.state.no_surat,
            nama_toko1:this.state.nama_toko,
            alamat1:this.state.alamat,
            keterangan1:this.state.keterangan,
        })
        this.loadReceivingType();
        this.setState({show:true});
    };

    handleClose1 = () => this.setState({show1:false});
    handleShow1 = (obxdata) => {
        var grade = undefined;
        this.state.grades.forEach((e) => {
            if(e.id === obxdata.grade) {
                grade = e;
            }
        })
        this.setState({
            obxdata:obxdata,
            show1: true,
            grade: grade,
            keterangan1:"",
        })
    };

    handleClose2 = () => this.setState({show2:false});
    handleShow2 = (ibxdata) => {
        var grade = undefined;
        this.state.grades.forEach((e) => {
            if(e.id === ibxdata.grade) {
                grade = e;
            }
        })
        this.setState({
            ibxdata:ibxdata,
            show2: true,
            grade: grade,
            keterangan1:"",
        })
    };

    handleClose3 = () => this.setState({show3:false});
    handleShow3 = (productdata) => {
        this.setState({
            productdata:productdata,
            grade:this.state.grades[0],
            show3: true,
            keterangan1:"",
        })
    };
    
    loadReceivingType() {
        fetch(process.env.REACT_APP_SERVER_URL + '/receiving-type-list')
            .then(res => res.json())
            .then((res) => {
                this.setState({ receiving_types: res.data})
                res.data.forEach((e) => {
                    if(e.id.toString() === this.state.tipe1) {
                        this.setState({ receiving_type: e})
                    }
                });
                this.setState({isLoading1:false})
            })
        .catch(console.log)
    }

    saveDetailObx = () => {
        var details = this.state.details;
        details.push([this.state.obxdata.outerbox, this.state.obxdata.shipping, this.state.grade.id, this.state.keterangan1, this.state.grade.nama]);
        
        var deletes = this.state.deletes;
        if(deletes.indexOf(this.state.obxdata.outerbox) >= 0) {
            deletes.splice(deletes.indexOf(this.state.obxdata.outerbox), 1);
        }

        this.setState({'details':details,'deletes':deletes,'keterangan1':"",'show1':false})
        this.loadShippingDetail(details)
    }

    saveDetailIbx = () => {
        var details = this.state.details_inner;
        details.push([this.state.ibxdata.innerbox, this.state.ibxdata.shipping, this.state.grade.id, this.state.keterangan1, this.state.grade.nama]);

        var deletes_inner = this.state.deletes_inner;
        if(deletes_inner.indexOf(this.state.ibxdata.innerbox) >= 0) {
            deletes_inner.splice(deletes_inner.indexOf(this.state.ibxdata.innerbox), 1);
        }

        this.setState({'details_inner':details,'deletes_inner':deletes_inner,'keterangan1':"",show2:false})
        this.loadShippingDetailInner(details)
    }

    saveDetailItem = () => {
        var details = this.state.details_inner;
        details.push([this.state.innerbox_id, 0, this.state.grade.id, this.state.keterangan1, this.state.grade.nama]);
        this.setState({details_inner:details,keterangan1:"",innerbox:'',show3:false})
        this.loadShippingDetailInner(details)
    }

    saveSurat = () => {
        // if(this.state.no_surat1 === '') {
        //     alert('No Surat harus diisi');
        //     return;
        // }
        if(this.state.nama_toko1 === '') {
            alert('Nama Toko harus diisi');
            return;
        }
        if(this.state.alamat1 === '') {
            alert('Alamat harus diisi');
            return;
        }

        this.setState({
            tipe:this.state.receiving_type.id,
            tipe_nama:this.state.receiving_type.nama,
            no_so:this.state.no_so1,
            no_surat:this.state.no_surat1,
            nama_toko:this.state.nama_toko1,
            alamat:this.state.alamat1,
            keterangan:this.state.keterangan1,
        })
        this.handleClose();
    }

    delete = (outerbox) => {
        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            var newdetails = [];
            var deletes = this.state.deletes;
            this.state.details.forEach(elem => {
                console.log(elem);
                console.log(outerbox)
                if(elem[0] === outerbox) {
                    deletes.push(elem[0]);
                }else {
                    newdetails.push(elem);
                }
            });
            console.log(deletes);
            console.log(newdetails);
            this.setState({'items':[],'details':newdetails,'deletes':deletes});
            if(newdetails.length === 0) {
                this.loadResumeItems();
            }else {
                this.loadShippingDetail(newdetails)
            }
        }
    }

    deleteInner = (innerbox) => {
        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            if(innerbox === 0) {
                this.setState({'items_inner':[],'details_inner':[]});
            }else {
                var newdetails = [];
                var deletes_inner = this.state.deletes_inner;
                this.state.details_inner.forEach(elem => {
                    console.log(elem);
                    console.log(innerbox)
                    if(elem[0] === innerbox) {
                        deletes_inner.push(elem[0]);
                    }else {
                        newdetails.push(elem);
                    }
                });
                console.log(newdetails);
                console.log(deletes_inner);
                if(newdetails.length === 0) {
                    this.setState({'items_inner':[],'details_inner':newdetails,'deletes_inner':deletes_inner}, function() {
                        this.loadResumeItems();
                    });
                }else {
                    this.setState({'items_inner':[],'details_inner':newdetails,'deletes_inner':deletes_inner});
                    this.loadShippingDetailInner(newdetails)
                }
            }
        }
    }

    loadLabel() {
        if(this.state.code === "") {
            alert("Outerbox / Innerbox harus diisi");
            return;
        }
        
        this.setState({'outerbox':this.state.code}, (e)=> {
            this.loadOuterLabel();
        });
    }

    onEnterPress = (f) => {
        if(f.keyCode === 13 && f.shiftKey === false) {
            f.preventDefault();
            this.loadOuterLabel()
        }
    }

    onEnterPress1 = (f) => {
        if(f.keyCode === 13 && f.shiftKey === false) {
            f.preventDefault();
            this.loadInnerLabel()
        }
    }

    onEnterPress2 = (f) => {
        if(f.keyCode === 13 && f.shiftKey === false) {
            f.preventDefault();
            this.loadInnerLabel1()
        }
    }
    
    loadOuterLabel() {
        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/receiving-labels-outer-by-code?code=' + this.state.outerbox)
            .then(res => res.json())
            .then((res) => {
                this.setState({isLoading1:false})
                if(res.status) {
                    var obxdata = res.data;
                    var details = this.state.details;
                    var isExist = false;
                    details.forEach((e) => {
                        if(e[0] === obxdata.outerbox) {
                            alert("Outerbox sudah pernah dimasukkan");
                            isExist = true;
                            return
                        }
                    })
                    if(isExist) {
                        return;
                    }

                    this.setState({outerbox:""})
                    this.handleShow1(obxdata)
                }else {
                    alert(res.error);
                    this.setState({'outerbox':''});
                }
            })
        .catch(console.log)
    }

    loadInnerLabel() {
        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/receiving-labels-inner-by-code?code=' + this.state.innerbox)
            .then(res => res.json())
            .then((res) => {
                this.setState({isLoading1:false})
                if(res.status) {
                    var ibxdata = res.data;
                    var details_inner = this.state.details_inner;
                    
                    var isExist = false;
                    details_inner.forEach((e) => {
                        if(e[0] === ibxdata.innerbox) {
                            alert("Innerbox sudah pernah dimasukkan");
                            isExist = true;
                        }
                    })
                    if(isExist) {
                        return;
                    }
                    this.setState({innerbox:""})
                    this.handleShow2(ibxdata)
                }else {
                    this.loadInnerLabel1();
                }
            })
        .catch(console.log)
    }

    loadInnerLabel1() {
        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/labels-inner-by-code?code=' + this.state.innerbox)
            .then(res => res.json())
            .then((res) => {
                this.setState({isLoading1:false})
                if(res.status) {
                    var ibxdata = res.data;
                    var details_inner = this.state.details_inner;
                    var isExist = false;
                    details_inner.forEach((e) => {
                        if(e[0] === ibxdata.innerbox) {
                            alert("Innerbox sudah pernah dimasukkan");
                            isExist = true;
                        }
                    })
                    if(isExist) {
                        return;
                    }
                    fetch(process.env.REACT_APP_SERVER_URL + '/nobox-innerbox-check?innerbox=' + ibxdata.innerbox)
                        .then(res => res.json())
                        .then((res) => {
                            if(res.status) {
                                // alert("Barang yang akan Anda masukkan adalah " + ibxdata.name + ". Barang ini belum pernah di-track oleh sistem Packtrack dan akan disimpan di " + this.state.innerbox)
                                this.setState({innerbox_id:ibxdata.innerbox})
                                this.handleShow3(ibxdata)
                            }else {
                                alert(res.error);
                                this.setState({'innerbox':''});
                            }
                        })
                    .catch(console.log)
                }else {
                    alert(res.error);
                }
            })
        .catch(console.log)
    }

    searchItem() {
        fetch(process.env.REACT_APP_SERVER_URL + '/item-by-plu?barcode=' + this.state.innerbox)
            .then(res => res.json())
            .then((res) => {
                this.setState({'innerbox':''});
                if(res.status) {
                    this.handleShow3(res.data)
                }else {
                    alert("Data tidak ditemukan");
                }
                console.log(res);
            })
        .catch(console.log)
    }

    loadShippingDetail(details) {
        if(details.length === 0) {
            return;
        }

        var param = "";
        details.forEach(elem => {
            param += "id=" + elem[0] + "&";
        });
        fetch(process.env.REACT_APP_SERVER_URL + '/receiving-details-by-ids?' + param)
                .then(res => res.json())
                .then((res) => {
                    if(res.status) {
                        var newitems = [];
                        details.forEach(elem => {
                            res.data.forEach(r => {
                                if(r.outerbox === elem[0]) {
                                    newitems.push([elem[1], r])
                                }
                            })
                        });
                        this.setState({items:newitems})
                        this.loadResumeItems()
                    }
                })
            .catch(console.log)
    }

    loadShippingDetailInner(details) {
        if(details.length === 0) {
            return;
        }

        var param = "";
        details.forEach(elem => {
            param += "id=" + elem[0] + "&";
        });
        fetch(process.env.REACT_APP_SERVER_URL + '/labels-inner-by-ids?' + param)
                .then(res => res.json())
                .then((res) => {
                    if(res.status) {
                        var param = "";
                        details.forEach(elem => {
                            param += "id=" + elem[1] + "&";
                        });
                        var items = res.data;
                        fetch(process.env.REACT_APP_SERVER_URL + '/shipping-by-ids?' + param)
                            .then(res => res.json())
                            .then((res) => {
                                var shippings = [];
                                if(res.status) {
                                    shippings = res.data;
                                }
                                var newdata = [];
                                details.forEach(elem => {
                                    console.log(elem);
                                    var newitem = {'id':elem[0],'item':false,'shipping':false}
                                    items.forEach((item) => {
                                        if(elem[0] === item.id) {
                                            newitem.item = item;
                                        }
                                    })
                                    shippings.forEach((item) => {
                                        if(elem[1] === item.id) {
                                            newitem.shipping = item;
                                        }
                                    })
                                    newdata.push(newitem);
                                });

                                console.log(newdata);
                                this.setState({items_inner:newdata})
                                this.loadResumeItems();
                            })
                        .catch(console.log)
                    }
                })
            .catch(console.log)
    }

    loadShippingDetailItem(outerbox, shipping) {
        fetch(process.env.REACT_APP_SERVER_URL + '/receiving-detail-items-by-outerbox?outerbox=' + outerbox + '&shipping=' + shipping)
                .then(res => res.json())
                .then((res) => {
                    if(res.status) {
                        this.setState({'shipping':res.data.shipping, 'subitems':res.data.items})
                    }else {
                        alert(res.error);
                    }
                })
            .catch(console.log)
    }

    getTotalBarang() {
        var total = 0;
        this.state.items.forEach((e) => {
            total += parseInt(e[1].total);
        })
        // total += this.state.items_inner.length;
        return total;
    }

    save = (status) => {
        if(this.state.details.length === 0 && this.state.details_inner.length === 0) {
            alert("Belum ada outerbox / innerbox nobox");
            return;
        }
        if(true) {
            var details = [];
            this.state.details.forEach((e) => {
                if(this.state.id === 0) {
                    details.push(e);
                }else if(this.state.details_original.indexOf(e) < 0) {
                    details.push(e);
                }
            });
            var details_inner = [];
            this.state.details_inner.forEach((e) => {
                if(this.state.id === 0) {
                    details_inner.push(e);
                }else if(this.state.details_inner_original.indexOf(e) < 0) {
                    details_inner.push(e);
                }
            });
            var data = {
                receiving:this.state.id,
                tipe:this.state.tipe,
                no_so:this.state.no_so,
                no_surat:this.state.no_surat,
                nama_toko:this.state.nama_toko,
                alamat:this.state.alamat,
                keterangan:this.state.keterangan,
                details:details,
                deletes:this.state.deletes,
                details_inner:details_inner,
                deletes_inner:this.state.deletes_inner,
                status:status,
                created_at:Helper.getCurrentTime(null, 7),
                lokasi:localStorage.getItem("LOCATION_ID"),
                created_by:localStorage.getItem("USERID"),
            }
            console.log(data);
            axios.post(process.env.REACT_APP_SERVER_URL+'/receiving-detail-save', data)
                .then(response => response.data)
                .then((res) => {
                        // console.log(res);
                        if(res.status) {
                            // alert("Data berhasil disimpan");
                            window.location.href='/penerimaan';
                        }else {
                            alert(res.error);
                        }
                    }
                );
        }
    }

    getOuterBoxTitle = (item) => {
        console.log(item);
        var title = this.getGradeObx(item.outerbox, item.grade) + " / " + item.serial_no + " / " + item.total;
        if(item.tipe === "A") {
            title += " / " + item.info + " / " + item.color + " / " + item.profil_karton;
        }
        return title;
    }

    loadResumeItems() {
        var param = "";
        this.state.details.forEach(elem => {
            param += "outerbox=" + elem[0] + "&";
        });
        if(param === "") {
            var items = []
            this.state.items_inner.forEach((subitem) => {
                items.push(subitem.item);
            })
            this.setState({resume_items:items})
        }else {
            fetch(process.env.REACT_APP_SERVER_URL + '/receiving-detail-items-by-outerboxs?' + param)
                    .then(res => res.json())
                    .then((res) => {
                        if(res.status) {
                            var items = []
                            this.state.items_inner.forEach((subitem) => {
                                items.push(subitem.item);
                            })
                            res.data.forEach((subitem) => {
                                items.push(subitem);
                            })
                            this.setState({resume_items:items})
                        }
                    })
                .catch(console.log)
        }
    }

    getResumeItems() {
        var items = [];
        this.state.resume_items.forEach((subitem) => {
            var itemExisting = false;
            items.forEach((i) => {
                if(i.sku_name === subitem.sku_name && i.color === subitem.color) {
                    itemExisting = i;
                    return;
                }
            });

            if(!itemExisting) {
                itemExisting = {
                    sku_name: subitem.sku_name,
                    color: subitem.color,
                    items: []
                };
                items.push(itemExisting);
            }

            var isExist = false;
            itemExisting.items.forEach((i) => {
                if(i.size === subitem.size) {
                    isExist = true;
                    i.total = i.total + 1;
                    return;
                }
            })

            if(!isExist) {
                itemExisting.items.push({
                    size: subitem.size,
                    total: 1
                })
            }

        });

        return items;
    }

    getTotal(e) {
        var result = 0;
        e.forEach((f) => {
            result = result + f.total;
        })

        return result;
    }

    getTotalAll() {
        var resume = this.getResumeItems();
        var total = 0;
        resume.forEach((i) => {
            i.items.forEach((j) => {
                total = total + j.total;
            })
        })

        return total;
    }

    loadGrades() {
        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/grades-list')
            .then(res => res.json())
            .then((res) => {
                var grades = res.data;
                grades.unshift({"id":0,"nama":"Grade A"})
                this.setState({ grades: res.data });
                res.data.forEach((e) => {
                    if(e.id.toString() === localStorage.getItem("GRADE")) {
                        this.setState({grade:e.id})
                    }
                })
                this.setState({isLoading1:false})
            })
        .catch(console.log)
    }

    getKeterangan(id) {
        var result = "";
        this.state.details_inner.forEach((e) => {
            if(e[0] === id) {
                result = "LABEL BARU - " + e[3];
            }
        })

        return result;
    }

    getGrade(id, grade) {
        var result = grade;
        console.log(this.state.details_inner);
        this.state.details_inner.forEach((e) => {
            if(e[0] === id && e[4] !== undefined) {
                result = e[4];
            }
        })

        return result;
    }

    getGradeObx(id, grade) {
        var result = grade;
        console.log(this.state.details);
        this.state.details.forEach((e) => {
            if(e[0] === id && e[4] !== undefined) {
                result = e[4];
            }
        })

        return result;
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/penerimaan"><span className="bc-link">Penerimaan Produk</span></a></li>
                            <li className="breadcrumb-item"><a href="/penerimaan-detail"><span className="bc-link">{this.state.id === 0 ? 'Tambah' : 'Ubah'} Daftar Permintaan Penerimaan</span></a></li>
                        </ol>
                    </div>
                </div>
                
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <h6 className="text-primary m-0 font-weight-bold">Permintaan Penerimaan</h6>
                        </div>
                        <div className="card-body">
                            <h4 className="mb-4"><strong>Informasi Penerimaan</strong></h4>
                            <div className="row">
                                <div className="col-12 col-lg-3 col-xl-3">
                                    <div className="mb-3">
                                        <label htmlFor="id-surat">Tipe Penerimaan</label>
                                        <input value={this.state.tipe_nama || ""} type="text" className="form-control" name="id-surat" readOnly/>
                                    </div>
                                </div>
                                <div className="col-12 col-lg-3 col-xl-3">
                                    <div className="mb-3">
                                        <label htmlFor="id-surat">No. Permintaan/Order</label>
                                        <input value={this.state.no_so} type="text" className="form-control" name="id-surat" readOnly/>
                                    </div>
                                </div>
                                <div className="col-12 col-lg-3 col-xl-3">
                                    <div className="mb-3">
                                        <label htmlFor="id-surat">No. Surat</label>
                                        <input value={this.state.no_surat} type="text" className="form-control" name="id-surat" readOnly/>
                                    </div>
                                </div>
                                <div className="col-md-12 col-lg-3 col-xl-3">
                                    <div className="mb-3">
                                        <label htmlFor="nama-toko">Nama Sumber</label>
                                        <input value={this.state.nama_toko} type="text" className="form-control" name="nama-toko" readOnly/>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12 col-lg-3 col-xl-3">
                                    <div className="mb-3">
                                        <label htmlFor="alamat-surat">Alamat Sumber</label>
                                        <input value={this.state.alamat} type="text" className="form-control" name="alamat-surat" readOnly/>
                                    </div>
                                </div>
                                <div className="col-12 col-lg-3 col-xl-6">
                                    <div className="mb-3">
                                        <label htmlFor="alamat-surat">Keterangan</label>
                                        <input value={this.state.keterangan} type="text" className="form-control" name="alamat-surat" readOnly/>
                                    </div>
                                </div>
                                <div className="col-md-12 col-lg-3 col-xl-3">
                                    <div className="mb-3">
                                        <label htmlFor="tujuan">Dibuat Oleh</label>
                                        <input value={this.state.username} type="text" className="form-control" name="tujuan" readOnly/>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div className="text-right">
                                {this.state.view ? null : <button onClick={()=>this.handleShow()} className="btn btn-primary btn-utama" id="btn-id-surat">Ubah Data Penerimaan</button>}
                            </div>

                            <br/>
                            <h4 className="mb-4"><strong>Ringkasan Data Barang</strong></h4>
                            
                            <div className="table-responsive table mt-2">
                                <table className="table">
                                    <thead>
                                        <tr style={{backgroundColor:"orange"}}>
                                            <th>Nama Barang</th>
                                            <th>Warna</th>
                                            <th>Rincian Jumlah Barang (ukuran/jumlah)	</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {(this.getResumeItems().map((subitem) => (
                                            <tr key={subitem.sku_name}>
                                                <td>{subitem.sku_name}</td>
                                                <td>{subitem.color}</td>
                                                <td>
                                                    {subitem.items.map((e) => (
                                                        <span key={e.size}>
                                                            {e.size}/{e.total},&nbsp;
                                                        </span>
                                                    ))}
                                                </td>
                                                <td>{this.getTotal(subitem.items)}</td>
                                            </tr>
                                        )))}
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colSpan="4" className="text-right"><h5>Total Semua barang: <strong>{this.getTotalAll()} Barang</strong></h5></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <br/>

                            <div className="row">
                                <div className="col text-left">
                                    <a className="btn btn-warning" role="button" href="/penerimaan">Batal</a>&nbsp;&nbsp;
                                </div>
                                {this.state.verifikasi ?
                                    <div className="col text-right">
                                        <button className="btn btn-danger" onClick={()=>this.save(2)}>Tolak</button>&nbsp;&nbsp;
                                        <button className="btn btn-primary btn-utama" onClick={()=>this.save(1)}>Setujui</button>
                                    </div> : null
                                }
                                {this.state.view || this.state.verifikasi || this.state.cancel ? null :
                                    <div className="col text-right">
                                        <button className="btn btn-primary btn-utama" onClick={()=>this.save(0)}>Simpan</button>
                                    </div>
                                }
                                {this.state.cancel ?
                                    <div className="col text-right">
                                        <button className="btn btn-danger" onClick={()=>this.save(0)}>Batal Verifikasi</button>
                                    </div>
                                    : null
                                }
                            </div>
                        </div>
                    </div>
                </div>
                
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <h6 className="text-primary m-0 font-weight-bold">Daftar Produk</h6>
                        </div>
                        <div className="card-body">
                            <div id="accordion-data" className="accordion m-0">
                                <ul className="nav nav-tabs">
                                    <li className="nav-item"><a className={"nav-link active"} role="tab" data-toggle="tab" href="#tab-1">Outerbox</a></li>
                                    <li className="nav-item"><a className={"nav-link"} role="tab" data-toggle="tab" href="#tab-2">Innerbox</a></li>
                                </ul>
                                <div className="tab-content">
                                    <div role="tabpanel" id="tab-1" className={"tab-pane active"}>
                                        {this.state.view ? null :
                                            <div className="row my-3">
                                                <div className="col">
                                                    <div className="input-group">
                                                        <input onKeyDown={(f)=>this.onEnterPress(f)} value={this.state.outerbox} onChange={(e) => this.setState({outerbox:e.target.value})} className="form-control" type="text" placeholder="Cari Outerbox" name="cari-produk"/>
                                                        <div className="input-group-append"><button onClick={()=>this.loadOuterLabel()} className="btn btn-info" type="button">TAMBAH</button></div>
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                        <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                            <table className="table dataTable my-0" id="dataTable">
                                                <thead>
                                                    <tr style={{backgroundColor:"orange"}}>
                                                        <th>Grade / Outerbox / Total Innerbox / PLU/SKU Name / Color / Profile</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                        {this.state.items.map((item) => (
                                        <div key={item[1].outerbox} className="card my-2">
                                            <div id={item[1].serial_no} className="card-header m-0 py-0">
                                                <div className="d-flex justify-content-between align-items-center align-items-xl-center">
                                                    <h1><button onClick={()=>this.loadShippingDetailItem(item[1].outerbox, item[0])} className="btn btn-link" type="button" data-toggle="collapse" data-target={"#collapse-" + item[1].serial_no} aria-expanded="true" aria-controls={"collapse-" + item[1].serial_no}><strong>{this.getOuterBoxTitle(item[1])}</strong></button></h1>
                                                    {this.state.view ? null :
                                                    <div>
                                                        <a className="text-danger" mina1="#" onClick={()=>this.delete(item[1].outerbox)} href="##"><i className="fa fa-trash-o"></i>&nbsp;Hapus</a>
                                                    </div>
                                                    }
                                                </div>
                                            </div>
                                            <div id={"collapse-" + item[1].serial_no} className="collapse hide" aria-labelledby={item[1].serial_no} data-parent="#accordion-data">
                                                <div className="card-body">
                                                    <div className="p-3 my-2 bg-success text-white rounded">
                                                        <p className="m-0"><i className="fas fa-info-circle"></i>&nbsp;<strong>{this.state.shipping ? item[1].grade + ", " + this.state.shipping.no_so + ", " + Helper.getCurrentDate(this.state.shipping.created_at) + ", " + this.state.shipping.nama_toko + ", " + this.state.shipping.lokasi + " - " + this.state.shipping.site : ""}</strong></p>
                                                    </div>
                                                    <div className="table-responsive">
                                                        <table className="table">
                                                            <thead>
                                                                <tr style={{backgroundColor:"orange"}}>
                                                                    <th>Nama Produk</th>
                                                                    <th>Warna</th>
                                                                    <th>Ukuran</th>
                                                                    <th>Innerbox</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {(this.state.subitems.length === 0) ? (
                                                                    <tr>
                                                                        <td colSpan="4">Belum ada data</td>
                                                                    </tr>
                                                                ) : this.state.subitems.map((subitem) => (
                                                                    <tr key={subitem.innerbox}>
                                                                        <td>{subitem.name}</td>
                                                                        <td>{subitem.color}</td>
                                                                        <td>{subitem.size}</td>
                                                                        <td>{subitem.serial_no}</td>
                                                                    </tr>
                                                                ))}
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                    <hr/>
                                    <h5 className="mb-4">Total Barang dalam Daftar:<strong> {this.getTotalBarang()} Barang&nbsp;</strong>dalam <strong>{this.state.items.length} Outerbox.</strong></h5>
                            
                                    </div>
                                    <div role="tabpanel" id="tab-2" className={"tab-pane"}>
                                        {this.state.view ? null :
                                            <div className="row my-3">
                                                <div className="col">
                                                    <div className="input-group">
                                                        <input onKeyDown={(f)=>this.onEnterPress1(f)} value={this.state.innerbox} onChange={(e) => this.setState({innerbox:e.target.value})} className="form-control" type="text" placeholder="Inner Box" name="cari-produk"/>
                                                        <div className="input-group-append"><button onClick={()=>this.loadInnerLabel()} className="btn btn-info" type="button">TAMBAH</button></div>
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                        <div className="table-responsive table mt-2">
                                            <table className="table">
                                                <thead>
                                                    <tr style={{backgroundColor:"orange"}}>
                                                        <th>Nama Produk</th>
                                                        <th>Warna</th>
                                                        <th>Ukuran</th>
                                                        <th>Innerbox</th>
                                                        <th>Grade</th>
                                                        <th>Keterangan</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {(this.state.items_inner.map((subitem) => (
                                                        <tr key={subitem.item.id}>
                                                            <td>{subitem.item.name}</td>
                                                            <td>{subitem.item.color}</td>
                                                            <td>{subitem.item.size}</td>
                                                            <td>{subitem.item.serial_no}</td>
                                                            <td>{this.getGrade(subitem.id, subitem.item.grade)}</td>
                                                            <td>{subitem.shipping === false ? 
                                                                this.getKeterangan(subitem.id) :
                                                                subitem.shipping.no_so + ", " + Helper.getCurrentDate(subitem.shipping.created_at) + ", " + subitem.shipping.nama_toko + ", " + subitem.shipping.lokasi
                                                            }</td>
                                                            <td>
                                                             {this.state.view ? null :<a className="text-danger" onClick={()=>this.deleteInner(subitem.item.id)} mina1="#" href="##"><i className="fa fa-trash-o"></i>&nbsp;Hapus</a>}
                                                            </td>
                                                        </tr>
                                                    )))}
                                                </tbody>
                                            </table>
                                        </div>
                                        <hr/>
                                        <h5 className="mb-4">Total Innerbox: <strong>{this.state.items_inner.length} Barang.</strong></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
                <Modal show={this.state.show} onHide={this.handleClose} animation={true} className="">
                    <Modal.Header closeButton>
                        <Modal.Title>Ubah Data Toko {this.state.isLoading1 ? ' (Loading ...)' : ''}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="form-group">
                            <div className="mb-2">
                                <label htmlFor="id-surat">Tipe Pengeluaran</label>
                                <DropdownList
                                    data={this.state.receiving_types}
                                    textField={item => item === undefined ? "Pilih" : item.nama}
                                    filter
                                    onChange={value => this.setState({receiving_type:value})}
                                    value={this.state.receiving_type}
                                />
                            </div>
                            <div className="mb-2">
                                <label htmlFor="id-surat">No. Permintaan Order</label>
                                <input value={this.state.no_so1} onChange={(e)=>this.setState({no_so1:e.target.value})} type="text" className="form-control" name="id-surat" placeholder="Mis: SO-FG01/2006/00001"/>
                            </div>
                            <div className="mb-2">
                                <label htmlFor="id-surat">No. Surat</label>
                                <input value={this.state.no_surat1} onChange={(e)=>this.setState({no_surat1:e.target.value})} type="text" className="form-control" name="id-surat" placeholder="Mis: DO-FG01/2006/00001"/>
                            </div>
                            <div className="mb-2">
                                <label htmlFor="nama-toko">Nama Toko/Tujuan</label>
                                <input value={this.state.nama_toko1} onChange={(e)=>this.setState({nama_toko1:e.target.value})} type="tel" className="form-control" name="nama-toko" placeholder="Toko Sepatu Maju Jaya"/>
                            </div>
                            <div className="mb-2">
                                <label htmlFor="alamat-surat">Alamat Tujuan</label>
                                <input value={this.state.alamat1} onChange={(e)=>this.setState({alamat1:e.target.value})} type="tel" className="form-control" name="alamat-surat" placeholder="Jl. Panglima Maharaja Semarang"/>
                            </div>
                            <div className="mb-2">
                                <label htmlFor="alamat-surat">Keterangan Penerimaan</label>
                                <textarea type="text" className="form-control" name="qty-label" placeholder="Keterangan Penerimaan" value={this.state.keterangan1} onChange={(e)=> this.setState({keterangan1:e.target.value})}/>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.handleClose()} className="btn btn-light">Batal</button>
                        <button onClick={()=> this.saveSurat()} className="btn btn-primary btn-utama">Lanjutkan</button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.show1} onHide={this.handleClose1} animation={true} className="">
                    <Modal.Header closeButton>
                        <Modal.Title>Informasi Barang</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p><strong>Info Asal</strong></p>
                        <div className="row col-md-12">
                            <table>
                                <tbody>
                                    <tr><td>No. OBX&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>{this.state.obxdata === undefined ? "" : this.state.obxdata.serial_no}</td></tr>
                                    <tr><td>Grade Asal&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>{this.state.obxdata === undefined ? "" : this.state.obxdata.grade_name}</td></tr>
                                    <tr><td>Tgl. Keluar&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>{this.state.obxdata === undefined ? "" : this.state.obxdata.shipping_date}</td></tr>
                                    <tr><td>No. Order&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>{this.state.obxdata === undefined ? "" : this.state.obxdata.no_so}</td></tr>
                                    <tr><td>Sumber&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>{this.state.obxdata === undefined ? "" : this.state.obxdata.nama_toko}</td></tr>
                                    <tr><td>Lokasi&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>{this.state.obxdata === undefined ? "" : this.state.obxdata.lokasi}</td></tr>
                                </tbody>
                            </table>
                        </div>
                        <br/>
                        <div className="alert alert-warning d-xl-flex align-items-xl-center d-flex" role="alert"><i className="fas fa-info-circle mr-3 text-warning" style={{fontSize:"40px"}}></i><span>Pastikan informasi barang telah sesuai sebelum meng-<em>update</em> data barang pada proses penerimaan ini.</span></div>
                        <p><strong>Info Baru</strong></p>
                        <div className="form-group">
                            <div className="mb-2">
                                <label htmlFor="id-surat">Grade Baru</label>
                                <DropdownList
                                    data={this.state.grades}
                                    textField={item => item === undefined ? "Pilih" : item.nama}
                                    filter
                                    onChange={value => this.setState({grade:value})}
                                    value={this.state.grade}
                                />
                            </div>
                            <div className="mb-2">
                                <label htmlFor="alamat-surat">Keterangan</label>
                                <textarea type="text" className="form-control" name="qty-label" placeholder="Keterangan Penerimaan" value={this.state.keterangan1} onChange={(e)=> this.setState({keterangan1:e.target.value})}/>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.handleClose()} className="btn btn-light">Batal</button>
                        <button onClick={()=> this.saveDetailObx()} className="btn btn-primary btn-utama">Lanjutkan</button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.show2} onHide={this.handleClose2} animation={true} className="">
                    <Modal.Header closeButton>
                        <Modal.Title>Informasi Barang</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p><strong>Info Asal</strong></p>
                        <div className="row col-md-12">
                            <table>
                                <tbody>
                                    <tr><td>No. IBX&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>{this.state.ibxdata === undefined ? "" : this.state.ibxdata.serial_no}</td></tr>
                                    <tr><td>Grade Asal&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>{this.state.ibxdata === undefined ? "" : this.state.ibxdata.grade_name}</td></tr>
                                    <tr><td>Tgl. Keluar&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>{this.state.ibxdata === undefined ? "" : this.state.ibxdata.shipping_date}</td></tr>
                                    <tr><td>No. Order&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>{this.state.ibxdata === undefined ? "" : this.state.ibxdata.no_so}</td></tr>
                                    <tr><td>Sumber&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>{this.state.ibxdata === undefined ? "" : this.state.ibxdata.nama_toko}</td></tr>
                                    <tr><td>Lokasi&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>{this.state.ibxdata === undefined ? "" : this.state.ibxdata.lokasi}</td></tr>
                                </tbody>
                            </table>
                        </div>
                        <br/>
                        <div className="alert alert-warning d-xl-flex align-items-xl-center d-flex" role="alert"><i className="fas fa-info-circle mr-3 text-warning" style={{fontSize:"40px"}}></i><span>Pastikan informasi barang telah sesuai sebelum meng-<em>update</em> data barang pada proses penerimaan ini.</span></div>
                        <p><strong>Info Baru</strong></p>
                        <div className="form-group">
                            <div className="mb-2">
                                <label htmlFor="id-surat">Grade Baru</label>
                                <DropdownList
                                    data={this.state.grades}
                                    textField={item => item === undefined ? "Pilih" : item.nama}
                                    filter
                                    onChange={value => this.setState({grade:value})}
                                    value={this.state.grade}
                                />
                            </div>
                            <div className="mb-2">
                                <label htmlFor="alamat-surat">Keterangan</label>
                                <textarea type="text" className="form-control" name="qty-label" placeholder="Keterangan Penerimaan" value={this.state.keterangan1} onChange={(e)=> this.setState({keterangan1:e.target.value})}/>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.handleClose()} className="btn btn-light">Batal</button>
                        <button onClick={()=> this.saveDetailIbx()} className="btn btn-primary btn-utama">Lanjutkan</button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.show3} onHide={this.handleClose3} animation={true} className="">
                    <Modal.Header closeButton>
                        <Modal.Title>Informasi Barang</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p><strong>Info Asal</strong></p>
                        <div className="row col-md-12">
                            <table>
                                <tbody>
                                    <tr><td>PLU/Barcode&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>{this.state.productdata === undefined ? "" : this.state.productdata.barcode}</td></tr>
                                    <tr><td>SKU&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>{this.state.productdata === undefined ? "" : this.state.productdata.sku}</td></tr>
                                    <tr><td>Nama Produk&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>{this.state.productdata === undefined ? "" : this.state.productdata.name}</td></tr>
                                    <tr><td>Warna&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>{this.state.productdata === undefined ? "" : this.state.productdata.color}</td></tr>
                                    <tr><td>Ukuran&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>{this.state.productdata === undefined ? "" : ("CM: " + this.state.productdata.cm + ", UK: " + this.state.productdata.uk + ", US: " + this.state.productdata.us)}</td></tr>
                                </tbody>
                            </table>
                        </div>
                        <br/>
                        <div className="alert alert-warning d-xl-flex align-items-xl-center d-flex" role="alert"><i className="fas fa-info-circle mr-3 text-warning" style={{fontSize:"40px"}}></i><span>Pastikan informasi barang telah sesuai sebelum meng-<em>update</em> data barang pada proses penerimaan ini.</span></div>
                        <p><strong>Info Baru</strong></p>
                        <div className="form-group">
                            <div className="mb-2">
                                <label htmlFor="id-surat">Grade Baru</label>
                                <DropdownList
                                    data={this.state.grades}
                                    textField={item => item === undefined ? "Pilih" : item.nama}
                                    filter
                                    onChange={value => this.setState({grade:value})}
                                    value={this.state.grade}
                                />
                            </div>
                            <div className="mb-2">
                                <label htmlFor="alamat-surat">Serial Innerbox</label>
                                <input disabled={this.state.innerbox_id !== undefined} onKeyDown={(f)=>this.onEnterPress2(f)} type="text" className="form-control" name="kode-produk" placeholder="Masukkan Serial Innerbox" value={this.state.innerbox} onChange={(e)=>this.setState({'innerbox':e.target.value})}/>
                            </div>
                            <div className="mb-2">
                                <label htmlFor="alamat-surat">Keterangan</label>
                                <textarea type="text" className="form-control" name="qty-label" placeholder="Keterangan Penerimaan" value={this.state.keterangan1} onChange={(e)=> this.setState({keterangan1:e.target.value})}/>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.handleClose()} className="btn btn-light">Batal</button>
                        <button onClick={()=> this.saveDetailItem()} className="btn btn-primary btn-utama">Lanjutkan</button>
                    </Modal.Footer>
                </Modal>
            
            </div>
        )
    }
}