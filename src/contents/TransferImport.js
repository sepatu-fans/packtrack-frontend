import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BaseComponent from '../components/BaseComponent'
import axios from 'axios';
import Helper from '../Helper'

export default class TransferImport extends Component {
    render(){
        return (
        <Fragment>
            <Sidebar active="transfer"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <TransferImportSub />
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class TransferImportSub extends BaseComponent {

    state = {
        csv: '',
        isLoading: false
    }
    
    uploadCsv(e) {
        let imageFromObj = new FormData();
        imageFromObj.append("csv", e.target.files[0]);
        this.setState({isLoading:true});
        axios.post(process.env.REACT_APP_SERVER_URL+'/transfer-uploadcsv', imageFromObj)
            .then(response => response.data)
            .then((res) => {
                if(res.status) {
                    alert("Upload Success");
                    this.setState({csv:res.data, isLoading:false});
                }else {
                    alert(res.error);
                    // this.setState({isLoading:false});
                    window.location.reload()
                }
            }
        );
    }

    componentDidMount() {
        if(!Helper.getAccessStatus(['Data Produk'], "import")) {
            window.location.href = "/notfound";
        }
    }
  
    handleSave = () => {
      console.log('this is:', this.state);
      if(this.state.csv === '') {
        alert("CSV File is required!");
        return;
      }
      this.putDataToDB();
    }
  
    putDataToDB = () => {
      var data = {
        csv:this.state.csv,
        lokasi:localStorage.getItem("LOCATION_ID"),
        created_by:localStorage.getItem("USERID")
      }
      console.log(data);
      this.setState({isLoading:true});
      axios.post(process.env.REACT_APP_SERVER_URL+'/transfer-upload', data)
          .then(response => response.data)
          .then((res) => {
            this.setState({isLoading:false});
            if(res.status) {
              alert("Import Success. " + res.data + " Imported");
              window.location.href = '/pemindahan';
            }else {
              alert(res.error);
            }
          }
        );
    };

    downlaodTemplate() {
        window.location.href = '/templates/template-transfer.csv';
    }
    
    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/pemindahan"><span className="bc-link">Pemindahan Produk</span></a></li>
                            <li className="breadcrumb-item"><a href="/pemindahan-import"><span className="bc-link">Import Item</span></a></li>
                        </ol>
                    </div>
                    <div className="col text-right"></div>
                </div>
                <div>
                    <div className="row">
                        <div className="col-lg-12 col-xl-12">
                            <div className="card shadow mb-4">
                                <div className="card-header py-3">
                                    <h6 className="text-primary m-0 font-weight-bold">Data Pemindahan</h6>
                                </div>
                                <div className="card-body">
                                    <h4 className="mb-4"><strong>Import Data Pemindahan</strong></h4>
                                    <div className="form-group">
                                        <div className="mb-3">
                                            <div className="row">
                                                <div className="col-12 col-md-6 col-lg-6 col-xl-6"><label htmlFor="nama-sepatu">CSV File</label>
                                                <input accept=".csv" onChange={(e) => this.uploadCsv(e)} type="file" className="form-control" name="file" style={{height:'auto'}}/></div>
                                            </div>
                                        </div>
                                        <div className="row my-4">
                                            <div className="col">

                                                <button onClick={()=> this.downlaodTemplate()} className="btn btn-primary">Download Template</button>&nbsp;&nbsp;&nbsp;
                                                {this.state.isLoading ? (
                                                <h6>Loading ...</h6>
                                                ) : 
                                                <button type="button" onClick={this.handleSave} className="btn btn-primary btn-utama">Simpan</button>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}