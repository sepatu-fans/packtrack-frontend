// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BaseComponent from '../components/BaseComponent'
import Barcode from 'react-barcode';
import axios from 'axios';
import Helper from '../Helper'

export default class ManajementLabelsAdd extends Component {

    render(){
        return (
        <Fragment>
            <Sidebar active="manajement-labels"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <ManajementLabelsAddSub />
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class ManajementLabelsAddSub extends BaseComponent {

    pageTitle = "Tambah Label Innerbox"

    state = {
        product_status : false,
        product_id: "",
        product_barcode : "",
        product_name : "",
        product_color : "",
        product_size : "",
        product_size_cm : "",
        product_size_uk : "",
        product_size_us : "",
        serial_no : "IBX000000000",
        jumlah : 1,
    }

    componentDidMount() {
        super.componentDidMount()
        if(!Helper.getAccessStatus(['Manajemen Label Inner','Manajemen Label Outer'], "add")) {
            window.location.href = "/notfound";
        }
        this.getMaxMin();
    }

    generate(redirect) {
        if(!this.state.product_status) {
            alert("Data Produk harus diisi");
            return;
        }
        if(this.state.serial_no === "") {
            alert("Nomor Serial harus diisi");
            return;
        }

        var data = {
            item:this.state.product_id,
            jumlah:this.state.jumlah,
            serial_no:this.state.serial_no,
            created_at:Helper.getCurrentTime(null, 7),
            created_by:localStorage.getItem("USERID"),
        }
        axios.post(process.env.REACT_APP_SERVER_URL+'/labels-inner-add', data)
            .then(response => response.data)
            .then((res) => {
                    // console.log(res);
                    if(res.status) {
                        // alert("Data berhasil disimpan");  
                        if(!redirect) {
                            window.location.href = '/manajement-labels';
                        }else {
                            var idmin = res.data;
                            var idmax = parseInt(res.data)+parseInt(this.state.jumlah)-1;
                            // var urlData = process.env.REACT_APP_SERVER_URL + '/labels-inner-by-id-min-max?idmin=' + idmin + '&idmax=' + idmax;
                            var urlPrint = process.env.REACT_APP_SERVER_URL + '/print-labels-inner-by-id-min-max?idmin=' + idmin + '&idmax=' + idmax;
                            fetch(urlPrint)
                                .then(res => res.json())
                                .then((data) => {
                                    if(data.status) {
                                        window.open(process.env.REACT_APP_SERVER_URL +'/barcode/label-inner-by-id-min-max?idmin=' + idmin + '&idmax=' + idmax)
                                    }else {
                                        alert(data.error)
                                    }
                                })
                                .catch(() => {
                                    this.setState({isLoading:false})
                                })
                        }   
                    }else {
                        alert(res.error);
                    }
                }
            );
    }
    
    searchItem(s) {
        fetch(process.env.REACT_APP_SERVER_URL + '/item-by-plu?barcode=' + s)
            .then(res => res.json())
            .then((data) => {
                if(data.status) {
                    this.setState({
                        product_status:true,
                        product_id:data.data.id,
                        product_barcode:data.data.barcode,
                        product_name:data.data.sku_name,
                        product_color:data.data.color,
                        product_size:data.data.size,
                        product_size_cm:data.data.cm,
                        product_size_uk:data.data.uk,
                        product_size_us:data.data.us,
                    })
                }else {
                    this.setState({
                        product_status:false,
                        product_barcode:"",
                        product_name:"",
                        product_color:"",
                        product_size:"",
                        product_size_cm : "",
                        product_size_uk : "",
                        product_size_us : "",
                    })
                }
                console.log(data);
            })
        .catch(console.log)
    }

    getMaxMin() {
        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/labels-inner-get-new-code')
            .then(res => res.json())
            .then((res) => {
                if(res.status) {
                    this.setState({
                        show1:true,
                        isOuter:false,
                        serial_no:res.data.serial_no
                    })
                }else {
                    alert(res.error);
                }
            })
            .catch(console.log)
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/manajement-labels"><span className="bc-link">Manajemen Label</span></a></li>
                            <li className="breadcrumb-item"><a href="/manajement-labels-add"><span className="bc-link">Tambah Label Innerbox</span></a></li>
                        </ol>
                    </div>
                </div>
                <div>
                    <div className="row">
                        <div className="col-lg-12 col-xl-12">
                            <div className="card shadow mb-4">
                                <div className="card-header py-3">
                                    <h6 className="text-primary m-0 font-weight-bold">Innerbox Barcode</h6>
                                </div>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-lg-12 col-xl-5 d-xl-flex align-items-xl-center mb-2">
                                            <div className="text-center border rounded border-dark border-2 w-75 mx-auto" style={{overflowX:"auto"}}>
                                                <BarcodeCard name={this.state.product_name} color={this.state.product_color} size={this.state.product_size} size_us={this.state.product_size_us} size_uk={this.state.product_size_uk} size_cm={this.state.product_size_cm} barcode={this.state.product_barcode} serial_no={this.state.serial_no} id="123456" />
                                                <h6>Preview</h6>
                                            </div>
                                        </div>
                                        <div className="col-xl-7 mt-4">
                                            <h4 className="mb-4"><strong>Konten Barcode</strong></h4>
                                            <div className="row">
                                                <div className="col-sm-6 col-xl-6">
                                                    <label htmlFor="kode-plu">Barcode Produk {!this.state.product_status ? <span className="badge badge-danger">* Produk tidak ditemukan</span> : <span></span>}</label>
                                                    <input type="text" className="form-control" placeholder="Masukkan Barcode Produk" name="kode-plu" onChange={(e)=> this.searchItem(e.target.value)}/>
                                                </div>
                                                <div className="col-sm-6 col-md-6 col-lg-6">
                                                    <label htmlFor="nama-sepatu">Nama Sepatu</label>
                                                    <input type="text" value={this.state.product_name} className="form-control" name="nama-sepatu" placeholder="Mis: AXEL N." disabled="disabled"/>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <div className="mb-3">
                                                    <div className="row">
                                                        <div className="col-sm-6 col-md-6 col-lg-6">
                                                            <label htmlFor="pilih-grade">Warna Sepatu</label>
                                                            <input type="text" value={this.state.product_color} className="form-control" name="nama-sepatu" placeholder="Mis: BLACK/BLACK" disabled="disabled"/>
                                                        </div>
                                                        <div className="col-sm-6 col-md-6 col-lg-6">
                                                            <label htmlFor="nama-sepatu">Ukuran Sepatu</label>
                                                            <input type="text" value={this.state.product_size} className="form-control" name="nama-sepatu" placeholder="Mis: 33" disabled="disabled"/>
                                                        </div>
                                                        <div className="col-sm-6 col-md-6 col-lg-6">
                                                            <label htmlFor="qty-label">Jumlah</label>
                                                            <input value={this.state.jumlah} type="number" min="1" className="form-control" placeholder="Jumlah Label" name="qty-label" onChange={(e)=> this.setState({jumlah:e.target.value})}/>
                                                        </div>
                                                        <div className="col-sm-6 col-md-6 col-lg-6">
                                                            <label htmlFor="qty-label">Nomor Serial</label>
                                                            <input type="text" value={this.state.serial_no}  className="form-control"  disabled="disabled" placeholder="Nomor Serial" onChange={(e)=> this.setState({serial_no:e.target.value})}/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="mb-3"></div>
                                                <div className="mb-3"></div>
                                                <div className="mb-3"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div className="row mb-3">
                                        <div className="col text-right">
                                            <button onClick={()=> this.generate(true)} className="btn btn-danger">Cetak Label</button>
                                            <button onClick={()=> this.generate(false)} className="btn btn-primary btn-utama ml-3">Generate</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class BarcodeCard extends Component {

    constructor(props) {
      super(props);
      this.d = props.id;
      this.id =  "div_" + props.id;
      this.idPrint =  "btnprint_" + props.id;
    }
  
    render() {
      return (
        <div id={this.id} style={{background: "white",visibility:"visible"}}>
          <div style={{}}>
            <table style={{margin:"0px auto"}}>
              <tbody>
              <tr>
                <td colSpan="2" style={{borderTop:"0px",padding:"0rem"}}>
                  <div style={{color:"black",fontSize:"35px", fontWeight:"bold"}}>{this.props.name === "" ? "SAMPLE" : this.props.name}</div>
                  <div style={{color:"black",fontSize:"25px", fontWeight:"medium", marginTop:"-15px"}}>{this.props.color === "" ? "SAMPLE" : this.props.color}</div>
                </td>
              </tr>
              <tr>
                <td style={{borderTop:"0px",padding:"0rem"}}>
                  <div style={{marginLeft:"-5px",marginTop:"-15px"}}>
                    <Barcode
                      value={this.props.barcode === "" ? "sample" : this.props.barcode}
                      width={1}
                      height={65}
                      fontSize={12}
                    />
                  </div>
                  <div style={{marginLeft:"-5px",marginTop:"-7.5px"}}>
                    <Barcode
                      value={this.props.serial_no === "" ? "sample" : this.props.serial_no}
                      width={1}
                      height={25}
                      fontSize={12}
                    />
                  </div>
                </td>
                <td style={{borderTop:"0px",padding:"0rem"}}>
                  <table style={{marginTop:"0px"}}>
                    <tbody>
                    <tr>
                      <td style={{borderTop:"0px",padding:"0rem",color:"black",fontSize:"15px", fontWeight:"bold", textAlign:"center"}}>CM<br/>{this.props.size_cm === "" || this.props.size_cm === null ? 0 : this.props.size_cm}</td>
                      <td style={{borderTop:"0px",padding:"0rem",color:"black",fontSize:"15px", fontWeight:"bold", textAlign:"center"}}>UK<br/>{this.props.size_uk === "" || this.props.size_uk === null ? 0 : this.props.size_uk}</td>
                      <td style={{borderTop:"0px",padding:"0rem",color:"black",fontSize:"15px", fontWeight:"bold", textAlign:"center"}}>USA<br/>{this.props.size_us === "" || this.props.size_us === null ? 0 : this.props.size_us}</td>
                    </tr>
                    <tr>
                      <td colSpan="3" style={{borderTop:"0px",padding:"0rem"}}>
                        <div style={{color:"black",fontSize:"100px", fontWeight:"bold", marginTop:"-35px"}}>{this.props.size === "" ? 0 : this.props.size}</div>
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              </tbody>
            </table>
          </div>
          <div className="page-break"></div>
        </div>
      );
    }
  }