import React,{ Component,Fragment } from 'react';
import queryString from 'query-string';
import htmlToImage from 'html-to-image';
import '../LabelPreview.css';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BaseComponent from '../components/BaseComponent'
import Helper from '../Helper'
import { savePDF } from '@progress/kendo-react-pdf';

const bodyRef = React.createRef();

export default class LabelOuterPreview extends Component {
  constructor(props) {
    super(props);
    
    this.params = queryString.parse(window.location.search)
  }

  render(){
      return (
      <Fragment>
          <Sidebar active="manajement-labels"/>
          <div className="d-flex flex-column" id="content-wrapper">
              <div id="content">
                  <Header/>
                  <LabelOuterPreviewSub params={this.params} />
              </div>
              <Footer/>
          </div>
      </Fragment>
      )
  }
}

class LabelOuterPreviewSub extends BaseComponent {

  pageTitle = "Cetak Outer Label"
    
  state = {
    ids : [],
    refids : [],
    title : ''
  }
  constructor(props) {
    super(props);

    this.params = props.params;
    console.log(this.params);

    var param = "";
    if(Array.isArray(this.params.id)) {
      this.params.id.forEach(elem => {
        param += "id=" + elem + "&";
      });
    }else {
      param = "id=" + this.params.id;
    }

    var title = "";
    fetch(process.env.REACT_APP_SERVER_URL + '/labels-outer-by-ids?' + param)
            .then(res => res.json())
            .then((res) => {
                if(res.status) {
                    var ids = [];
                    var refids = [];
                    res.data.forEach(elem => {
                      elem.refid = this.makeid(4);
                      refids.push(elem.refid);
                      ids.push(elem)
                      // if(title === '') {
                      //   title = elem.serial_no;
                      // }else {
                      //   title = title + "-" + elem.serial_no;
                      // }
                    })
                    // if(this.params.special === '1') {
                      title = res.data[0].serial_no + "-" + res.data[res.data.length-1].serial_no;
                      if(res.data.length === 1) {
                        title = res.data[0].serial_no;
                      }
                    // }
                    this.setState({ids:ids,refids:refids,title:title});
                }
            })
        .catch(console.log)
  }

  componentDidMount() {
    super.componentDidMount()
      if(!Helper.getAccessStatus(['Manajemen Label Outer'], "print")) {
          window.location.href = "/notfound";
      }
  }

  makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  downloads() {
    this.ids.map((id, i) => {     
      return this.download(id);
    })
  }

  download(id) {
    htmlToImage.toPng(document.getElementById("div_" + id), { quality: 1 })
              .then(function (dataUrl) {
                // var link = document.createElement('a');
                // link.download = 'label.png';
                // link.href = dataUrl;

                var canvas  = document.createElement("canvas");
                var img = new Image();
                img.src = dataUrl;
                img.onload = () => {
                  canvas.height = img.width;
                  canvas.width = img.height;
                  var context = canvas.getContext("2d");
                  context.rotate(-90 * Math.PI / 180);
                  context.translate(-canvas.height, 0);
                  context.drawImage(img, 0, 0);
                  var rotatedImageSrc =  canvas.toDataURL("image/jpeg", 100);

                  // document.getElementById(idPrint).style.visibility = "visible";
                  document.getElementById("div_" + id).style.width = "50mm";
                  document.getElementById("div_" + id).style.height = "70mm";
                  document.getElementById("div_" + id).className = "imageDone";
                  document.getElementById("div_" + id).innerHTML = "<img style='width:150px;height:210px;' src='" + rotatedImageSrc + "'/>";

                  var progress = parseInt(document.getElementsByClassName("imageDone").length);
                  var progressMax = parseInt(document.getElementById("progressmax").value);
                  var percent = parseInt(progress*100/progressMax);
                  document.getElementById("progress").innerHTML = percent + "%";

                };
              });
  }

  createPdf = (html, title) => {
    var param = "";
    this.state.ids.forEach(elem => {
      param += "id=" + elem.id + "&";
    });
    this.setState({isLoading:true})
    fetch(process.env.REACT_APP_SERVER_URL + '/print-labels-outer-by-ids?' + param)
          .then(res => res.json())
          .then((data) => {
              if(data.status) {
                  this.setState({isLoading:true})
                  savePDF(bodyRef.current, { 
                    paperSize: ["50mm","70mm"],
                    fileName: this.state.title + '.pdf',
                    margin: 0,
                    landscape: true
                  }, () => {
                    this.setState({isLoading:false})
                  })
              }
          })
          .catch(() => {
            this.setState({isLoading:false})
          })
    return;
  };

  kembali = (html) => {
    window.location.href = "/manajement-labels"
  }

  render() {
    return(
      
      <div className="container-fluid">
        <div className="row py-2 my-3">
            <div className="col-6 col-sm-8 col-md-8">
                <ol className="breadcrumb h6">
                    <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                    <li className="breadcrumb-item"><a href="/manajement-labels"><span className="bc-link">Manajemen Label</span></a></li>
                    <li className="breadcrumb-item"><a href="/"><span className="bc-link">Cetak Outer Label</span></a></li>
                </ol>
            </div>
            <div className="col text-right"></div>
        </div>
        <div>
            <div className="row">
                <div className="col-lg-12 col-xl-12">
                <div className="card shadow mb-4">
                  <div className="card-header py-3">
                      <h6 className="text-primary m-0 font-weight-bold">Cetak Label</h6>
                  </div>
                  <div className="card-body">
                      <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                      <div style={{width:"100%"}}>
                          <section className="header-bar">
                            {/* <h6>Progress : <span id="progress">0%</span></h6> */}
                            <input type="hidden" id="progressmax" value={this.state.ids.length}/>
                            {/* <button onClick={(e) => this.downloads()} style={{margin:"0px auto"}}>
                              Export Single Label
                            </button> */}
                          </section>
                          <section className="pdf-container">
                            <section className="pdf-toolbar">
                              <button className="btn btn-warning" onClick={this.kembali}><i className="fa fa-long-arrow-left"></i>&nbsp;Kembali</button>&nbsp;
                              {this.state.isLoading ? "Loading ..." : <button id="btnCetak" className="btn btn-danger" onClick={this.createPdf}><i className="fa fa-print"></i>&nbsp;Cetak</button>}
                              <hr/>
                            </section>
                            <section className="pdf-body" ref={bodyRef} style={{maxHeight:"350px",overflowY:"scroll"}}>
                              {
                                this.state.ids.map((dt, i) => {    
                                  return (<BarcodeCard key={dt.refid} id={dt.refid} cetak_ke={dt.cetak_ke} barcode={dt.barcode} serial_no={dt.serial_no} description={dt.name} color={dt.color} size={dt.size} jumlah={dt.jumlah} />) 
                                })
                              }
                            </section>
                          </section>
                        </div>
                      </div>
                      <div className="row my-4">
                          {/* <div className="col text-left"><a className="btn btn-warning text-white" role="button" href="manajemen-label.html"><i className="fa fa-long-arrow-left"></i>&nbsp;Kembali</a></div> */}
                          {/* <div className="col text-right"><a className="btn btn-danger text-white" role="button" rel="noopener noreferrer" target="_blank" href="about:blank"><i className="fa fa-print"></i>&nbsp;Cetak</a></div> */}
                      </div>
                  </div>
              </div>
                </div>
            </div>
        </div>
    </div>

      
    )
  }
}

class BarcodeCard extends Component {

  constructor(props) {
    super(props);
    // Don't call this.setState() here!
    this.d = props.id;
    this.id =  "div_" + props.id;
    this.idPrint =  "btnprint_" + props.id;
  }

  download(id, jumlah) {
    setTimeout(function() {
      htmlToImage.toPng(document.getElementById("div_" + id), { quality: 1 })
              .then(function (dataUrl) {

                var canvas  = document.createElement("canvas");
                var img = new Image();
                img.src = dataUrl;
                img.onload = () => {
                  canvas.height = img.width;
                  canvas.width = img.height;
                  var context = canvas.getContext("2d");
                  context.rotate(-90 * Math.PI / 180);
                  context.translate(-canvas.height, 0);
                  context.drawImage(img, 0, 0);
                  var rotatedImageSrc =  canvas.toDataURL("image/jpeg", 100);

                  // document.getElementById(idPrint).style.visibility = "visible";
                  document.getElementById("div_" + id).style.width = "50mm";
                  document.getElementById("div_" + id).style.height = "70mm";
                  document.getElementById("div_" + id).className = "imageDone";
                  document.getElementById("div_" + id).innerHTML = "<img style='width:150px;height:210px;' src='" + rotatedImageSrc + "'/>";

                  var progress = parseInt(document.getElementsByClassName("imageDone").length);
                  var progressMax = parseInt(document.getElementById("progressmax").value);
                  var percent = parseInt(progress*100/progressMax);
                  document.getElementById("progress").innerHTML = percent + "%";

                  console.log(jumlah);
                  for(var i = 0; i < jumlah - 1; i++) {
                    document.getElementById("div_" + id).parentElement.insertBefore(document.getElementById("div_" + id).cloneNode(true),document.getElementById("div_" + id))
                  }
                };
              });
    }, 50);
  }

  render() {
    return (
      <div id={this.id} style={{height:"50mm", width: "70mm",background: "white"}}>
        <div style={{padding:"10px"}}>
            <img alt="" style={{width:"72.5%"}} src={process.env.REACT_APP_SERVER_URL+"/barcode?height=18&code=" + this.props.serial_no}/>
            <div>
              <table className="table" style={{"width":"100%",marginTop:"10px" }}>
                <tbody>
                  <tr>
                    <td>&nbsp;
                      <img alt="" className="" style={{height:"25px"}} src="img/fans-logo.svg"/>
                      &nbsp;{this.props.cetak_ke}&nbsp;
                      <img alt="" className="" style={{height:"25px"}} src="img/ili-logo.svg"/>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
        </div>
        <div className="page-break"></div>
        {/* {this.download(this.d, this.props.jumlah)} */}
      </div>
    );
  }
}