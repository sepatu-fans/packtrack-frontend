// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'

export default class NotFound extends Component {
    render(){
        return (
        <Fragment>
            <Sidebar active='home'/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <NotFoundSub />
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class NotFoundSub extends Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="text-center mt-5">
                    <div className="error mx-auto" data-text="404">
                        <p className="m-0">404</p>
                    </div>
                    <p className="text-dark mb-5 lead">Page Not Found</p>
                    <p className="text-black-50 mb-0">It looks like you found a glitch in the matrix...</p><a href="/">← Back to Dashboard</a></div>
            </div>
        )
    }
}