// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';
import Helper from '../Helper'
import BaseComponent from '../components/BaseComponent'

export default class OpenCloseBox extends Component {

    render(){
        return (
        <Fragment>
            <Sidebar active="open-close-box"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <OpenCloseBoxSub />
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class OpenCloseBoxSub extends BaseComponent {
    
    pageTitle = "Open Close Box"

    state = {
        items: [],
        size : 10,
        page : 1,
        pages : [1],
        isLoading : false,
        search : '',
        show : false,
        outerbox : '',
        keterangan : 'Breakpack Eceran',
        show1 : false
    }

    role_name = "Open Close Box"

    handleClose = () => this.setState({show:false});
    handleShow = () => {
        this.setState({show:true, outerbox: '', keterangan: 'Breakpack Eceran'})
    }

    handleClose1 = () => this.setState({show1:false});
    handleShow1 = (item) => {
        this.setState({show1:true, outerbox: '', outerbox1: item.serial_no, id: item.id, grade: item.grade})
    }

    handleClose2 = () => this.setState({show2:false, outerbox1: ""});
    handleShow2 = () => {
        this.setState({show1:false,show2:true})
    }

    checkBox = () => {
        if(this.state.grade === "A") {
            window.location.href="/grades-a-detail?id=" + this.state.id
        }else {
            window.location.href="/grades-other-detail?id=" + this.state.id
        }
    }

    sizes = [
        10,20,50,100
    ]

    componentDidMount() {
        super.componentDidMount()
        if(!Helper.getAccessStatus(this.role_name, "view")) {
            window.location.href = "/notfound";
        }
        this.loadData();
    }

    loadData() {
        this.setState({isLoading:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/open-close-box?page=' + this.state.page + '&size=' + this.state.size + '&search=' + this.state.search + '&lokasi=' + localStorage.getItem("LOCATION_ID"))
            .then(res => res.json())
            .then((data) => {
            this.setState({ items: data.data, pages: data.pages })
            this.setState({isLoading:false})
        })
        .catch(console.log)
    }

    handleChange = (event) => {
        console.log(event.target.value);
        this.setState({ size: event.target.value }, function() {
            this.loadData();
        });
    };

    searchChange = (event) => {
        console.log(event.target.value);
        this.setState({search:event.target.value,page:1}, function() {
            this.loadData();
        });
    }

    pageChange = (event) => {
        console.log(event.target.getAttribute('data'));
        this.setState({ page: event.target.getAttribute('data') }, function() {
            this.loadData();
        });
    };

    closeBox() {
        if(this.state.outerbox === '') {
            alert("Outerbox harus diisi");
            return
        }
        if(this.state.outerbox !== this.state.outerbox1) {
            alert("Outerbox tidak sama");
            return
        }

        var data = {
            id:this.state.id,
            tipe:this.state.grade,
            note:this.state.keterangan,
            outerbox:this.state.outerbox,
            created_by:localStorage.getItem("USERID")
        }
        axios.post(process.env.REACT_APP_SERVER_URL+'/open-close-box-close', data)
            .then(response => response.data)
            .then((res) => {
                    // console.log(res);
                    if(res.status) {
                        if(res.data) {
                            alert("Outerbox berhasil ditutup.")
                            this.handleClose1()
                            this.loadData()
                        }else {
                            this.handleShow2()
                        }
                    }else {
                        alert(res.error);
                    }
                }
            );
    }

    openOuterbox = () => {
        if(this.state.outerbox === '') {
            alert("Outerbox harus diisi");
            return
        }
        fetch(process.env.REACT_APP_SERVER_URL + '/open-close-box-check?outerbox=' + this.state.outerbox + '&lokasi=' + localStorage.getItem("LOCATION_ID"))
            .then(res => res.json())
            .then((res) => {
                console.log(res);
                if(res.status) {
                    var resdata = res.data;
                    var data = {
                        id:resdata.reffid,
                        tipe:resdata.type,
                        note:this.state.keterangan,
                        outerbox:this.state.outerbox,
                        created_by:localStorage.getItem("USERID")
                    }
                    axios.post(process.env.REACT_APP_SERVER_URL+'/open-close-box-open', data)
                        .then(response => response.data)
                        .then((res) => {
                                // console.log(res);
                                if(res.status) {
                                    alert("Outerbox berhasil dibuka.")
                                    this.handleClose()
                                    this.loadData()
                                }else {
                                    alert(res.error);
                                }
                            }
                        );
                }else {
                    alert(res.error);
                }
            })
            .catch(console.log)
    }

    getTitle = (item) => {
        if(item.grade === "A") {
            if(item.tipe_profile === 1 || item.tipe_profile === 2) {
                return (
                    <div>
                        {item.info} - {item.color} / {item.profile}
                    </div>
                )
            } else if(item.tipe_profile === 3 || item.tipe_profile === 4) {
                return (
                    <div>
                        {item.profile}
                    </div>
                )
            }
        }

        return (
            <div>
                {item.profile}
            </div>
        )
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/profil-karton"><span className="bc-link">Open Close Box</span></a></li>
                        </ol>
                    </div>
                    <div className="col text-right">
                        {
                        !Helper.getAccessStatus(this.role_name, "add") ? null : 
                        <button onClick={()=>{this.handleShow()}} className="btn btn-primary btn-utama" id="btn-grade-a-requirement">Tambah</button>
                        }
                    </div>
                </div>
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <h6 className="text-primary m-0 font-weight-bold">Daftar Box Terbuka</h6>
                        </div>
                        <div className="card-body">
                            <div className="row my-3">
                                <div className="col">
                                    <select className="custom-select max-view mb-3" defaultValue={this.state.size} onChange={this.handleChange} >
                                        {this.sizes.map((item) => (
                                            <option key={item} value={item}>{item} baris</option>
                                        ))}
                                    </select>
                                </div>
                                <div className="col">
                                    <div className="input-group">
                                        <input onChange={this.searchChange} className="form-control" type="text" placeholder="Cari Produk" name="cari-produk"/>
                                        <div className="input-group-append"><button onClick={this.handleSearch} className="btn btn-info" type="button"><i className="fa fa-search"></i></button></div>
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                <table className="table dataTable my-0" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Dibuat Pada</th>
                                            <th>Tanggal Open Box</th>
                                            <th>Tanggal Close Box</th>
                                            <th>No. Serial</th>
                                            <th>PLU/SKU Name / Warna / Profil</th>
                                            <th>Keterangan</th>
                                            <th>Status</th>
                                            <th>Ketersediaan</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.isLoading ? (
                                            <tr>
                                                <td colSpan="8">
                                                    Loading
                                                </td>
                                            </tr>
                                        ) : 
                                        this.state.items.map((item) => (
                                            <tr key={item.id}>
                                                <td>{Helper.getCurrentTime(item.created_at)}</td>
                                                <td>{Helper.getCurrentTime(item.open_at)}</td>
                                                <td>{item.close_at === null ? '-' : Helper.getCurrentTime(item.close_at)}</td>
                                                <td>{item.serial_no}</td>
                                                <td>{this.getTitle(item)}</td>
                                                <td>{item.open_note}</td>
                                                <td>{item.is_close === 1 ? "CLOSED" : "OPEN"}</td>
                                                <td>{item.is_close === 1 ? "Tersedia" : "Tak Tersedia"}</td>
                                                <td>
                                                    {item.is_close === 0 && Helper.getAccessStatus(this.role_name, "close") ? <a onClick={()=>this.handleShow1(item)} href="##" className="m-2 link-red">Tutup</a> : null}
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                            <div className="row">
                                <div className="col-md-6 align-self-center">
                                    <div></div>
                                    <div className="mb-4">
                                        {/* <a href="/profil-karton-import" className="btn btn-success mr-2" role="button">Impor Data</a> */}
                                        {/* <a href="/profil-karton" className="btn btn-danger mr-2" role="button">Ekspor Data</a> */}
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <nav className="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers" style={{'float':'right'}}>
                                        <ul className="pagination">
                                            <li className={"page-item " + (this.state.pages[0] === 1 ? "disabled" : "")}><button data={this.state.pages[0]-1} className="page-link" aria-label="Previous" onClick={this.pageChange}>«</button></li>
                                            {this.state.pages.map((page) => (
                                                <li key={page} className={"page-item " + ((parseInt(this.state.page) === page ? "active" : ""))}><button data={page} className="page-link" onClick={this.pageChange}>{page}</button></li>
                                            ))}
                                            <li className="page-item"><button data={this.state.pages[this.state.pages.length-1]+1} className="page-link" aria-label="Next" onClick={this.pageChange}>»</button></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
                <Modal show={this.state.show} onHide={this.handleClose} animation={true} className="">
                    <Modal.Header closeButton>
                    <Modal.Title>Open Outerbox</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p>Outerbox yang di-open akan dapat diambil innerbox di dalamnya untuk dinput ke dalam daftar pengiriman barang.&nbsp;</p>
                        <div class="form-group">
                            <div class="mb-2">
                                <label for="outerbox">Outerbox yang Ingin Di-open</label>
                                <input value={this.state.outerbox} onChange={(e)=>this.setState({outerbox:e.target.value})} type="text" className="form-control" name="outerbox" placeholder="Mis: OBX000000001" autofocus=""/>
                            </div>
                            <div class="mb-2">
                                <label for="keterangan">Keterangan</label>
                                <input value={this.state.keterangan} onChange={(e)=>this.setState({keterangan:e.target.value})} type="text" id="keterangan" className="form-control" name="keterangan" placeholder="Mis: Breakpack Eceran"/>
                            </div>
                        </div>
                        <p>*Keterangan dapat diubah sesuai kebutuhan</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.handleClose()} className="btn btn-light">Batal</button>
                        <button onClick={()=> this.openOuterbox()} className="btn btn-primary btn-utama">Buka</button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.show1} onHide={this.handleClose1} animation={true} className="">
                    <Modal.Header closeButton>
                    <Modal.Title>Menutup Box</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p>Apakah Anda Yakin ingin menutup box <b>{this.state.outerbox1}</b>, silakan masukkan Nomor OBX tersebut untuk memvalidasi tindakan ini.&nbsp;</p>
                        <div class="form-group">
                            <div class="mb-2">
                                <label for="outerbox">Nomor OBX yang akan ditutup</label>
                                <input value={this.state.outerbox} onChange={(e)=>this.setState({outerbox:e.target.value})} type="text" className="form-control" name="outerbox" placeholder="Mis: OBX000000001" autofocus=""/>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.handleClose1()} className="btn btn-light">Batal</button>
                        <button onClick={()=> this.closeBox()} className="btn btn-primary btn-utama">Tutup Box</button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.show2} onHide={this.handleClose2} animation={true} className="">
                    <Modal.Header closeButton>
                    <Modal.Title>Menutup Box</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p>Isi box <b>{this.state.outerbox1}</b> belum sesuai dengan profil kartonnya, harap periksa kembali.</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <button onClick={()=> this.handleClose2()} className="btn btn-light">Batal</button>
                        <button onClick={()=> this.checkBox()} className="btn btn-primary btn-utama">Periksa</button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}