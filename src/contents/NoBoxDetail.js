// Header.js
import React, {Component, Fragment} from 'react';
import Sidebar from '../components/Sidebar'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BaseComponent from '../components/BaseComponent'
import axios from 'axios';
import queryString from 'query-string';
import Helper from '../Helper'

export default class NoBoxDetail extends Component {
    constructor(props) {
      super(props);
      
      this.params = queryString.parse(window.location.search)
    }

    render(){
        return (
        <Fragment>
            <Sidebar active="nobox"/>
            <div className="d-flex flex-column" id="content-wrapper">
                <div id="content">
                    <Header/>
                    <NoBoxDetailSub params={this.params}/>
                </div>
                <Footer/>
            </div>
        </Fragment>
        )
    }
}

class NoBoxDetailSub extends BaseComponent {
    pageTitle = 'Daftar Produk No Box'
    state = {
        id : 0,
        grades : [],
        grade : false,
        details: [],
        isLoading1 : false,
        isLoading3 : false,
        innerbox : '',
        show : false,
    }

    isDisable = false;

    handleClose = () => this.setState({show:false});
    handleShow = () => {
        this.setState({show:true});
    };

    constructor(props) {
        super(props);
        this.params = props.params;
        console.log(this.params);
    }

    componentDidMount() {
        super.componentDidMount()
        this.loadGrades();
    }
    
    loadGrades() {
        this.setState({isLoading1:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/grades-list')
            .then(res => res.json())
            .then((res) => {
                this.setState({ grades: res.data });
                res.data.forEach((e) => {
                    if(e.id.toString() === localStorage.getItem("GRADE")) {
                        this.setState({grade:e.id})
                    }
                })
                this.setState({isLoading1:false})
            })
        .catch(console.log)
    }

    delete = (serial) => {

        if(window.confirm("Apakah anda yakin menghapus data ini ?")) {
            var newdetails = [];
            this.state.details.forEach(elem => {
                if(elem.innerbox === serial) {
                }else {
                    newdetails.push(elem);
                }
            });
            console.log(newdetails);
            this.setState({'details':newdetails});
        }
    }
    
    loadInnerLabel() {
        if(this.isDisable) {
            return
        }
        this.isDisable = true;

        if(this.state.innerbox === '') {
            alert('Serial innerbox harus diisi');
            this.isDisable = false;
            return;
        }
        var isSkip = false;
        this.state.details.forEach(elem => {
            if(elem.serial_no === this.state.innerbox) {
                alert("Data sudah pernah dimasukkan");
                this.setState({'innerbox':''});
                isSkip = true;
                return;
            }
        });
        if(isSkip) {
            this.isDisable = false;
            return;
        }
        this.setState({isLoading3:true})
        fetch(process.env.REACT_APP_SERVER_URL + '/labels-inner-by-code?code=' + this.state.innerbox)
            .then(res => res.json())
            .then((res) => {
                if(res.status) {
                    var innerboxid = res.data.innerbox;
                    var data = res.data;

                    fetch(process.env.REACT_APP_SERVER_URL + '/nobox-innerbox-check?innerbox=' + innerboxid)
                        .then(res => res.json())
                        .then((res) => {
                            this.setState({isLoading3:false})
                            if(res.status) {
                                var details = this.state.details;
                                details.push(data);
                                this.setState({'details':details,'innerbox':''});
                            }else {
                                alert(res.error);
                                this.setState({'innerbox':''});
                            }
                            this.isDisable = false;
                        })
                    .catch(console.log)
                }else {
                    alert(res.error);
                    this.setState({'innerbox':''});
                    this.setState({isLoading3:false})
                    this.isDisable = false;
                }
            })
        .catch(console.log)
    }

    save = () => {
        if(!this.state.grade) {
            alert("Grade belum dipilih");
            return;
        }
        if(this.state.details.length === 0) {
            alert("Belum ada innerbox");
            return;
        }
        if(true) {
            var details = [];
            this.state.details.forEach((e) => {
                details.push(e.innerbox);
            });
            var data = {
                grade:this.state.grade,
                details:details,
                created_at:Helper.getCurrentTime(null, 7),
                created_by:localStorage.getItem("USERID"),
                lokasi:localStorage.getItem("LOCATION_ID"),
            }
            axios.post(process.env.REACT_APP_SERVER_URL+'/nobox-save', data)
                .then(response => response.data)
                .then((res) => {
                        // console.log(res);
                        if(res.status) {
                            // alert("Data berhasil disimpan");
                            window.location.href='/nobox';
                        }else {
                            alert(res.error);
                        }
                    }
                );
        }
    }

    onEnterPress1 = (f) => {
        if(f.keyCode === 13 && f.shiftKey === false) {
            f.preventDefault();
            this.loadInnerLabel()
        }
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row py-2 my-3">
                    <div className="col-6 col-sm-8 col-md-8">
                        <ol className="breadcrumb h6">
                            <li className="breadcrumb-item"><a href="/"><span className="bc-link"><i className="fa fa-home"></i></span></a></li>
                            <li className="breadcrumb-item"><a href="/grades-other"><span className="bc-link">Produk No Box</span></a></li>
                            <li className="breadcrumb-item"><a href="/grades-other"><span className="bc-link">Input Produk No Box</span></a></li>
                        </ol>
                    </div>
                </div>
                <div>
                    <div className="card shadow mb-4">
                        <div className="card-header py-3">
                            <h6 className="text-primary m-0 font-weight-bold">Produk</h6>
                        </div>
                        <div className="card-body">
                            <div className="row my-3">
                                <div className="col-xl-4">
                                    <h4 className="mb-4"><strong>Profil Packing</strong><br/></h4>
                                    <div className="form-group">
                                        <div className="mb-3">
                                            <label htmlFor="profil-sepatu">Grade</label>
                                            <select onChange={(e)=>this.setState({grade:e.target.value})} className="custom-select form-control" name="profil-selector" value={this.state.grade}>
                                                <option value=''>Pilihan Grade</option>
                                                <option value='0'>Grade A</option>
                                                {this.state.grades.map((item) => (
                                                    <option key={item.id} value={item.id}>{item.nama}</option>
                                                ))}
                                            </select>
                                        </div>
                                        <div className="mb-3">
                                            <label htmlFor="kode-produk">Serial Innerbox</label>
                                            <input autoFocus={true} onKeyDown={(f)=>this.onEnterPress1(f)} type="text" className="form-control" name="kode-produk" placeholder="Masukkan Serial Innerbox" value={this.state.innerbox} onChange={(e)=>this.setState({'innerbox':e.target.value})}/>
                                        </div>
                                        <div className="text-right mt-2">
                                            <button onClick={()=>this.loadInnerLabel()} className="btn btn-primary btn-utama" type="button">Tambah</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="col">
                                    <h4 className="mb-2"><strong>Data Inner Box</strong></h4>
                                    <p>{this.state.details.length} Innerbox dimasukkan</p>
                                    <div className="table-responsive">
                                        <table className="table">
                                            <thead>
                                                <tr>
                                                    <th>Nama Produk</th>
                                                    <th>Barcode</th>
                                                    <th>Warna</th>
                                                    <th>Ukuran</th>
                                                    <th>Innerbox</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.details.length === 0 ? (
                                                    <tr>
                                                        <td colSpan="5">Belum ada data</td>
                                                    </tr>
                                                ) : this.state.details.map((item) => (
                                                    <tr key={item.innerbox}>
                                                        <td>{item.barcode}</td>
                                                        <td>{item.name}</td>
                                                        <td>{item.color}</td>
                                                        <td>{item.size}</td>
                                                        <td>
                                                            {item.serial_no}
                                                        </td>
                                                        <td><a onClick={()=>this.delete(item.innerbox)} className="shadow-none text-danger" href="##"><i className="fa fa-trash-o"></i>&nbsp;Hapus</a></td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>  
                            <div className="row my-4">
                                <div className="col text-right d-flex justify-content-between">
                                    <a className="btn btn-warning" role="button" href="/grades-other">Batal</a>
                                    <div>
                                        <button className="btn btn-primary btn-utama" onClick={()=>this.save()}>Simpan</button>&nbsp;&nbsp;
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            
            </div>
        )
    }
}