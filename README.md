# packtrack-frontend

**Install**

- `git clone [repo-url]`
- `cd [project-path]`
- `npm install --verbose`

**How to Running**
- Modify env file
- `cp .env.sample .env`
- `nano .env`
- set api url configuration
- `npm start`

**Auto Start**
- Install forever (`npm -g install forever`)
- `cd [project-path]`
- `forever start -c "npm start" ./`
